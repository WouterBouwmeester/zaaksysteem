package Zaaksysteem::Backend::ZaakMeta::Component;

use Zaaksysteem::Moose;

extends 'Zaaksysteem::Backend::Component';
with 'Zaaksysteem::Search::ZQL::Role::Component';

sub _build_csv_data {
    # Not implemented
    return;
}

sub _build_csv_header {
    # Not implemented
    return;
}


1;


__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2021, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
