package Zaaksysteem::Backend::Rules::Rule::Action::SetValueMagicString;

use Moose;
use namespace::autoclean;

use Moose::Util::TypeConstraints;

use BTTW::Tools;

with qw[
    Zaaksysteem::Backend::Rules::Rule::Action
    Zaaksysteem::Backend::Rules::Rule::AttributesValidation
    MooseX::Log::Log4perl
];

=head1 NAME

Zaaksysteem::Backend::Rules::Rule::Action::SetValueMagicString - Dynamic value
setting via rules

=head1 SYNOPSIS

=head1 DESCRIPTION

This specific action set a dynamically calculated value on an attribute

=head1 TYPES

=head2 ValidExtendedFormulaAttribute

Validates a pseudo-attributename so we don't set values on unsupported aspects
of a case.

=cut

=head1 ATTRIBUTES

=head2 attribute_name

Holds a symbolic reference (magic string) to the target attribute for which
this action should set a value.

=cut

has attribute_name => (
    is       => 'rw',
    isa      => 'Defined',
    required => 1
);

=head2 value

State container for the replacement value of attributes on a case.

=cut

has formula => (
    is        => 'rw',
    isa       => 'Str',
    predicate => 'has_formula',
    init_arg  => 'value',
);

has can_change => (
    is => 'ro',
    isa => 'Bool',
    default => 0,
);

=head2 _data_attributes

Array of attributes to show in data.

=cut

has _data_attributes => (
    is      => 'rw',
    isa     => 'ArrayRef',
    default => sub {
        return [qw[attribute_name attribute_type formula can_change]];
    }
);

=head1 METHODS

=cut

sub BUILD {
    my $self = shift;

    $self->_validate_values;
}

=head2 integrity_verified

Hook for the rule verification checks. See
L<Zaaksysteem::Backend::Rules::Rule::Action> for the interface description.

=cut

sub integrity_verified { my $self = shift; return $self->formula ? 1 : 0 }

=head2 _validate_values

Private method that does the actual validation for values. Executed at during
object construction.

=head3 Exceptions

=over 4

=item rules/rule/action/set_value/invalid_values

Thrown when validation is unable to find a value to validate.

=back

=cut

sub _validate_values {
    my $self = shift;

    return if $self->has_formula;

    throw(
        'rules/rule/action/set_value/invalid_formula',
        'No values set in attribute "formula"'
    );
}

=head2 _populate_validation_results

Populates a validation result object with the local value.

=cut

sig _populate_validation_results => 'Object, HashRef';

sub _populate_validation_results {
    my ($self, $result_object, $params, $case) = @_;

    my $ztt = Zaaksysteem::ZTT->new();

    $ztt->add_context($case) if $case;
    $ztt->add_context($params);

    my $value = $ztt->process_template($self->formula)->string;
    my $name = $self->attribute_name;

    $self->log->debug(
        sprintf(
            "Setting '%s' to '%s' via '%s'", $name, $value, $self->formula
        )
    );

    $params->{$name} = $value;
    $result_object->changes->{$name} = $value;

    $result_object->changeable_attributes->{$name} = 0;
    return $result_object;

}

__PACKAGE__->meta->make_immutable;

__END__

=head1 SEE ALSO

L<Zaaksysteem::Backend::Rules>, L<Zaaksysteem::Manual>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2020, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
