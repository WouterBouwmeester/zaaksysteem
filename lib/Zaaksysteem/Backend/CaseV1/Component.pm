package Zaaksysteem::Backend::CaseV1::Component;

use Zaaksysteem::Moose;

extends 'Zaaksysteem::Backend::Component';

sub as_object {
    my $self = shift;

    my %instance = $self->get_inflated_columns();
    my $uuid     = $self->get_column('id');
      return {
          type      => 'case',
          reference => $uuid,
          preview   => 'case(...' . substr($uuid, -6) . ')',
          instance  => \%instance,
      };
}

1;


__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2021, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
