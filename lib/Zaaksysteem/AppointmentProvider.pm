package Zaaksysteem::AppointmentProvider;
use Moose::Role;

use BTTW::Tools;

=head1 NAME

Zaaksysteem::AppointmentProvider - Interface role for providers of appointment systems

=head1 SYNOPSIS

    package Zaaksysteem::AppointmentProvider::SomeCoolAppointmentSystem;
    use Moose;
    with 'Zaaksysteem::AppointmentProvider';
    # etc.

=head1 REQUIRED METHODS

=head2 shortname

Returns a unique short name for the appointment provider plugin.

=head2 label

Returns a descriptive label for this appointment provider plugin. This is shown
to the person configuring the interface as the name of the appointment
provider.

=head2 configuration_items

This class method returns a list of L<Zaaksysteem::ZAPI::Form::Field>
instances, that are used to configure this plugin. Usually, it's for things
like API endpoint URLs, certificates, etc.

The C<name> fields of these objects should start with
C<< interface_<shortname> >>, so the field names don't clash with those of
other plugins.

=head2 test_connection

Tests the connection to the remote system. If a connection can't be established,
an exception should be thrown, containing enough diagnostic information for the
administrator to fix the problem.

=head2 get_location_list

Retrieve a list of locations for appointments to take place in, as available in
the remote system.

Return value should be an array reference containing hash references like this:

    [
        { id => 123, label => 'The Moon' },
        { id => 42, label => 'My back yard' },
    ]

=head2 get_product_list

Retrieve a list of products for the location whose "id" is given as the first
argument.

Should return a list of products:

    [
        { id => 12, label => 'Long appointment' },
        { id => 13, label => 'Short appointment' },
    ]

=head2 get_dates

Returns a list of available dates for the given product and location ids.

    [
        "2017-05-12",
        "2017-05-13",
        "2017-05-14",
    ]

=head2 get_timeslots

Given a product id, location id and date, return a list of available timeslots.

These timeslots will be used as a base for book_appointment, and should have the
following structure:

    [
        {
            start_time => "2017-05-12T13:00:00Z",
            end_time => "2017-05-12T14:00:00Z",
            plugin_data => {
                xxx => "plugin-specific data goes here",
                yyy => "frontend passes it back verbatim",
            },
        },
    ]

=head2 book_appointment

Book the appointment in the remote system. Accepts one argument, a "timeslot"
as returned by the L</get_timeslots> method.

Should return a new L<Zaaksysteem::Object::Types::Appointment> instance (unsaved).

=head2 delete_appointment

Delete the appointment described by the supplied
L<Zaaksysteem::Object::Types::Appointment> field from the remote system.

=cut

requires qw(
    shortname
    label
    configuration_items

    test_connection

    get_location_list
    get_product_list

    get_dates
    get_timeslots
    book_appointment
    delete_appointment
);

sig shortname => '=> Str';

sig label => '=> Str';

sig configuration_items => '=> @Zaaksysteem::ZAPI::Form::Field';

sig test_connection => '';

sig get_location_list => ' => ArrayRef[HashRef]';

sig get_product_list => 'Str => ArrayRef[HashRef]';

sig get_dates => 'Str, Str => ArrayRef[Str]';

sig get_timeslots => 'DateTime, Str, Str => ArrayRef[HashRef]';

sig book_appointment => 'HashRef, Zaaksysteem::Object::Types::Subject => Zaaksysteem::Object::Types::Appointment';

sig delete_appointment => 'Zaaksysteem::Object::Types::Appointment => HashRef';

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
