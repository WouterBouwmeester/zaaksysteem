package Zaaksysteem::Zaken::Roles::CommunicationTab;
use Moose::Role;

use BTTW::Tools;

requires qw(log);

sub unread_communication_count {
    my $self = shift;

    return $self->zaak_meta->unread_communication_count;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2021, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
