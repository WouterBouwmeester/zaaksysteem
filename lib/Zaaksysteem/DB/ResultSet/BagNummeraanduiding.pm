package Zaaksysteem::DB::ResultSet::BagNummeraanduiding;

use Moose;
use namespace::autoclean;

use BTTW::Tools;

BEGIN {
    extends 'Zaaksysteem::DB::ResultSet::BagGeneral';
}

with 'Zaaksysteem::Search::ScoredResultSet';

=head1 NAME

Zaaksysteem::DB::ResultSet::BagNummeraanduiding - Additional BAG search logic

=head1 METHODS

=head2 search_address

This method returns a resultset of ranked rows that best match the given
address.

    my $rs = $c->model('DB::BagNummeraanduiding')->search_address(
        Zaaksysteem::Object::Types::Address->new(...)
    );

This method uses L<Zaaksysteem::Search::ScoredResultSet>, and defines 5
scoring levels (+1, +10, +100, +1000, +10000).

=cut

sig search_address => 'Zaaksysteem::Object::Types::Address';

sub search_address {
    my $self = shift;
    my $address = shift;

    return $self->scored_search($address, {
        join => { openbareruimte => 'woonplaats' }
    });
}

=head2 _build_score_map

Implements scoring map for L<Zaaksysteem::Search::ScoredResultSet>.

=cut

sub _build_score_map {
    my $self = shift;
    my $address = shift;

    return [
        {
            'woonplaats.naam' => $address->city,
        },
        {
            'openbareruimte.naam' => $address->street,
        },
        {
            'me.postcode' => $address->zipcode
        },
        {
            'me.huisnummer' => $address->street_number
        },
        {
            'me.huisletter' => $address->street_number_letter,
            'me.huisnummertoevoeging' => $address->street_number_suffix
        }
    ];
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
