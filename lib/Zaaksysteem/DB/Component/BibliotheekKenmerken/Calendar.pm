package Zaaksysteem::DB::Component::BibliotheekKenmerken::Calendar;
use Moose::Role;

use DateTime::Format::Strptime;
use DateTime::Format::DateParse;

=head2 format_date

parse something that looks like  '2013-12-26T12:00:00.000'

return '26 december 2013' - Dutch locale

=cut

sub format_date {
    my ($self, $date) = @_;

    my $dt = DateTime::Format::DateParse->parse_datetime($date);

    $dt->set_locale('nl_NL');
    $dt->set_time_zone('UTC')->set_time_zone('Europe/Amsterdam');

    return $dt->day . ' '. $dt->month_name . ' ' . $dt->year;
}

=head2 format_time

Parse something that looks like an ISO timestamp ('1970-01-01T13:20:00.000')

Return '14:20'

=cut

sub format_time {
    my ($self, $time) = @_;

     my $strp = DateTime::Format::Strptime->new(
        pattern   => '%z',
        time_zone => 'UTC',
        locale    => 'nl_NL',
    );

    my $dt = DateTime::Format::DateParse->parse_datetime($time);

    $dt->set_locale('nl_NL');
    $dt->set_time_zone('UTC')->set_time_zone('Europe/Amsterdam');

    return $dt->strftime('%H:%M');
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
