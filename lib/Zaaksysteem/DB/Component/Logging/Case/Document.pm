package Zaaksysteem::DB::Component::Logging::Case::Document;

use Moose::Role;
with qw(Zaaksysteem::Moose::Role::LoggingSubject);


has file => ( is => 'ro', lazy => 1, default => sub {
    my $self = shift;

    $self->rs('File')->find($self->data->{ file_id } // $self->component_id);
});

around TO_JSON => sub {
    my $orig = shift;
    my $self = shift;

    my $data = $self->$orig(@_);

    if ($self->data->{file_name}) {
        $data->{ $_ }       = $self->data->{ $_ } for qw/file_id file_name mimetype/;
    }
    
    return $data;
};

sub event_category { 'document'; }

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 event_category

TODO: Fix the POD

=cut

