package Zaaksysteem::DB::Component::Logging::Case::Document::CopyCaseInto;
use Moose::Role;
with qw(Zaaksysteem::Moose::Role::LoggingSubject);

use HTML::Entities qw(encode_entities);

=head1 NAME

Zaaksysteem::DB::Component::Logging::Case::Document::CopyCaseInto - Event message
handler for document copy_case_into events.

=head1 DESCRIPTION

See L<Zaaksysteem::DB::Component::Logging::Event>.

=head1 METHODS

=head2 onderwerp

Defines the logline for this event. Assumes C<< $self->data >> to have this
structure:

=over

=item target_case_id

=item target_case_subject

=item source_case_id

=item source_case_subject

=item copy_file_id

=item copy_filename

=item copy_version

=item original_file_id

=item original_filename

=item original_version

=back

=cut

sub onderwerp {
    my $self = shift;

    my $msg = sprintf( "Document '%s' (versie %d) gekopieerd naar zaak '%s' (%s), als document '%s'",
        encode_entities($self->data->{original_filename}),
        $self->data->{original_version},
        $self->data->{target_case_id},
        encode_entities($self->data->{target_case_subject} || '<geen onderwerp>' ),
        encode_entities($self->data->{copy_filename}),
    );
    return $msg;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018 Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
