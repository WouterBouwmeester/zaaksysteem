use utf8;
package Zaaksysteem::Schema::ContactRelationshipView;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::ContactRelationshipView

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';
__PACKAGE__->table_class("DBIx::Class::ResultSource::View");

=head1 TABLE: C<contact_relationship_view>

=cut

__PACKAGE__->table("contact_relationship_view");
__PACKAGE__->result_source_instance->view_definition(" SELECT contact_relationship.contact,\n    contact_relationship.contact_uuid,\n    contact_relationship.contact_type,\n    contact_relationship.relation,\n    contact_relationship.relation_uuid,\n    contact_relationship.relation_type\n   FROM contact_relationship\nUNION ALL\n SELECT custom_object_relationship.related_person_id AS contact,\n    custom_object_relationship.related_uuid AS contact_uuid,\n    'person'::text AS contact_type,\n    custom_object_relationship.custom_object_id AS relation,\n    co.uuid AS relation_uuid,\n    'custom_object'::text AS relation_type\n   FROM (custom_object_relationship\n     JOIN custom_object co ON ((custom_object_relationship.custom_object_id = co.id)))\n  WHERE (custom_object_relationship.related_person_id IS NOT NULL)\nUNION ALL\n SELECT custom_object_relationship.related_organization_id AS contact,\n    custom_object_relationship.related_uuid AS contact_uuid,\n    'company'::text AS contact_type,\n    custom_object_relationship.custom_object_id AS relation,\n    co.uuid AS relation_uuid,\n    'custom_object'::text AS relation_type\n   FROM (custom_object_relationship\n     JOIN custom_object co ON ((custom_object_relationship.custom_object_id = co.id)))\n  WHERE (custom_object_relationship.related_organization_id IS NOT NULL)\nUNION ALL\n SELECT custom_object_relationship.related_employee_id AS contact,\n    custom_object_relationship.related_uuid AS contact_uuid,\n    'employee'::text AS contact_type,\n    custom_object_relationship.custom_object_id AS relation,\n    co.uuid AS relation_uuid,\n    'custom_object'::text AS relation_type\n   FROM (custom_object_relationship\n     JOIN custom_object co ON ((custom_object_relationship.custom_object_id = co.id)))\n  WHERE (custom_object_relationship.related_employee_id IS NOT NULL)");

=head1 ACCESSORS

=head2 contact

  data_type: 'integer'
  is_nullable: 1

=head2 contact_uuid

  data_type: 'uuid'
  is_nullable: 1
  size: 16

=head2 contact_type

  data_type: 'text'
  is_nullable: 1

=head2 relation

  data_type: 'integer'
  is_nullable: 1

=head2 relation_uuid

  data_type: 'uuid'
  is_nullable: 1
  size: 16

=head2 relation_type

  data_type: 'text'
  is_nullable: 1

=cut

__PACKAGE__->add_columns(
  "contact",
  { data_type => "integer", is_nullable => 1 },
  "contact_uuid",
  { data_type => "uuid", is_nullable => 1, size => 16 },
  "contact_type",
  { data_type => "text", is_nullable => 1 },
  "relation",
  { data_type => "integer", is_nullable => 1 },
  "relation_uuid",
  { data_type => "uuid", is_nullable => 1, size => 16 },
  "relation_type",
  { data_type => "text", is_nullable => 1 },
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2020-10-13 14:30:36
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:Va2qgld007XtCWo1fLQaHw

__PACKAGE__->load_components(
  "+Zaaksysteem::DB::Component::ContactRelationView",
  __PACKAGE__->load_components(),
);


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2020, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
