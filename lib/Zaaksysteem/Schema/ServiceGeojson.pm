use utf8;
package Zaaksysteem::Schema::ServiceGeojson;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::ServiceGeojson

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<service_geojson>

=cut

__PACKAGE__->table("service_geojson");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_nullable: 0

=head2 uuid

  data_type: 'uuid'
  is_nullable: 0
  size: 16

=head2 geo_json

  data_type: 'jsonb'
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_nullable => 0 },
  "uuid",
  { data_type => "uuid", is_nullable => 0, size => 16 },
  "geo_json",
  { data_type => "jsonb", is_nullable => 0 },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<service_geojson_uuid_key>

=over 4

=item * L</uuid>

=back

=cut

__PACKAGE__->add_unique_constraint("service_geojson_uuid_key", ["uuid"]);

=head1 RELATIONS

=head2 service_geojson_relationships

Type: has_many

Related object: L<Zaaksysteem::Schema::ServiceGeojsonRelationship>

=cut

__PACKAGE__->has_many(
  "service_geojson_relationships",
  "Zaaksysteem::Schema::ServiceGeojsonRelationship",
  { "foreign.service_geojson_id" => "self.id" },
  undef,
);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2020-09-02 13:42:41
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:RQLJGsEhjyoYqMf3sLG0iw


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2020, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
