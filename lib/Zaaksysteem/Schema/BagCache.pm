use utf8;
package Zaaksysteem::Schema::BagCache;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::BagCache

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<bag_cache>

=cut

__PACKAGE__->table("bag_cache");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_nullable: 0

=head2 bag_type

  data_type: 'text'
  is_nullable: 0
  original: {data_type => "varchar"}

=head2 bag_id

  data_type: 'char'
  is_nullable: 0
  size: 16

=head2 bag_data

  data_type: 'jsonb'
  is_nullable: 0

=head2 date_created

  data_type: 'timestamp'
  default_value: timezone('UTC'::text, current_timestamp)
  is_nullable: 0
  timezone: 'UTC'

=head2 last_modified

  data_type: 'timestamp'
  default_value: timezone('UTC'::text, current_timestamp)
  is_nullable: 0
  timezone: 'UTC'

=head2 refresh_after

  data_type: 'timestamp'
  default_value: (timezone('UTC'::text, current_timestamp) + '24:00:00'::interval)
  is_nullable: 0
  timezone: 'UTC'

=cut

__PACKAGE__->add_columns(
  "id",
  { data_type => "integer", is_nullable => 0 },
  "bag_type",
  {
    data_type   => "text",
    is_nullable => 0,
    original    => { data_type => "varchar" },
  },
  "bag_id",
  { data_type => "char", is_nullable => 0, size => 16 },
  "bag_data",
  { data_type => "jsonb", is_nullable => 0 },
  "date_created",
  {
    data_type     => "timestamp",
    default_value => \"timezone('UTC'::text, current_timestamp)",
    is_nullable   => 0,
    timezone      => "UTC",
  },
  "last_modified",
  {
    data_type     => "timestamp",
    default_value => \"timezone('UTC'::text, current_timestamp)",
    is_nullable   => 0,
    timezone      => "UTC",
  },
  "refresh_after",
  {
    data_type     => "timestamp",
    default_value => \"(timezone('UTC'::text, current_timestamp) + '24:00:00'::interval)",
    is_nullable   => 0,
    timezone      => "UTC",
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<bag_cache_bag_type_bag_id_key>

=over 4

=item * L</bag_type>

=item * L</bag_id>

=back

=cut

__PACKAGE__->add_unique_constraint("bag_cache_bag_type_bag_id_key", ["bag_type", "bag_id"]);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2021-08-27 13:13:23
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:dDYAwX2UxQ85vmToXSLagw

__PACKAGE__->inflate_column('bag_data', {
    inflate => sub { JSON::XS->new->decode(shift // '{}') },
    deflate => sub { JSON::XS->new->encode(shift // {}) },
});

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2021, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
