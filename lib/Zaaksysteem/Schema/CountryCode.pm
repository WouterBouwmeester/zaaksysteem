use utf8;
package Zaaksysteem::Schema::CountryCode;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

=head1 NAME

Zaaksysteem::Schema::CountryCode

=cut

use strict;
use warnings;


=head1 BASE CLASS: L<Zaaksysteem::Result>

=cut

use base 'Zaaksysteem::Result';

=head1 TABLE: C<country_code>

=cut

__PACKAGE__->table("country_code");

=head1 ACCESSORS

=head2 id

  data_type: 'integer'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'country_code_id_seq'

=head2 dutch_code

  data_type: 'integer'
  is_nullable: 0

=head2 label

  data_type: 'text'
  is_nullable: 0

=head2 uuid

  data_type: 'uuid'
  default_value: uuid_generate_v4()
  is_nullable: 0
  size: 16

=head2 historical

  data_type: 'boolean'
  default_value: false
  is_nullable: 0

=cut

__PACKAGE__->add_columns(
  "id",
  {
    data_type         => "integer",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "country_code_id_seq",
  },
  "dutch_code",
  { data_type => "integer", is_nullable => 0 },
  "label",
  { data_type => "text", is_nullable => 0 },
  "uuid",
  {
    data_type => "uuid",
    default_value => \"uuid_generate_v4()",
    is_nullable => 0,
    size => 16,
  },
  "historical",
  { data_type => "boolean", default_value => \"false", is_nullable => 0 },
);

=head1 UNIQUE CONSTRAINTS

=head2 C<country_code_dutch_code_key>

=over 4

=item * L</dutch_code>

=back

=cut

__PACKAGE__->add_unique_constraint("country_code_dutch_code_key", ["dutch_code"]);


# Created by DBIx::Class::Schema::Loader v0.07049 @ 2021-05-04 15:46:39
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:jznvIEgz7lNgVTgcdrGJ6A


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2021, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
