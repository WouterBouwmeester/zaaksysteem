package Zaaksysteem::XML::TopX::Instance;
use Moose;

=head1 NAME

Zaaksysteem::XML::Zaaksysteem::Instance - Zaaksysteem's implementation of Zaaksysteem XSD

=head1 SYNOPSIS

=cut

use File::Spec::Functions qw(catfile);
use BTTW::Tools;

with 'Zaaksysteem::XML::Compile::Instance';

has name => (
    is      => 'ro',
    default => 'topx',
);

has schemas => (
    is      => 'ro',
    lazy    => 1,
    isa     => 'ArrayRef[Str]',
    default => sub {
        my $self = shift;

        my $basedir = catfile($self->home, qw(share xsd topx));

        my @schemas;
        foreach (qw(ToPX-2.3_2)) {
            push(@schemas, catfile($basedir, "$_.xsd"));
        }
        return \@schemas;
    }
);

has '+schema' => (
    is      => 'ro',
    isa     => 'XML::Compile::Cache',
    lazy    => 1,
    builder => 'get_schema',
);

has elements => (
    is      => 'ro',
    lazy    => 1,
    isa     => 'ArrayRef',
    default => sub {
        return [
            {
                element => '{http://www.nationaalarchief.nl/ToPX/v2.3}ToPX',
                compile => 'RW',
                method  => 'tmlo_export',
            },
        ];
    },
);

has _reader_config => (
    is      => 'ro',
    default => sub { return [] },
);

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2020, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
