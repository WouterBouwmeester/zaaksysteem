package Zaaksysteem::XML::RIP::Instance;
use Moose;

=head1 NAME

Zaaksysteem::XML::RIP - Zaaksysteem's implementation of RIP

=head1 DESCRIPTION

RIP is an unsupported standard of the Dutch National Archive which is in use by
local e-depots.

=head1 SYNOPSIS

=cut

use File::Spec::Functions qw(catfile);
use BTTW::Tools;

with 'Zaaksysteem::XML::Compile::Instance';

has '+name' => (
    default => 'rip',
);

has schemas => (
    is      => 'ro',
    lazy    => 1,
    isa     => 'ArrayRef[Str]',
    default => sub {
        my $self = shift;

        my $basedir = catfile($self->home, qw(share xsd rip));

        my @schemas;
        foreach (qw(RIP_v0.3)) {
            push(@schemas, catfile($basedir, "$_.xsd"));
        }
        return \@schemas;
    }
);

has '+schema' => (
    is      => 'ro',
    isa     => 'XML::Compile::Cache',
    lazy    => 1,
    builder => 'get_schema',
);

has elements => (
    is      => 'ro',
    lazy    => 1,
    isa     => 'ArrayRef',
    default => sub {
        return [
            {
                element => '{http://www.nationaalarchief.nl/RIP/v0.3}recordInformationPackage',
                compile => 'RW',
                method  => 'tmlo_export',
            },
        ];
    },
);

has _reader_config => (
    is      => 'ro',
    default => sub { return [] },
);

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2020, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
