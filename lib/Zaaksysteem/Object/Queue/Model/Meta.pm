package Zaaksysteem::Object::Queue::Model::Meta;

use Moose::Role;

use BTTW::Tools;
use List::MoreUtils qw[any];

=head1 NAME

Zaaksysteem::Object::Queue::Model::Meta - Meta queue item handlers

=head1 DESCRIPTION

=head1 METHODS

=head2 run_ordered_item_set

=cut

sig run_ordered_item_set => 'Zaaksysteem::Backend::Object::Queue::Component';

sub run_ordered_item_set {
    my $self = shift;
    my $item = shift;

    for my $item_id (@{ $item->data->{ item_ids } }) {
        my $item = $self->fetch_item($item_id);

        my $sub_item = $self->run_item_externally($item);

        if ($sub_item->is_failed) {
            $self->log->info(
                sprintf(
                    "Item %s failed: %s [ %s ]",
                    $sub_item->type, $sub_item->label, $sub_item->id
                )
            );
        }
    }

}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2016, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
