package Zaaksysteem::Object::Queue::Model::BAG;
use Moose::Role;

requires qw(build_resultset);

use BTTW::Tools;

with 'MooseX::Log::Log4perl';

=head1 NAME

Zaaksysteem::Object::Queue::Model::BAG - BAG queue handlers

=head1 DESCRIPTION

=head1 METHODS

=head2 refresh_bag_cache

Refresh a BAG cache entry

=cut

sig refresh_bag_cache => 'Zaaksysteem::Backend::Object::Queue::Component';

sub refresh_bag_cache {
    my ($self, $item) = @_;

    my $schema = $item->result_source->schema;
    my $data = $item->data;

    my $bag_object_type = $data->{object_type};
    my $bag_object_id = $data->{object_id};

    my $model = Zaaksysteem::Geo::BAG::Model->new(schema => $schema);

    $model->refresh($bag_object_type, $bag_object_id);

    return 1;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2021, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
