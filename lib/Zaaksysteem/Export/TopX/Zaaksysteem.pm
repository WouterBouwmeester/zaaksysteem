package Zaaksysteem::Export::TopX::Zaaksysteem;
use Zaaksysteem::Moose;

my @roles = qw(
    Zaaksysteem::Export::TopX::Base
);


=head1 NAME

Zaaksysteem::Export::TopX::Zaaksysteem - Export cases as TopX in a ZS style

=head1 DESCRIPTION

This model hides the logic for developers to transform data to CSV.

=head1 SYNOPSIS

    use Zaaksysteem::Export::TopX::Zaaksysteem;

    my $model = Zaaksysteem::Export::TopX::Zaaksysteem->new(
      schema  => $self->schema,
      user    => $user,
      objects => $results,
      tar     => $tar,
      $mapping ? (attribute_mapping => $mapping) : (),
    );

    $model->export();

=cut

sub _export_case_xml {
    my ($self, $case, $xml) = @_;

    $self->add_scalar_to_export($case->get_column('uuid') . "/case.xml", $xml);
    return 1;
}

sub _export_case_document_xml {
    my ($self, $case, $file, $xml) = @_;

    my $basedir = $case->get_column('uuid');

    my $uuid = $file->uuid;
    my $name = sprintf("%s/%s.xml", $basedir, $uuid);
    $self->add_scalar_to_export($name, $xml);

    $name = sprintf("%s/%s.blob", $basedir, $uuid);
    $self->add_fh_to_export($name, $file->filestore->get_path);
    return 1;
}

with @roles;

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2020, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
