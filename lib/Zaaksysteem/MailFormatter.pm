package Zaaksysteem::MailFormatter;
use Moose;

use Mail::Track;
use BTTW::Tools;

=head2 format

Format an the e-mail header for outputting it to a file.

=cut

define_profile format => (
    required => [qw/from to subject body/],
    optional => [qw/attachments cc bcc/],
);

sub format {
    my $self   = shift;
    my $params = assert_profile(shift)->valid;

    my $attachments
        = $params->{attachments}
        ? join "\n", map { $_->{filename} } @{ $params->{attachments} }
        : 'geen';

    my $str = sprintf("Van: %s\nAan: %s\n", $params->{from}, $params->{to});
    if ($params->{cc}) {
        $str .= "CC: $params->{cc}\n";
    }
    if ($params->{bcc}) {
        $str .= "BCC: $params->{bcc}\n";
    }

    $str .= sprintf("Onderwerp: %s\n\nBijlagen: %s\n\n%s", $params->{subject}, $attachments,    $params->{body});
    return $str;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
