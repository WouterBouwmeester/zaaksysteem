package Zaaksysteem::API::v1::Serializer::Reader::Plain;
use Moose;

sub grok {
    my ($class, $object) = @_;

    return unless ref $object;

    if (ref($object) eq 'HASH' || ref($object) eq 'ARRAY') {
        return sub {
            my $serializer = shift;
            my $object     = shift;

            return $object;
        }
    }

    return;
};

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a
look at the L<LICENSE|Zaaksysteem::LICENSE> file.
