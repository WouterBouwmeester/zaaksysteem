package Zaaksysteem::Environment;
use warnings;
use strict;

=head1 NAME

Zaaksysteem::Environment - Some global variables that should be available throughout all of Zaaksysteem code

=head1 DESCRIPTION

This package holds global variables (not constants!) that can be used
throughout the Zaaksysteem code base, where passing around (parts of) request
context (Catalyst's C<$c>) is impossible or cumbersome.

Before adding a new variable here, please consult your colleagues: globals like
this can make code very hard to test.

=head1 VARIABLES

=head2 $REQUEST_ID

The identifier of the current request (if any).

=cut

our $REQUEST_ID;

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
