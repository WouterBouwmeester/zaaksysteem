package Zaaksysteem::Controller::Redirect;
use Zaaksysteem::Moose;
use feature 'state';
use Zaaksysteem::Types qw(NonEmptyStr UUID PositiveNumber CaseTabType);

BEGIN { extends 'Zaaksysteem::Controller' }

=head1 NAME

Zaaksysteem::Controller::Redirect - Redirects to other parts of the system

=head1 DESCRIPTION

Redirection endpoints (UUID + type based) to "old" pages in the system.

=head1 ACTIONS

=head2 base

Base method for redirection controllers.

=cut

sub base : Chained('/') : PathPart('redirect') : CaptureArgs(0) {}

define_profile case => (
    required => {
        uuid => UUID,
    },
    optional => {
        tab => CaseTabType,
    }
);

sub case : Chained('base') : PathPart('case') : Args(0) : DB('RO') {
    my ($self, $c) = @_;

    my $args = assert_profile($c->req->params)->valid;
    my $case = $c->model('DB::Zaak')->search_restricted('read')
        ->search_rs({ uuid => $args->{uuid} })->first;

    throw(
        "redirect/case/not_found",
        "Case not found with $args->{uuid}"
    ) if !$case;

    state %tabs = (
        document      => 'documenten/',
        timeline      => 'timeline/',
        communication => 'communicatie/',
        relation      => 'relaties',
        location      => 'locaties'
    );

    my @uri = qw(intern zaak);
    unshift(@uri, "");
    push(@uri, $case->id);

    if (my $k = $args->{tab}) {
        push(@uri, $tabs{$k});
    }

    $c->redirect($c->uri_for(join("/", @uri)));
}

=head2 contact_page

Redirect to the contact page for a given contact.

=head2 Arguments

=over

=item * type

One of 'employee', 'organization', 'person'. Type of contact to redirect to.

=item * uuid

UUID of the contact.

=back

=cut

my %subject_v1_type_map = (
    person   => 'natuurlijk_persoon',
    employee => 'medewerker',
    company  => 'bedrijf',
);

my %subject_v2_type_map = (
    person   => 'person',
    employee => 'employee',
    company  => 'organization',
);

sub _make_uri {
    my ($c, $version, $type, $id, $uuid) = @_;

    if (defined($version) && ($version eq 'v1')) {
        return $c->uri_for("/betrokkene/$id", {gm => 1, type => $subject_v1_type_map{$type}});
    }

    return $c->uri_for("/main/contact-view/$subject_v2_type_map{$type}/$uuid")
}


define_profile contact_page => (
    optional => {
        uuid    => UUID,
        id      => PositiveNumber,
        type    => NonEmptyStr,
        version => NonEmptyStr,
    },
    dependency_groups => { id_type_group => ['id', 'type'], },
);

sub contact_page : Chained('base') : PathPart('contact_page') : Args(0) : DB('RO') {
    my ($self, $c) = @_;

    my $args = assert_profile($c->req->params)->valid;

    my ($betrokkene_db_row, $betrokkene_type);

    if (!exists $args->{type}) {

        my $subject = $c->model('BR::Subject')->find($args->{uuid});
        throw(
            "redirect/contact_page/contact_not_foud",
            "Contact with UUID '$args->{uuid}' not found."
        ) unless $subject;

        my $type = $subject->subject_type;

        $c->res->redirect(
            _make_uri($c, $args->{version}, $type, $subject->subject->_table_id, $args->{uuid})
        );
        $c->res->body('');
        return;
    }
    elsif ($args->{type} eq 'employee') {
        $betrokkene_db_row = $c->model('DB::Subject')->search({
            'subject_type' => 'employee',
            'id'         => $args->{id},
        })->single;

        $betrokkene_type = 'employee';
    }
    elsif ($args->{type} eq 'organization') {
        $betrokkene_db_row = $c->model('DB::Bedrijf')->search({
            'id' => $args->{id},
            'deleted_on' => undef,
        })->single;

        $betrokkene_type = 'company';
    }
    elsif ($args->{type} eq 'person') {
        $betrokkene_db_row = $c->model('DB::NatuurlijkPersoon')->search({
            'id' => $args->{id},
            'deleted_on' => undef,
        })->single;

        $betrokkene_type = 'person';
    }
    else {
        throw(
            "redirect/contact_page/unknown_contact_type",
            "Unknown contact type '$args->{type}'.",
        );
    }

    if (not defined $betrokkene_db_row) {
        throw(
            "redirect/contact_page/contact_not_foud",
            "Contact with UUID '$args->{uuid}' not found."
        );
    }

    my $betrokkene_uuid = $betrokkene_db_row->uuid;
    my $betrokkene_id = $betrokkene_db_row->id;

    $c->redirect(
        _make_uri($c, $args->{version}, $betrokkene_type, $betrokkene_id, $betrokkene_uuid)
    );
 
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
