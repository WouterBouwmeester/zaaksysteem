package Zaaksysteem::Controller::Search::Export;
use Moose;

use Zaaksysteem::Search::Object;
use BTTW::Tools;

BEGIN { extends 'Zaaksysteem::General::ZAPIController' }

define_profile export_by_zql => (
    required => {
        zql => 'Str',
    },
);

sub export_by_zql :Chained("/search/base") :PathPart("export/zql") :Args() {
    my ($self, $c, $filetype) = @_;

    my $params = assert_profile($c->req->params)->valid;

    my $model = Zaaksysteem::Search::Object->new(
        schema       => $c->model('DB')->schema,
        user         => $c->user,
        query        => $params->{zql},
        object_model => $c->model('Object'),
    );

    my $queue = $c->model('Queue');

    my $item = $queue->create_item(
        {
            label => "Document list export of cases",
            type  => 'export_document_list',
            data  => {
                subject_id => $model->user->id,
                zql        => $model->query,
            },
            metadata => {
                request_id           => $c->get_zs_session_id,
                require_object_model => 0,
            }
        }
    );
    $queue->queue_item($item) if $item;
    $c->stash->{zapi} = [ { success => \1 } ];
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, 2020 Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
