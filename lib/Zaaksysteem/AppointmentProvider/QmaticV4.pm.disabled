package Zaaksysteem::AppointmentProvider::QmaticV4;
use Moose;
use namespace::autoclean;

with qw(
    Zaaksysteem::AppointmentProvider
    MooseX::Log::Log4perl
);

=head1 NAME

Zaaksysteem::AppointmentProvider::QmaticV4 - Appointment provider plugin for te Qmatic v4 API

=head1 DESCRIPTION

Adds support for using the (deprecated) v4 API of the
L<Qmatic|http://www.qmatic.com/> application for appointments.

=cut

use Zaaksysteem::Tools::SysinModules qw(:certificates);
use Zaaksysteem::ZAPI::Form::Field;

=head1 METHODS

=head2 shortname

Always returns the string C<qmaticv4>.

=cut

sub shortname { "qmaticv4" }

=head2 label

Always returns the string C<Qmatic v4>.

=cut

sub label { "Qmatic v4" }

my @CONFIGURATION_ITEMS = (
    Zaaksysteem::ZAPI::Form::Field->new(
        name        => 'interface_qmaticv4_url',
        type        => 'text',
        label       => 'SOAP endpoint',
        required    => 1,
        description => '<p>Qmatic SOAP server URL.</p>',
    ),
    ca_certificate(name => 'qmaticv4_ca_certificate'),
);

=head2 configuration_items

Returns a list of configuration items (L<Zaaksysteem::ZAPI::Form::Field>
instances) that are necessary to configure the appointment interface to
use the Qmatic v4 API.

=cut

sub configuration_items {
    return @CONFIGURATION_ITEMS;
}

__PACKAGE__->meta->make_immutable();

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
