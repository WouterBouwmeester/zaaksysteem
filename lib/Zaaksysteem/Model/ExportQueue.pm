package Zaaksysteem::Model::ExportQueue;
use Moose;
use namespace::autoclean;

extends 'Catalyst::Model::Factory::PerRequest';

use BTTW::Tools;

=head1 NAME

Zaaksysteem::Model::ExportQueue - Catalyst model factory for
L<Zaaksysteem::Store::ExportQueue>

=head1 SYNOPSIS

    my $model = $c->model('ExportQueue');

=cut

__PACKAGE__->config(
    class       => 'Zaaksysteem::Export::Model',
    constructor => 'new',
);

=head1 METHODS

=head2 prepare_arguments

Prepares the arguments to create a new L<Zaaksysteem::Store::ExportQueue> instance.

=cut

sub prepare_arguments {
    my ($self, $c) = @_;

    return {
        schema     => $c->model('DB')->schema,
        request_id => $c->get_session_id // '',
        uri        => $c->uri_for('/exportqueue'),
        $c->user_exists ? (user => $c->user) : (),
    };
}

__PACKAGE__->meta->make_immutable;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
