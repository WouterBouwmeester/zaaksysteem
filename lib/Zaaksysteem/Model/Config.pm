package Zaaksysteem::Model::Config;

use Moose;
use namespace::autoclean;

extends 'Catalyst::Model::Factory::PerRequest';

__PACKAGE__->config(
    class => 'Zaaksysteem::Config::Model',
    constructor => 'new'
);

=head1 NAME

Zaaksysteem::Model::Object - Catalyst model factory for
L<Zaaksysteem::Config::Model>.

=head1 DESCRIPTION

=cut

use BTTW::Tools;
use Syzygy::Object::Model;
use Zaaksysteem::Object::ConstantTables qw[
    ZAAKSYSTEEM_CONFIG_CATEGORIES
    ZAAKSYSTEEM_CONFIG_DEFINITIONS
];
use Zaaksysteem::Object::Reference;
use Zaaksysteem::Object::Types::Config::Category;
use Zaaksysteem::Object::Types::Config::Definition;
use Zaaksysteem::Tools::SimpleStore;

sub prepare_arguments {
    my ($self, $c, @args) = @_;

    my $category_store = Zaaksysteem::Tools::SimpleStore->new(
        object_type => 'config/category'
    );

    my $definition_store = Zaaksysteem::Tools::SimpleStore->new(
        object_type => 'config/definition'
    );

    for my $category_spec (@{ ZAAKSYSTEEM_CONFIG_CATEGORIES() }) {
        # Bind category_id to silence BTTW::Tools::sig retval checks
        my $category_id = $category_store->create(
            Zaaksysteem::Object::Types::Config::Category->new($category_spec)
        );
    }

    for my $definition_spec (@{ ZAAKSYSTEEM_CONFIG_DEFINITIONS() }) {
        my $definition_args = { %{ $definition_spec } };

        $definition_args->{ category } = Zaaksysteem::Object::Reference::Instance->new(
            type => 'config/category',
            id => delete $definition_args->{ category_id }
        );

        # Bind definition_id to silence BTTW::Tools::sig retval checks
        my $definition_id = $definition_store->create(
            Zaaksysteem::Object::Types::Config::Definition->new($definition_args)
        );
    }

    return {
        schema           => $c->model('DB')->schema,
        item_rs          => $c->model('DB::Config'),
        subject_model    => $c->model('BR::Subject'),
        definition_store => $definition_store,
        category_store   => $category_store,

        bibliotheek_notificaties_rs => $c->model('DB::BibliotheekNotificaties'),

        groups_rs        => $c->model('DB::Groups'),
        roles_rs         => $c->model('DB::Roles'),
    };
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
