package Zaaksysteem::Model::ConceptCase;

use Moose;
use namespace::autoclean;

extends 'Catalyst::Model::Factory::PerRequest';

__PACKAGE__->config(
    class       => 'Zaaksysteem::Zaken::Concept',
    constructor => 'new'
);

=head1 NAME

Zaaksysteem::Model::ConceptCase - Catalyst model factory for
L<Zaaksysteem::Zaken::ConceptCase> instances.

=head1 SYNOPSIS

    my $model = $c->model('ConceptCase');

=head1 METHODS

=head2 prepare_arguments

Prepares the constructor arguments for L<Zaaksysteem::Zaken::ConceptCase>.

=cut

sub prepare_arguments {
    my ($self, $c, $args) = @_;

    my $ret = {
        concept_rs => $c->model('DB::ZaakOnafgerond'),
    };

    return $ret;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
