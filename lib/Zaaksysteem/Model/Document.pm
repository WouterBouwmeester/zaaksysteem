package Zaaksysteem::Model::Document;
use Moose;
use namespace::autoclean;

extends 'Catalyst::Model::Factory::PerRequest';

__PACKAGE__->config(class => 'Zaaksysteem::Document::Model',);

=head1 NAME

Zaaksysteem::Model::Document - Catalyst model factory for
L<Zaaksysteem::Document::Model>.

=head1 SYNOPSIS

    my $document_model = $c->model('Document');

=head1 METHODS

=head2 prepare_arguments

Prepares the arguments for L<<Zaaksysteem::Document::Model->new>>.

=cut

sub prepare_arguments {
    my ($self, $c) = @_;

    return {
        rs_file      => $c->model('DB::File'),
        rs_filestore => $c->model('DB::Filestore'),
        $c->user_exists ? (
            subject      => $c->user,
        ) : (),
        storage      => $c->model('DB')->schema->storage,
    };
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
