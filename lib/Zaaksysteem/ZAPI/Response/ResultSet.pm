package Zaaksysteem::ZAPI::Response::ResultSet;

use Moose::Role;
use BTTW::Tools;

requires qw(
    log
);

sub from_resultset {
    my $self        = shift;
    my $resultset   = shift;

    ### Preperation
    die('Not a valid ResultSet: ' . ref($resultset))
        unless UNIVERSAL::isa($resultset, 'DBIx::Class::ResultSet');

    $self->_input_type('resultset');

    my %options;

    ### Ordering
    ### TODO: Create features in resultset (no_ordering)
    if ($self->order_by && $resultset->result_source->name ne 'object_data') {
        $self->order_by_direction('desc') unless $self->order_by_direction;

        $options{order_by} = {
            '-' . $self->order_by_direction => $self->order_by,
        };
    }

    $options{page} = $self->page_current if $self->page_current;
    $options{rows} = $self->page_size if $self->page_size;

    $resultset = $resultset->search_rs(undef, \%options) if %options;

    if ($self->no_pager) {
        my $count = $resultset->pager->total_entries;
        $resultset = $resultset->search(undef, { rows => $count });
    }

    $self->_generate_paging_attributes(
        $resultset->pager,
    );

    $self->result($resultset);

    $self->_validate_response;

    return $self->response;
}

around from_unknown => sub {
    my $orig        = shift;
    my $self        = shift;
    my ($data)      = @_;

    if (
        UNIVERSAL::isa($data, 'DBIx::Class::ResultSet')
    ) {
        $self->from_resultset(@_);
    }

    $self->$orig( @_ );
};

around '_validate_response' => sub {
    my $method      = shift;
    my $self        = shift;

    if ($self->_input_type eq 'resultset') {
        die('Invalid format for result attribute') unless UNIVERSAL::isa(
            $self->result,
            'DBIx::Class::ResultSet'
        );
    }

    return $self->$method(@_);
};

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 from_resultset

TODO: Fix the POD

=cut

