package Zaaksysteem::EventLog::Model;

use Moose;
use namespace::autoclean;

use BTTW::Tools;
use File::Temp;
use List::Util qw(any none);
use Zaaksysteem::Tools::EsQuery qw(parse_es_query);

with 'MooseX::Log::Log4perl';

=head1 NAME

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 INSTRUCTIONS

=head2 FORM LAYOUT

=head1 METHODS

=cut

=head1 ATTRIBUTES

=cut

has schema => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Schema',
    required => 1,
);

has object_model => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Object::Model',
    required => 1,
);

has subject_model => (
    is       => 'ro',
    isa      => 'Zaaksysteem::BR::Subject',
    required => 1,
);

has betrokkene_model => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Zaken::ResultSetBetrokkenen',
    required => 1,
);

has user => (
    is       => 'ro',
    isa      => 'Zaaksysteem::Schema::Subject',
    required => 1,
);

has eventlog => (
    is       => 'ro',
    isa      => 'Zaaksysteem::DB::ResultSet::Logging',
    lazy      => 1,
    builder  => '_build_eventlog'
);

has fh => (
    is      => 'ro',
    isa     => 'File::Temp',
    lazy    => 1,
    default => sub {
    }
);

has environment => (
    is => 'ro',
    isa => 'Str',
    predicate => 'has_environment',
);

has zs_version => (
    is => 'ro',
    isa => 'Str',
    predicate => 'has_zs_version',
);

sub _build_eventlog {
    my $self = shift;
    return $self->schema->resultset('Logging');
}

sub export {
    my ($self, $search_params) = @_;

    my $fh = File::Temp->new();
    $self->export_with_fh($fh, $search_params);
    return $fh;
}

sub export_with_fh {
    my ($self, $fh, $search_params, $opts) = @_;

    if (!$self->has_zs_version || !$self->environment) {
        die "a horrible death";
    }

    my $rs = $self->search($search_params);
    my $tar = Archive::Tar::Stream->new(outfh => $fh);

    $self->object_model->export_rs_as_object(
        resultset  => $rs,
        tar_handle => $tar,
        metadata   => {
            user        => $self->user->username,
            environment => $self->environment,
            zs_version  => $self->zs_version,
        }
    );
    $tar->FinishTar();
    return 1;
}

sig search => 'HashRef';

sub search {
    my ($self, $search_params) = @_;

    my $rs = $self->eventlog->search_rs(
        {
            deleted_on => undef,
            restricted => 0,

            # Do nothing, these types are legacy and need be migrated.
            # This falls out of scope for the contactdossier epic
            event_type => { '!=' => undef },
        },
        { order_by => { -desc => 'id' } }
    );

    my $query = {};

    delete $search_params->{$_} for qw(page rows_per_page);

    if (keys %{$search_params}) {
        my $es = parse_es_query($search_params)->{query};

        if (defined $es->{match}{event_type}) {
            $query->{event_type} = [-in => $es->{match}{event_type}];
        }

        if (defined $es->{match}{keyword}) {
            my $like = "%$es->{match}{keyword}%";

            $query->{'-and'} = { -or => [
                event_data => { ilike => $like },
                onderwerp => { ilike => $like },
            ]},
        }

        if (defined $es->{match}{case_id}) {
            if ($es->{match}{case_id} =~ /^[0-9]+$/) {
                # Restrict $rs to the specified case_id
                $rs = $rs->search_rs({ zaak_id => $es->{match}{case_id} });
            } else {
                $rs = $rs->search_rs(\'1 = 0');
            }
        }

        if (defined $es->{match}{timeline} || defined $es->{match}{subject}) {
            my $object = $self->_find_subject_by_uuid($es->{match}{timeline}//
                $es->{match}{subject});

            if ($object) {
                my $bid = $object->old_subject_identifier;

                my (undef, $type, $id) = split(/\-/, $bid);
                my $rs_related_cases = $self->betrokkene_model->search_rs(
                    {
                        betrokkene_type      => $type,
                        gegevens_magazijn_id => $id,
                        deleted              => undef
                    },
                );

                $query->{'-or'} = [
                    { created_by  => $object->old_subject_identifier },
                    { created_for => $object->old_subject_identifier },
                    { modified_by => $object->old_subject_identifier },
                ];

                if (defined $es->{match}{timeline}) {
                    my $related_cases_query = $rs_related_cases->get_column('zaak_id')->as_query;
                    push @{ $query->{'-or'} }, {
                        zaak_id => { -in =>  $related_cases_query }
                    };
                }
            }
            else {
                $query = $self->object_model_to_search_query(
                    $es->{match}{timeline}
                );
            }
        }
        elsif (defined $es->{match}{object}) {
            my $object = $self->_find_subject_by_uuid($es->{match}{object});
            throw(
                'api/v1/event_log/subject_object_not_yet_supported',
                'Unable to construct query for subject object instances'
            ) if $object;

            $query = $self->object_model_to_search_query($es->{match}{object});
        }
    }

    return $rs->search_rs($query);
}

sub _find_subject_by_uuid {
    my ($self, $uuid) = @_;
    return $self->subject_model->find($uuid);
}


sub object_model_to_search_query {
    my $self = shift;
    my $uuid = shift;

    my $object = $self->object_model->retrieve(uuid => $uuid);
    if ($object->type eq 'case') {
        return {zaak_id => $object->case_number};
    }
    return {object_uuid => $uuid};
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2020, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
