package Zaaksysteem::CLI::Knife::BibliotheekKenmerken;
use Moose::Role;
use Zaaksysteem::CLI::Knife::Action;
use BTTW::Tools;

my $knife = 'bibliotheek';

=head1 NAME

Zaaksysteem::CLI::Knife::BibliotheekKenmerken - BibliotheekKenmerken CLI actions

=cut

register_knife $knife => (description => "Bibliotheek related functions");

register_category kenmerken => (
    knife       => $knife,
    description => "Kenmerken",
);

register_action list => (
    knife       => $knife,
    category    => 'kenmerken',
    description => 'List kenmerkenx ',
    run         => sub {
        my $self = shift;
        my (@params) = @_;

        my $rs = $self->schema->resultset('BibliotheekKenmerken')->search_rs({ deleted => undef }, { order_by => { '-desc' => 'magic_string' } });
        while (my $k = $rs->next) {
            $self->log->info($self->_list_one($k));
        }
    }
);

register_action list_duplicates => (
    knife       => $knife,
    category    => 'kenmerken',
    description => 'List duplicate magic strings',
    run         => sub {
        my $self = shift;
        my (@params) = @_;

        my $rs = $self->get_duplicate_magicstrings();
        while (my $k = $rs->next) {
            $self->log->info($self->_list_one($k));
        }
    }
);

register_action merge_duplicates => (
    knife       => $knife,
    category    => 'kenmerken',
    description => 'Merge duplicate magic strings',
    run         => sub {
        my $self = shift;
        my (@params) = @_;

        my $p = $self->get_knife_params;

        if (!$p->{from} || !$p->{to}) {
            throw("zsknife/args", "Invalid arguments, missing to or from");
        }

        my $rs   = $self->schema->resultset('BibliotheekKenmerken');
        my $from = $rs->find($p->{from});
        my $to   = $rs->find($p->{to});

        if (!$from || !$to) {
            throw("zsknife/attributes", "Unable to find BibliotheekKenmerken");
        }

        if (   $from->magic_string ne $to->magic_string
            || $from->value_type ne $to->value_type)
        {
            throw("zsknife/attributes/not_identical", "Unable to merge, attributes differ from magic_string or type");
        }
        if ($from->deleted || $to->deleted) {
            throw("zsknife/attributes/deleted", "Attribute is already deleted");
        }

        my $zk = $self->schema->resultset('ZaakKenmerk')->search({ bibliotheek_kenmerken_id => $from->id });
        $zk->update({ bibliotheek_kenmerken_id => $to->id });

        my $ztk = $self->schema->resultset('ZaaktypeKenmerken')->search({ bibliotheek_kenmerken_id => $from->id });
        $ztk->update({ bibliotheek_kenmerken_id => $to->id });
        $from->update({ magic_string => sprintf("migrated_%s_%s", $to->id, $to->magic_string) });
    }
);

sub _list_one {
    my $self = shift;
    my $k    = shift;
    return join(", ", $k->id, $k->magic_string);
}

=head2 get_duplicate_magicstrings

Get all the duplicate magic strings

=cut

sub get_duplicate_magicstrings {
    my $self = shift;

    my $sql = "select magic_string from bibliotheek_kenmerken where deleted is NULL group by magic_string having count(magic_string) > 1";
    return $self->schema->resultset('BibliotheekKenmerken')
        ->search({ magic_string => { in => \"($sql)" } }, { order_by => { -asc => 'me.magic_string' } });
}


1;

=head1 SEE ALSO

L<Zaaksysteem::CLI::Knife>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut
