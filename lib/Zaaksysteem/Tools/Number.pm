package Zaaksysteem::Tools::Number;
use warnings;
use strict;
use autodie;

use Exporter qw(import);

our @EXPORT_OK = qw(
    phonenumber_to_object
    reformat_telephone_number
    sloppy_phonenumber_to_cm
);

use Number::Phone;

=head1 NAME

Zaaksysteem::Tools::Number - Play with numbers

=head1 DESCRIPTION

Namespace for dealing with numbers so we keep it DRY.

=head1 METHODS


=head2 phonenumber_to_object

Coerce a string to a L<Number::Phone> object.

=cut

sub phonenumber_to_object {
    my $number = shift;

    $number = _sanity_phone($number);

    if (substr($number, 0,2) eq '00') {
        return Number::Phone->new("+" . substr($number, 2));
    }

    if ($number =~ /^0[0-9]{9}$/) {
        return Number::Phone->new('NL', $number);
    }

    return Number::Phone->new($number);
}

=head2 reformat_telephone_number

Format a L<Number::Phone> object to one style that can be used by our SMS toko.

=cut

sub reformat_telephone_number {
    my $object = shift;

    my $number = $object->format;

    $number =~ s/\s+//g;

    return "00" . substr($number, 1);
}

=head2 sloppy_phonenumber_to_cm

Allow any number to be a phonenumber and do some magic things

=cut

sub sloppy_phonenumber_to_cm {
    my $number = shift;

    $number = _sanity_phone($number);

    return "00" . substr($number, 1) if substr($number, 0, 1) eq '+';

    return "0031" . substr($number, 1) if $number =~ /^0[0-9]{9}$/;

    # legally, 088 numbers can only be 10 long,
    return "0031" . substr($number, 1) if $number =~ /^088[0-9]{7,9}/;

    return $number;
}

sub _sanity_phone {
    my $number = shift;
    # Make phonenumbers a bit saner
    $number =~ s/\-+//g;
    $number =~ s/\s+//g;
    $number =~ s/\(([0-9]+)\)/$1/g;
    return $number;
}


1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2019, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
