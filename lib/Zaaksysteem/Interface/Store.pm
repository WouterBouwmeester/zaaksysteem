package Zaaksysteem::Interface::Store;

use Moose::Role;

use BTTW::Tools;

=head1 NAME

Zaaksysteem::Interface::Store - Generalised abstraction for SCRUDE
implementations

=head1 INTERFACE

This interface declares a common way to perform C<SCRUDE> operations on a
store of data.

=head2 search

    my @results = $store->search(qb(...));

Search for entries using a L<Zaaksysteem::Object::Query>.

=head2 create

    my $id = $store->create({ ... });

Create new entries.

=head2 retrieve

    my $entry = $store->retrieve($id);

Retrieve an entry by store identifier.

=head2 update

    my @ids = $store->update(qb(...), { ... });

Update entries using a L<Zaaksysteem::Object::Query> and a hash of
field->value pairs.

=head2 delete

    my @ids = $store->delete(qb(...));

Delete entries using a L<Zaaksysteem::Object::Query>.

=head2 exists

    my @ids = $store->exists(qb(...));

Check existance of entries using a L<Zaaksysteem::Object::Query>.

=cut

requires qw[
    search
    create
    retrieve
    update
    delete
    exists
];

sig search => 'Zaaksysteem::Object::Query => @Zaaksysteem::Object';

sig create => 'Zaaksysteem::Object => Str';

sig retrieve => 'Str => Zaaksysteem::Object => ?Zaaksysteem::Object';

sig update => 'Zaaksysteem::Object::Query, HashRef => @Str';

sig delete => 'Zaaksysteem::Object::Query => @Str';

sig exists => 'Zaaksysteem::Object::Query => @Str';

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
