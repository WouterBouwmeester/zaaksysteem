package Zaaksysteem::Zaaktypen::BaseStatus;
use Zaaksysteem::Moose;

extends 'DBIx::Class::ResultSet';
with 'MooseX::Log::Log4perl';

use Params::Profile;

use constant STATUS_PREFIX  => 'zaaktype_';
use constant STATUS_RELATIES => [qw/
    zaaktype_kenmerken
    zaaktype_sjablonen
    zaaktype_relaties
    zaaktype_notificaties
    zaaktype_resultaten
    zaaktype_checklists
    zaaktype_regels
    zaaktype_standaard_betrokkenen
    zaaktype_object_mutation
/];

#    zaaktype_regels

use constant PARAMS_PROFILE_STATUS  => {
    'required'      =>  [qw/
        status
        naam
    /],
    'optional'      => [qw/
        status_type
        omschrijving
        help
        ou_id
        role_id
        afhandeltijd
        zaaktype_node_id
    /],
    constraint_methods  => {
        'status'        => qr/^\d+$/,
        'naam'          => qr/^[\w\d ]+$/,
        'status'        => qr/^\d+$/,
    },
    defaults            => {
        status_type     => 'behandelen',
    },
};

### XXX MOVE LOWER CLASS
sub _retrieve_columns {
    my ($self)  = @_;

    my @columns     = $self->result_source->columns;

    ### It is possible we ask a relation with extra information,
    ### when the component exports extra_columns, we can add
    ### these to the columns information
    if ($self->result_source->result_class->can('added_columns')) {
        push(
            @columns,
            @{ $self->result_source->result_class->added_columns }
        );
    }

    return @columns;
}

### XXX MOVE LOWER CLASS
sub _get_session_template {
    my ($self) = @_;

    my $relaties        = STATUS_RELATIES;
    my $relatieprefix   = STATUS_PREFIX;


    my $template = {
        'definitie' => {},
        'elementen' => {},
    };

    for my $key ($self->_retrieve_columns) {
        $template->{definitie}->{$key} = undef;
    }

    for my $relatie (@{ $relaties }) {
        my $relatie_info    =
            $self->result_source->relationship_info($relatie);

        my $relatie_object  = $self->result_source->schema->resultset($relatie_info->{source});

        next unless $relatie_object->can('_get_session_template');

        $relatie    =~ s/$relatieprefix//g;

        $template->{elementen}->{$relatie} =
            $relatie_object->_get_session_template;

        #$dv_error = 1 unless ($rv->{$status}->{elementen}->{$element}->success
    }

    return $template;
}

sub _validate_session_self {
    my ($self, $status_info)    = @_;

    my $rv                      = {};

    ### Get profile from Model
    my $profile = PARAMS_PROFILE_STATUS;

    Params::Profile->register_profile(
        method => '_validate_session_self',
        profile => $profile,
    );

    my $dv      = Params::Profile->check(
        params  => $status_info->{definitie} || {},
    );

    return $dv;
}

sub _validate_session {
    my ($self, $session)    = @_;

    my $rv                  = {};

    my $relatieprefix       = STATUS_PREFIX;

    ### Validate self
    while (my ($status, $status_info) = each %{ $session }) {
        my $dv_error = 0;

        $rv->{$status}              = {
            'definitie'     => {},
            'elementen'     => {},
            'success'       => 0,
        };

        ### Eigen status definitie
        $rv->{$status}->{definitie} = $self->_validate_session_self($status_info);

        $dv_error = 1 unless $rv->{$status}->{definitie}->success;

        ### Alle elementen, zoals notificatie / resultaten / relaties
        ### (subzaken) etc
        while (
            my ($element, $element_data) =
                each %{ $session->{$status}->{elementen} }
        ) {
            my $relatie_info    =
                $self->result_source->relationship_info($relatieprefix . $element);


            next unless ($relatie_info || $relatie_info->{source});

            my $relatie_object  = $self->result_source->schema->resultset($relatie_info->{source});

            next unless $relatie_object->can('_validate_session');

            $rv->{$status}->{elementen}->{$element} =
                $relatie_object->_validate_session($element_data);

            #$dv_error = 1 unless ($rv->{$status}->{elementen}->{$element}->success
        }

        $rv->{$status}->{success} = 1 unless $dv_error;
    }

    return $rv;
}

sub _retrieve_as_session {
    my $self        = shift;
    my $extraopts   = shift;

    my @columns     = $self->_retrieve_columns;

    my $counter     = 0;

    my $rv          = {};

    my $rows        = $self->search(
        {},
        {
            order_by    => 'id'
        }
    );


    my $relaties        = STATUS_RELATIES;
    my $relatieprefix   = STATUS_PREFIX;

    while (my $row  = $rows->next) {
        $rv->{$row->status} = {
            definitie   => {},
            elementen   => {},
        };
        for my $column (@columns) {
            ### When this is a reference to another table, just
            ### retrieve the id
            if (!ref($row->$column)) {
                $rv->{$row->status}->{definitie}->{$column} = $row->$column;
            }
        }

        for my $relatie (@{ $relaties }) {
            next unless $row->$relatie->can('_retrieve_as_session');

            ### Remove prefix,
            ### eg: $rv->{kenmerken} ipv $rv->{zaaktype_kenmerken}
            my $key         = $relatie;
            $key            =~ s/^$relatieprefix//;

            $rv->{$row->status}->{elementen}->{$key}     = $row->$relatie->_retrieve_as_session($extraopts);
        }
    }

    return $rv;
}

sub _params_to_database_params {
    my ($self, $params) = @_;

    my %result;
    my @columns = $self->_retrieve_columns;

    # Deal with default values from the database
    foreach (@columns) {
        if (!defined $params->{$_}) {
            my $info = $self->result_source->column_info($_);
            next if exists $info->{default_value};
        }
        $result{$_} = $params->{ $_ };
    }
    return \%result;
}

sub _commit_session_self {
    my ($self, $node, $status_params)   = @_;

    my $status_info = $self->_params_to_database_params($status_params);

    ### Get profile from Model
    delete($status_info->{id});

    my $data = {
        %{ $status_info },
        'zaaktype_node_id'  => $node->id,
    };

    return $self->create($data);
}

sub _get_relationship_object {
    my ($self, $reltype) = @_;

    my $source = $self->result_source;
    my $rel_id = STATUS_PREFIX . $reltype;

    if (!$source->has_relationship($rel_id)) {
        # See lib/Zaaksysteem/Schema/ZaaktypeStatus.pm
        my @rel_names = $source->relationships();
        $self->log->info(
            "Unable find $rel_id for $reltype, please use on of the following: "
                . dump_terse(\@rel_names));
        return;
    }

    my $info = $self->result_source->relationship_info(STATUS_PREFIX . $reltype);
    my $rs = $self->result_source->schema->resultset($info->{source});
    return $rs if $rs->can('_commit_session');
    $self->log->debug("Unable to commit session for $reltype");
    return;
}

sub _commit_session {
    my ($self, $node, $session)    = @_;

    my $rv = {};

    while (my ($status, $status_info) = each %{ $session }) {
        my $dv_error = 0;

        $rv->{$status}              = {
            'definitie'     => {},
            'elementen'     => {},
            'success'       => 0,
        };

        ### Eigen status definitie
        my $db_status = $self->_commit_session_self(
            $node,
            $status_info->{definitie}
        );

        $rv->{$status}->{definitie} = $db_status;

        # deliberately put regels at the end, so it can refer to the updated ids
        # in the other elements. these updated ids are only known after the inserts
        # in the database are made
        my @elementen = qw(
            checklists
            sjablonen
            resultaten
            relaties
            notificaties
            kenmerken
            standaard_betrokkenen
            object_mutation
            regels
        );

        foreach my $element (@elementen) {
            my $rs = $self->_get_relationship_object($element);
            next unless $rs;
            $self->log->trace("Trying to commit $element");

            $rv->{$status}{elementen}{$element} = $rs->_commit_session(
                $db_status,
                $session->{$status}{elementen}{$element}
            );

            $self->_post_process_commit_session(
                $element,
                $session->{$status}{elementen},
                $rv->{$status}{elementen}
            );

        }

        $rv->{$status}->{success} = 1 unless $dv_error;
    }

    return $rv;

}

sub _post_process_commit_session {
    my ($self, $element, $old_data, $new_data) = @_;

    $self->_map_date_kenmerken_properties(
        $element,
        old => $old_data,
        new => $new_data,
    );

    $self->_map_object_mutation_rules(
        $element,
        old => $old_data,
        new => $new_data,
    );

}

sub _map_date_kenmerken_properties {
    my ($self, $element, %args) = @_;

    return 0 if $element ne 'kenmerken';

    my $mapping = $self->_setup_mapping($element, %args);
    return 0 unless $mapping;

    my $new = $args{new}{$element};

    my ($object, $properties);
    foreach (keys %$new) {
        $object = $new->{$_};
        my $properties = $object->properties;
        if ($properties) {
            if (exists $properties->{date_limit}) {
                foreach (qw(start end)) {
                    if (exists $mapping->{$properties->{date_limit}{$_}{reference}}) {
                        $properties->{date_limit}{$_}{reference} = $mapping->{$properties->{date_limit}{$_}{reference}};
                    }
                }
            }
        }
        $object->update({properties => $properties// {}});
    }
    return 1;
}

sub _setup_mapping {
    my ($self, $element, %args) = @_;

    my $new = $args{new}{$element};
    my $old = $args{old}{$element};

    my %mapping;

    foreach (keys %$old) {
        if (my $prev = $old->{$_}{id}) {
            $mapping{$prev} = $new->{$_}->id;
        }
    }
    return \%mapping if %mapping;
    return;
}

sub _map_object_mutation_rules {
    my ($self, $element, %args) = @_;
    return 0 if $element ne 'object_mutation';

    my $rules = $args{old}{regels};
    return 0 unless $rules;

    my $mapping = $self->_setup_mapping($element, %args);
    return 0 unless $mapping;

    foreach my $rule (values %$rules) {
        my @actions = grep { /^actie_[0-9]+$/ } keys %$rule;
        foreach (@actions) {
            next unless $rule->{$_} eq 'trigger_object_action';
            my $id_key = join("_", $_, 'type');
            my $old = $rule->{$id_key};
            if (defined $mapping->{$old}) {
                $rule->{$id_key} = $mapping->{$old};
            }
        }
    }

    return 1;
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
