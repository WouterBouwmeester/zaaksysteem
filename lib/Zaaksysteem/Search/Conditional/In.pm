package Zaaksysteem::Search::Conditional::In;

use Moose::Role;

=head1 NAME

Zaaksysteem::Search::Conditional::In - Special case for IN () conditions

=head1 METHODS

=head2 evaluate

Overriden evaluate behavior (see L<Zaaksysteem::Search::Conditional>).

Because PostgreSQL does not have an optimized (= indexed) way to handle
C<IN ()> on hstory-type fields, this renders C<field IN("a", "b", "c")> as:

    (
        (index_hstore @> '"field"=>"a"')
        OR (index_hstore @> '"field"=>"b"')
        OR (index_hstore @> '"field"=>"c"')
    )

If the conditional is negated (C<NOT (field IN ("a", "b", "c"))>), it will be
rendered as:

    (
        NOT (index_hstore @> '"field"=>"a"')
        AND NOT (index_hstore @> '"field"=>"b"')
        AND NOT (index_hstore @> '"field"=>"c"')
    )

=cut

around evaluate => sub {
    my $orig = shift;
    my $self = shift;
    my $rs = shift;

    my $values;
    if ($self->rterm->isa('Zaaksysteem::Search::Conditional')) {
        $values = $self->rterm->lterm->values;
    } else {
        $values = $self->rterm->values;
    }

    my $conditional = sprintf("%s @> hstore(?, ?)", $rs->hstore_column);
    my $field = $self->lterm->value;

    my @parameters;
    for my $val ( @{ $values } ) {
        push @parameters, (
            [{} => $field],
            [{} => $val->value],
        );
    }

    if ($self->invert) {
        return \[
            "(NOT " . join(" AND NOT ", ($conditional) x @$values) . ")",
            @parameters
        ];
    }
    else {
        return \[
            "(" . join(" OR ", ($conditional) x @$values) . ")",
            @parameters
        ];
    }
};

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2021, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut

