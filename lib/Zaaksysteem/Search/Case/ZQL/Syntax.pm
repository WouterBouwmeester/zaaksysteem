package Zaaksysteem::Search::Case::ZQL::Syntax;

use base 'Exporter';

use Zaaksysteem::Search::ZQL::Literal::String;
use Zaaksysteem::Search::ZQL::Literal::Number;
use Zaaksysteem::Search::ZQL::Literal::Null;
use Zaaksysteem::Search::ZQL::Literal::Set;
use Zaaksysteem::Search::ZQL::Literal::DateTime;
use Zaaksysteem::Search::ZQL::Literal::JPath;
use Zaaksysteem::Search::ZQL::Literal::Boolean;
use Zaaksysteem::Search::ZQL::Literal::Column;
use Zaaksysteem::Search::ZQL::Literal::ObjectType;
use Zaaksysteem::Search::ZQL::Literal::Function;

use Zaaksysteem::Search::ZQL::Expression::Infix;
use Zaaksysteem::Search::ZQL::Expression::Infix::Between;
use Zaaksysteem::Search::ZQL::Expression::Prefix;

use Zaaksysteem::Search::ZQL::Operator::Infix;
use Zaaksysteem::Search::ZQL::Operator::Prefix;

use Zaaksysteem::Search::Case::ZQL::Command::Select;
use Zaaksysteem::Search::ZQL::Command::Describe;
use Zaaksysteem::Search::ZQL::Command::Analyze;
use Zaaksysteem::Search::Case::ZQL::Command::Count;

our @EXPORT = qw[zql_command zql_literal zql_op zql_expr];

use List::Util qw(any);

sub zql_command {
    my $cmd = shift;
    my $package = sprintf('Zaaksysteem::Search::ZQL::Command::%s', $cmd);

    if (any { $cmd = $_ } qw(Select Count)) {
        $package = sprintf('Zaaksysteem::Search::Case::ZQL::Command::%s', $cmd);
    }

    return $package->new_from_production(@_);
}

sub zql_literal {
    my $package = sprintf('Zaaksysteem::Search::ZQL::Literal::%s', shift);

    return $package->new_from_production(@_);
}

sub zql_op {
    my $package = sprintf('Zaaksysteem::Search::ZQL::Operator::%s', shift);

    return $package->new_from_production(@_);
}

sub zql_expr {
    my $package = sprintf('Zaaksysteem::Search::ZQL::Expression::%s', shift);

    return $package->new_from_production(@_);
}

1;



__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.

=cut


=head1 UNDOCUMENTED FUNCTIONS

Below you will find a list of undocumented functions
Please find the time to fix them
This is done to start propper POD coverage testing on new modules

=head2 zql_command

TODO: Fix the POD

=cut

=head2 zql_expr

TODO: Fix the POD

=cut

=head2 zql_literal

TODO: Fix the POD

=cut

=head2 zql_op

TODO: Fix the POD

=cut

