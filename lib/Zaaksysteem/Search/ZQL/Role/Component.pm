package Zaaksysteem::Search::ZQL::Role::Component;
use Moose::Role;

=head1 DESCRIPTION

=head1 SYNOPSIS

=cut

requires qw(
    _build_csv_data
    _build_csv_header
);

=head2 blacklisted_columns

Set an array ref of strings which define the blacklisted columns. The
attributes found will display a message telling the user that they aren't
allowed to view the attributes

=cut

has blacklisted_columns => (
    is      => 'rw',
    isa     => 'ArrayRef[Str]',
    traits  => [qw(Array)],
    handles => { _is_blacklisted => 'first' },
    default => sub { [] },
    lazy    => 1,
);

has _zql_options => (
    is        => 'rw',
    lazy      => 1,
    default   => sub { {}; },
    isa       => 'HashRef',
    predicate => 'has_zql_options',
);

has csv_data => (
    is      => 'ro',
    isa     => 'ArrayRef',
    lazy    => 1,
    builder => '_build_csv_data',
);

has csv_header => (
    is      => 'ro',
    isa     => 'ArrayRef',
    lazy    => 1,
    builder => '_build_csv_header',
);

sub _initialize_zql {
    my $self = shift;
    $self->_zql_options(shift)
}

sub get_zql_options {
    my $self = shift;
    my $key = shift;
    return unless $self->has_zql_options;
    return $self->_zql_options->{$key};
}

sub get_requested_columns {
    my ($self) = @_;
    my $attr = $self->get_zql_options('requested_attributes');
    return @$attr if $attr;
    return;
}

sub get_include_row_actions {
    my ($self) = @_;
    return $self->get_zql_options('include_row_actions');
}

sub is_blacklisted {
    my ($self, $term) = @_;
    return $self->_is_blacklisted(sub { $_ eq $term });
}



1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2021, Mintlab B.V. and all the persons listed in the
L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at
the L<LICENSE|Zaaksysteem::LICENSE> file.
