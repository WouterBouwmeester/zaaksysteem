package Zaaksysteem::StUF::0204::General;

use Moose::Role;

use BTTW::Tools;

with 'Zaaksysteem::StUF::0204::SPOOF';

=head1 NAME

Zaaksysteem::StUF::0204::General - General functions and methods for StUF 0204 handling

=head1 SYNOPSIS

    $stuf0204->generate_stuurgegevens(
        {
            messagetype             => 'Lk01',
            entitytype              => 'PRS',
            sender                  => 'ZSNL',
            receiver                => 'CMODIS',
            reference               => 'ZS0000229154',
            datetime                => '2014030209011458',
            mutation_type           => 'create',
            follow_subscription     => 1,
        },
    );

    # Return: XML Compile readable hash for StUF0204 stuurgegevens

=head1 DESCRIPTION

StUF (specific 0204) related functions for inclusion by StUF 0204

Tests can be found in: TestFor::General::StUF::0204::General

=head1 METHODS

=head2 generate_stuurgegevens

Arguments: \%OPTIONS

Return value: \%HASH_FOR_XML_COMPILE

    $stuf0204->generate_stuurgegevens(
        {
            messagetype             => 'Lk01',
            entitytype              => 'PRS',
            sender                  => 'ZSNL',
            receiver                => 'CMODIS',
            reference               => 'ZS0000229154',
            datetime                => '2014030209011458',
            mutation_type           => 'create',
            follow_subscription     => 1,
        },
    );

    # Return: XML Compile readable hash for StUF0204 stuurgegevens


Given a hash of options, it will return the perl hash containing stuurgegevens for StUF0204
formatted xml messages.

B<Options>

=over 4

=item messagetype

Type of message, e.g. C< Lk01 >. See StUF0204 for more information

B<Allowed messages>

    lk01
    lv01

=item entitytype

The entitytype for this message, e.g. C< PRS > or C< ADR > or C< NNP >

=item sender

default: ZSNL

The sender of this message.

=item receiver

The intended receiver for this message, e.g. C< CMG >

=item reference

The reference id for this message, our transaction ID for instance.

=item datetime

The datetime of this message, needs to be 16 digits

=item mutation_type [optional]

The mutation type for this message, can be one of:

    create
    update
    delete

    # or:

    C       (create)
    W       (update)
    V       (delete)

=item follow_subscription [optional]

Sets the XML "indicatorOvername" to C<V> when set, and to C<I> when unset.

=item rows

Maximum number of rows to retrieve (in case of a vraarBericht)

=back

=cut

define_profile 'generate_stuurgegevens' => (
    required    => [qw/
        messagetype
        entitytype
        sender
        receiver
        reference
        datetime
    /],
    optional    => [qw/
        follow_subscription
        mutation_type
    /],
    dependencies        => {
        'messagetype'   => sub {
            my $dfv         = shift;
            my $messagetype = shift;

            if (lc($messagetype) eq 'lk01') {
                return ['mutation_type']
            }

            return [];
        }
    },
    constraint_methods  => {
        messagetype     => qr/^Lv01|Lk01$/,
    },
    defaults    => {
        rows        => 10,
    }
);

sub generate_stuurgegevens {
    my $self                = shift;
    my $params              = assert_profile($_[0] || {})->valid;

    my $mutation_map        = {
        'create'        => 'T',
        'update'        => 'W',
        'delete'        => 'V',
    };

    my $rv = {
        berichtsoort        => $params->{messagetype},
        entiteittype        => $params->{entitytype},
        sectormodel         => 'BG',
        versieStUF          => '0204',
        versieSectormodel   => '0204',
        zender              => {
            applicatie          => $params->{sender},
        },
        ontvanger           => {
            applicatie          => $params->{receiver},
        },
        referentienummer    => $params->{reference},
        tijdstipBericht     => $params->{datetime},
    };

    if (lc($params->{messagetype}) =~ /lk01/) {
        $rv->{kennisgeving} = {
            mutatiesoort        => (
                length($params->{mutation_type}) > 1
                    ? $mutation_map->{ $params->{mutation_type} }
                    : $params->{mutation_type}
            ),
            indicatorOvername   => (
                $params->{follow_subscription}
                    ? 'V'
                    : 'I'
            )
        };
    }

    if (lc($params->{messagetype}) =~ /lv01/) {
        $rv->{vraag}    = {
            maximumAantal       => $params->{rows},
        };
    }

    return $rv;
}

=head1 INTERNAL METHODS

=head2 _get_mutation_type_from_stuurgegevens

Arguments: \%STUURGEGEVENS

    my $stuur = {
        'berichtsoort' => 'Lk01',
        'entiteittype' => 'PRS',
        'kennisgeving' => {
            'indicatorOvername' => 'V',
            'mutatiesoort' => 'T'
        },
    };

    print $self->_get_mutation_type_from_stuurgegevens($stuur);

    ## Prints 'create'

Prints the mutation type according to the C<mutatiesoort> of a kennisgeving

B<Possible outcomes>

=over 4

=item T: create

=item W: update

=item V: delete

=item C: update

=back

=cut


sub _get_mutation_type_from_stuurgegevens {
    my $self                = shift;
    my $stuurgegevens       = shift;

    my $mutation_map = {
        'T' => 'create',
        'W' => 'update',
        'V' => 'delete',
        'C' => 'update'
    };

    if (
        !$stuurgegevens->{kennisgeving}
    ) {
        return 'create';
    }

    my $mutatiesoort        = uc($stuurgegevens->{kennisgeving}->{mutatiesoort});

    return $mutation_map->{$mutatiesoort};
}

1;

__END__

=head1 SEE ALSO

L<Zaaksysteem::StUF::0204::Instance> L<Zaaksysteem::Manual>

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
