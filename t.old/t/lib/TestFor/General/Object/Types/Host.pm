package TestFor::General::Object::Types::Host;

# ./zs_prove -v t/lib/TestFor/General/Object/Types/Host.pm
use base qw(ZSTest);

use TestSetup;
use Zaaksysteem::Object::Types::Host;

require Zaaksysteem::TestUtils;

sub zs_object_types_host : Tests {

    my $model = $zs->object_model;

    $zs->zs_transaction_ok(
        sub {
            my %opts = (
                owner      => 'betrokkene-bedrijf-1',
                fqdn       => 'hosts.testsuite.zs.nl',
                ip         => '194.134.5.5',
                label      => 'Some host',
                disabled   => '1',
                ssl_key    => 'Some key',
                ssl_cert   => 'Some cert',
            );

            my ($obj, $saved) = $zs->create_object_ok('Host', \%opts, { disabled => 'true'});
        },
        'Host object'
    );

}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2015, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
