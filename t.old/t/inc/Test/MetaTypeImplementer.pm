package Test::MetaTypeImplementer;

use Moose;

extends 'Zaaksysteem::Object';

has instance_prefix => (
    is => 'rw',
    isa => 'Str',
    default => 'object'
);

has instance_superclass => (
    is => 'rw',
    isa => 'Str',
    default => 'Zaaksysteem::Object'
);

has _instance_roles => (
    is => 'rw',
    isa => 'ArrayRef[Str]',
    traits => [qw[Array]],
    default => sub { [] },
    init_arg => 'instance_roles',
    handles => {
        instance_roles => 'elements'
    }
);

has _instance_attributes => (
    is => 'rw',
    isa => 'ArrayRef[HashRef]',
    traits => [qw[Array]],
    default => sub { [] },
    init_arg => 'instance_attributes',
    handles => {
        instance_attributes => 'elements'
    }
);

has _instance_relations => (
    is => 'rw',
    isa => 'ArrayRef[HashRef]',
    traits => [qw[Array]],
    default => sub { [] },
    init_arg => 'instance_relations',
    handles => {
        instance_relations => 'elements'
    }
);

# Apply role late in the game, so all attributes and proxies haven been
# installed.
with 'Zaaksysteem::Object::Roles::MetaType';

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2009-2014, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
