export default ( elementToWaitFor ) => {
    browser.driver.wait( () => {
        return $(elementToWaitFor).isPresent().then( (el) => {
            return el === true;
        });
    }, 30000);
};

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
