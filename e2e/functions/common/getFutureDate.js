export default daysToAdd => {
    const expectedDate = new Date();
    let day;
    let month;
    let year;
    let expectedDateText;

    expectedDate.setDate(expectedDate.getDate() + daysToAdd);

    day = expectedDate.getDate() < 10 ? `0${expectedDate.getDate()}` : expectedDate.getDate();
    month = expectedDate.getMonth() < 9 ? `0${expectedDate.getMonth() + 1}` : expectedDate.getMonth() + 1;
    year = expectedDate.getFullYear();
    expectedDateText = `${day}-${month}-${year}`;

    return expectedDateText;
};

/* ***** COPYRIGHT and LICENSE **************************************
 *
 *  Copyright (c) 2017, Mintlab B.V. and all the persons listed in the CONTRIBUTORS file.
 *  Zaaksysteem uses the EUPL license, for more information please have a look at the LICENSE file.
 *
 * ******************************************************************/
