[% BLOCK show_parent_groups %]
    [%
    parent_group_names = [];
    FOREACH parent_group IN parents;
        parent_group_names.push(parent_group.name);
    END;
    %]
    [% IF parent_group_names.size %] ([% parent_group_names.join('/') | html_entity %])[% END %]
[% END %]

[% BLOCK tree_entry %]
    <tr id="[% tree_type %]node-[% entry.id %]" class="[%
        (parent_entry
            ? ' child-of-groupnode-' _ entry.parent_id
            : ''
        ) %]">
        <td class="entry-[% tree_type %]"[% (entry.type ==
        'role' || entry.type == 'user' ? ' style="cursor: move;"' : '' ) %]>
            [% UNLESS tree_type == 'group' %]
            <div class="entry-[% tree_type  %]-draggable">
            [% END %]
                <span class="entry_name">
                    [% IF tree_type == 'group' %]
                    <a href="[% c.uri_for('/medewerker') %]" class="ezra_tree_table_reload">
                    [% END %]
                    [% entry.name | html_entity %]

                    [% IF ou_view %]
                    </a>
                    [% END %]
                </span>
                <input type="hidden" name="identifier" value="[% tree_type %]-[% entry.id %]" />
            [% UNLESS tree_type == 'group' %]
            </div>
            [% END %]
        </td>
    
        <td class="td60 td-del row-actions row-actions-left">
        [% IF entry.table == 'Roles'; entry_identifier = 'role-' _ entry.id; END %]
        [% IF entry.table == 'Groups'; entry_identifier = 'group-' _ entry.id; END %]
        [% IF !entry_identifier; entry_identifier = 'user-' _ entry.id; END %]
        [% UNLESS entry.system_role || (show_roles && tree_type == 'group') %]
            <a href="[% c.uri_for('/medewerker/edit', { dn => entry_identifier }) %]" class="ezra_dialog row-action" title="Bewerken">
               <i class="icon-font-awesome icon-pencil"></i> 
            </a>
            [% UNLESS entry.children.size %]
            <a href="[% c.uri_for('/medewerker/delete', { dn => entry_identifier }) %]" class="ezra_dialog row-action" title="Verwijderen">
                <i class="icon-font-awesome icon-remove"></i>
            </a>
            [% END %]
        [% END %]

        </td>
    </tr>

    [% IF entry.children.size -%]
        [% FOREACH child_entry IN entry.children %]
            [% INCLUDE tree_entry
                simple          = simple
                entry           = child_entry
                parent_entry    = entry
                ou_view         = ou_view
                tree_type       = tree_type
            %]
        [% END %]
    [% END -%]

    [% IF entry.roles.size && show_roles -%]
        [% FOREACH role_entry IN entry.roles %]
            [% INCLUDE tree_entry
                simple          = simple
                entry           = role_entry
                parent_entry    = entry
                ou_view         = ou_view
                tree_type       = 'role'
            %]
        [% END %]
    [% END %]
[% END %]

[% BLOCK table_medewerker_row %]
    <tr>
        [% IF entry.group_ids.size %]
        <td class="entry-user" style="cursor: move;">
            <div class="entry-user-draggable">
                <span class="entry_name">
                    [% entry.displayname | html_entity %] (<b>[% entry.username | html_entity %]</b>)
                </span>
                <input type="hidden" name="identifier" value="user-[% entry.id %]" />
                <a href="[% c.uri_for('/medewerker/edit/user/' _ entry.id) %]" class="ezra_dialog right row-actions" title="Bewerk entry">
                    <i class="icon-font-awesome icon-pencil"></i>
                </a>
                <a href="[% c.uri_for('/medewerker/login-history/' _ entry.id) %]" class="ezra_dialog ezra_dialog_history right row-actions"
                    title="Loginhistorie">
                   <i class="icon-font-awesome icon-time" style="height: auto; width: 25px; text-align:left;"></i>
                </a>
            </div>
            <ul class="ezra_entryrol">
                [% FOREACH entryrol IN entry.roles %]
                <li>
                    <span class="entry_role">[% entryrol.name | html_entity %][% PROCESS show_parent_groups parents=entryrol.parent_groups %]</span>
                    <input type="hidden" name="entry_role_id" value="role-[% entryrol.id | html_entity %]" />
                    <a href="#" class="ezra_medewerkers_delete_role right row-actions">
                        <i class="icon-font-awesome icon-remove"></i>
                    </a>
                </li>
                [% END %]
            </ul>
        </td>
        [% ELSE %]
        [% # INBOX -%]
        <td class="entry-user" style="cursor: move;">
            <div class="entry-user-draggable">
                <span class="entry_name">
                    [% entry.displayname | html_entity %] (<b>[% entry.username | html_entity %]</b>)
                </span>
                <input type="hidden" name="identifier" value="user-[% entry.id %]" />
                <a href="[% c.uri_for('/medewerker/edit/user/' _ entry.id) %]" class="ezra_dialog right" title="Bewerk entry">
                    <i class="icon-font-awesome icon-pencil"></i>
                </a>
            </div>
            <ul class="ezra_entryrol[% IF !entry.primary_group && entry.preferred_group %] has-entry-user-accept[% END %]">
               
                    [% IF !entry.primary_group && entry.preferred_group %]
                     <div class="entry-user-accept">
                        <b>Voorkeursafdeling:</b>
                        <span class="entry_unit">[% entry.preferred_group.name | html_entity %]</span>
                        <br />
                        <b>Toegangsniveau:</b>
                        <select name="authorization_scope">
                            <option value="full">Zaaksysteem en Apps</option>
                            <option value="app">Apps</option>
                        </select>
                        <a href="#" rel="group_identifier: group-[% entry.preferred_group.id %]" class="accept_ou_group button button-primary button-xsmall right">
                            accepteer
                        </a>
                        <br />
                    </div>
                    [% END %]
                    [% FOREACH entryrol IN entry.roles %]
               
                <li>
                    <span class="entry_role">[% entryrol.name | html_entity %]</span>
                    <input type="hidden" name="entry_role_id" value="role-[% entryrol.id %]" />
                    <a href="#" class="ezra_medewerkers_delete_role right row-actions">
                        <i class="icon-font-awesome icon-remove"></i>
                    </a>
                </li>
                [% END %]
            </ul>
        </td>
        [% END %]
    </tr>
[% END %]

[% BLOCK medewerkers_rollen -%]
    [% USE Scalar -%]
    <div class="block-fullscreen medewerkers-sidebar medewerkers rollen block-inner-scroll-right">
        <div class="block-header header-with-actions clearfix">
            <h1 class="block-header-title left">Rollen</h1>
            <div class="ezra_actie_button_handling block-header-actions right">
                <div class="ezra_actie_button_handling_navigation">
                    <div class="button-group button-group-relative clearfix">
                        <a class="button button-secondary buttonb dropdown-toggle" href="#rollen_voegtoe">
                            <i class="icon icon-font-awesome icon-plus"></i>
                        </a>
                        <div class="ezra_actie_button_handling_popover">
                            <div class="popover-tip ezra_actie_button_handling_popover_tip"></div>
                            <div class="popover" id="popover_rollen_voegtoe">
                                <div class="buttons">
                                    <a
                                        title="Rol toevoegen"
                                        class="ezra_dialog"
                                        href="[% c.uri_for('/medewerker/add/role') | html_entity %]">Rol</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="blockcontent">
            <div class="spinner-groot spinner-groot-no-alpha"><div></div></div>
            <table class="ezra_tree_table treeTable fixed-layout">
              <tbody>
              [% FOREACH entry IN ou_entries %]
                [% PROCESS tree_entry
                    node_prefix = 'rollen'
                    tree_type   = 'group'
                    show_roles  = 1
                    simple      = 1
                    entry       = entry
                    tree_parent = 0
                    ou_view     = 0
                %]
              [% END %]
              </tbody>
            </table>
        </div>
    </div>
</div>
[% END -%]

[% BLOCK medewerkers_overzicht -%]
    <div class="block-inner-scroll gebruikers">
        <div class="block-fullscreen block-inner-scroll-left medewerkers overzicht">
            <div class="block-header header-with-actions clearfix">

                <h1 class="block-header-title">Medewerkers</h1>

                <div class="ezra_actie_button_handling block-header-actions right">

                    <div class="ezra_actie_button_handling_navigation">

                        <div class="button-group button-group-relative clearfix">

                            <a class="button button-secondary button dropdown-toggle" href="#overzicht_voegtoe">
                                <i class="icon icon-font-awesome icon-plus"></i>
                            </a>

                            <div class="ezra_actie_button_handling_popover">
                                <div class="popover-tip ezra_actie_button_handling_popover_tip"></div>
                                <div class="popover" id="popover_overzicht_voegtoe">
                                    <div class="buttons">
                                        <a
                                            title="Afdeling toevoegen"
                                            class="ezra_dialog"
                                            href="[% c.uri_for('/medewerker/add/ou') | html_entity %]"
                                        >
                                            Afdeling
                                        </a>
                                        <a
                                            title="Medewerker toevoegen"
                                            class="ezra_dialog"
                                            href="[% c.uri_for('/medewerker/add/user') | html_entity %]"
                                        >
                                            Medewerker
                                        </a>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>
                <div class="ezra_actie_button_handling block-header-actions right">

                    <div class="ezra_actie_button_handling_navigation">

                        <div class="button-group button-group-relative clearfix">

                            <a class="button button-secondary button dropdown-toggle" href="#overzicht_download">
                                Exporteren
                            </a>

                            <div class="ezra_actie_button_handling_popover">
                                <div class="popover-tip ezra_actie_button_handling_popover_tip"></div>
                                <div class="popover" id="popover_overzicht_download">
                                    <div class="buttons">
                                        <a
                                            title="Gebruikersoverzicht met rollen"
                                            href="/medewerker/export?type=user_role"
                                        >
                                            Gebruikersoverzicht met rollen
                                        </a>
                                        <a
                                            title="Gebruikersoverzicht met rechten"
                                            href="/medewerker/export?type=user_permission"
                                        >
                                            Gebruikersoverzicht met rechten
                                        </a>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>
            </div>
            <div class="splitter">
                <div class="blockcontent blockcontent-left">
                    <div class="spinner-groot spinner-groot-no-alpha"><div></div></div>
                    <table class="ezra_tree_table">
                      <tbody>
                      [% entries = ou_entries %]
                      [% FOREACH entry IN entries %]
                        [% PROCESS tree_entry
                            node_prefix = 'overzicht'
                            tree_type   = 'group'
                            only_roles  = 0
                            simple      = 0
                            entry       = entry
                            tree_parent = 0
                            ou_view     = 1
                        %]
                      [% END %]
                      <tr id="overzichtnode-inbox" class="">
                          <td class="entry-group">
                              <span class="entry_name">
                                <a href="[% c.uri_for('/medewerker') | html_entity %]" class="ezra_tree_table_reload">
                                  <span class="inbox_group[% inbox.count ? ' has_entries' : '' %]">Inbox (<span class="inbox_counter">[% inbox.count %]</span>)</span>
                                </a>
                              </span>
                              <input type="hidden" name="identifier" value="inbox" />
                          </td>
                          <td class="td40 td-del" colspan="2">
                          </td>
                      </tr>
                      </tbody>
                    </table>
                </div>
                <div class="blockcontent blockcontent-right" id="ezra_medewerker_panel">
                    <div class="panel_content">
                        <div class="spinner-groot spinner-groot-no-alpha"><div></div></div>
                        <table class="ezra_tree_table">
                          <tbody>
                          [% entries = people_entries %]
                          [% FOREACH entry IN entries %]
                            [% PROCESS table_medewerker_row
                                node_prefix = 'overzicht'
                                only_roles  = 0
                                simple      = 0
                                entry       = entry
                                tree_parent = 0
                                ou_view     = 0
                            %]
                          [% END %]
                          [% UNLESS people_entries && people_entries.size %]
                             <tr>
                                <td style="padding:10px 15px;color:#777;" colspan="3">Geen gebruikers gevonden</td>
                             </tr>
                          [% END %]
                          </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
[% END -%]


<form method="POST" action="[% c.uri_for('/medewerker') | html_entity %]">
[% PROCESS medewerkers_overzicht %]
[% PROCESS medewerkers_rollen %]

</form>
