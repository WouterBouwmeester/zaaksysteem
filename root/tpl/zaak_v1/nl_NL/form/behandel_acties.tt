[% USE Dumper %]

[% RETURN UNLESS c.user_exists %]

[%
    RETURN UNLESS definitie.toewijzing_zaakintake ||
        definitie.extra_relaties_in_aanvraag ||
        definitie.automatisch_behandelen ||
        zaaktype.properties.confidentiality
    %]
<div class="stap-content stap-content-multiple">
    <div class="stap-tekst">
        <h3>Zaakacties</h3>
    </div>
[% IF definitie.toewijzing_zaakintake -%]
    <div class="row kenmerk-veld">
    [% PROCESS widgets/toewijzing.tt
        AUTH_SELECT_OU_NAME     = 'actie_ou_id'
        AUTH_SELECT_OU_VALUE    = zaaktype.scalar.zaaktype_statussen.scalar.search(
            {
                status  => 1
            }
        ).first.ou_id
        AUTH_SELECT_ROLE_NAME   = 'actie_role_id'
        AUTH_SELECT_ROLE_VALUE  = zaaktype.scalar.zaaktype_statussen.scalar.search(
            {
                status  => 1
            }
        ).first.role_id
        AUTH_SELECT_NO_GLOBAL = 1
        ASSIGN_TO_ME = definitie.automatisch_behandelen
    %]
    </div>
[% END %]
[% IF !definitie.toewijzing_zaakintake && definitie.automatisch_behandelen %]
<div class="row kenmerk-veld ml-animate-auto-height" data-ng-show="!caseWebform.isApplicationPausedWithoutResumal()">

    <div class="column large-4">
        <div class="field-label field-has-help">
            <div class="field-help">
                [% PROCESS widgets/general/dropdown.tt
                icon = 'icon-question-sign',
                icon_type = 'icon-font-awesome',
                dropdown_content = 'Na aanmaken van deze zaak wordt u direct behandelaar'
            %]
            </div>
            <label class="titel">Zaak automatisch in behandeling nemen</label>
        </div>
    </div>

    <div class="column large-8">

        <div class="field">
            <input
            type="checkbox"
            name="actie_automatisch_behandelen"
            checked="checked"
            value="1" />
        </div>
        <div class="validator rounded">
            <div class="validate-tip"></div>
            <div class="validate-content rounded">
                <span></span>
            </div>
        </div>

    </div>

</div>
[% END %]

[% IF definitie.extra_relaties_in_aanvraag %]
<div class="row kenmerk-veld">

    <div class="column large-4">
        <div class="field-label field-has-help">
            <div class="field-help">
                [% PROCESS widgets/general/dropdown.tt
                icon = 'icon-question-sign',
                icon_type = 'icon-font-awesome',
                dropdown_content = 'Het kan voorkomen dat er naast de aanvrager meerdere personen/bedrijven gerelateerd zijn aan deze zaak. Deze relaties kun je hieronder toevoegen.'
            %]
            </div>
            <label class="titel">
                Relaties
            </label>
        </div>
    </div>

    <div class="column large-8">
        <div class="field">
            [% PROCESS widgets/general/simple_table.tt %]
        </div>



        <div class="validator rounded">
            <div class="validate-tip"></div>
            <div class="validate-content rounded">
                <span></span>
            </div>
        </div>

    </div>

</div>
[% END %]

[% IF zaaktype.properties.confidentiality %]
    <div class="row kenmerk-veld">
        <div class="column large-4">
            <div class="field-label field-has-help">
                <div class="field-help">
                    [% PROCESS widgets/general/dropdown.tt
                    icon = 'icon-question-sign',
                    icon_type = 'icon-font-awesome',
                    dropdown_content = 'De zaak zal vertrouwelijk worden behandeld.'
                %]
                </div>
                <label class="titel">Vertrouwelijk behandelen</label>
            </div>
        </div>
        <div class="column large-8">
            <div class="field">
                <select name="actie_confidentiality">
                    <option value="public">Openbaar</option>
                    <option value="internal">Intern</option>
                    <option value="confidential">Vertrouwelijk</option>
                </select>
            </div>
            [% PROCESS widgets/general/validator.tt %]
        </div>
    </div>
[% END %]
</div>
