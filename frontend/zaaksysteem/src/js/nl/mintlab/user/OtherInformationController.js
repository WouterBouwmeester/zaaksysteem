// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular
    .module('Zaaksysteem.user')
    .controller('nl.mintlab.user.OtherInformationController', [
      '$scope',
      '$element',
      function ($scope, $element) {
        var iframe = document.createElement('iframe');

        iframe.id = 'iframe-other-information';
        iframe.title = 'iframe-other-information';
        iframe.alt = 'other-information-body';
        iframe.classList.add('iframe-other-information');
        iframe.style.height = 0;

        function resizeIframe() {
          var iframe = document.getElementById('iframe-other-information');
          var innerDoc =
            iframe.contentDocument || iframe.contentWindow.document;
          var body = innerDoc.getElementsByTagName('body')[0];
          // scrollHeight is 0 while loading or 87 when with spinner
          var loading = body.scrollHeight < 150;

          if (loading) {
            setTimeout(() => {
              resizeIframe();
            }, 1000);
          } else {
            iframe.style.height = body.scrollHeight.toString() + 'px';
          }
        }

        iframe.onload = resizeIframe;

        $scope.init = function (contactType, contactUuid) {
          iframe.src = '/main/profile/' + contactType + '/' + contactUuid;

          $element.find('otherinformation').replaceWith(iframe);
        };
      },
    ]);
})();
