// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular.module('Zaaksysteem.subject').directive('zsSubjectView', [
    '$http',
    'objectService',
    'systemMessageService',
    function ($http, objectService, systemMessageService) {
      return {
        controller: [
          '$scope',
          '$element',
          '$attrs',
          function ($scope, $element, $attrs) {
            var ctrl = this,
              subjectId = $attrs.subjectId,
              subject;

            ctrl.getSubjectId = function () {
              return $attrs.subjectId;
            };

            $http({
              method: 'GET',
              url: '/api/subject/' + subjectId,
            })
              .success(function (response) {
                subject = objectService.createObject('subject');

                _.each(response.result[0], function (value, key) {
                  subject.values[key] = value;
                });
              })
              .error(function (/*response*/) {
                throw systemMessageService.emitError('de betrokkene');
              });

            return ctrl;
          },
        ],
      };
    },
  ]);
})();
