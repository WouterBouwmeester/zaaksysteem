// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular.module('Zaaksysteem.ui').directive('zsPasswordStrengthIndicator', [
    function () {
      var getPasswordStrength = window.zsFetch(
        'nl.mintlab.utils.getPasswordStrength'
      );

      return {
        restrict: 'E',
        scope: {
          password: '@',
        },
        template:
          '<div class="password-strength-indicator password-strength-indicator-<[getPassStrength().status]>">' +
          '<div class="password-strength-indicator-bar">' +
          '<div class="password-strength-indicator-bar-filled">' +
          '</div>' +
          '</div>' +
          '<div class="password-strength-indicator-label" data-ng-switch="getPassStrength().status">' +
          '<span data-ng-switch-default>Ongeldig</span>' +
          '<span data-ng-switch-when="weak">Zwak</span>' +
          '<span data-ng-switch-when="good">Goed</span>' +
          '<span data-ng-switch-when="strong">Sterk</span>' +
          '</div>' +
          '</div>',
        replace: true,
        controller: [
          '$scope',
          function ($scope) {
            $scope.getPassStrength = function () {
              return getPasswordStrength('', $scope.password || '');
            };
          },
        ],
      };
    },
  ]);
})();
