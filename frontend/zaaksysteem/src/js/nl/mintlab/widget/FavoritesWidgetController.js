// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular
    .module('Zaaksysteem')
    .controller('nl.mintlab.widget.FavoritesWidgetController', [
      '$scope',
      'smartHttp',
      'translationService',
      function ($scope, smartHttp, translationService) {
        var safeApply = window.zsFetch('nl.mintlab.utils.safeApply'),
          arrayMove = window.zsFetch('nl.mintlab.utils.collection.arrayMove'),
          hasMoreResults = true,
          configs = {
            casetype: {
              isAddSupported: true,
              title: translationService.get('Favoriete zaaktypen'),
              empty: translationService.get(
                'Geen favoriete zaaktypen toegevoegd'
              ),
            },
            saved_search: {
              isAddSupported: false,
              title: translationService.get('Mijn zoekopdrachten'),
              empty: translationService.get(
                'U heeft geen gedeelde of favoriete zoekopdrachten'
              ),
              more: {
                label: 'Bekijk alle zoekopdrachten',
                link: '/search',
              },
            },
          };

        $scope.loading = false;

        $scope.favorites = [];
        $scope.maxFavs = $scope.maxFavs || 10;

        function loadFavorites() {
          $scope.loading = true;
          smartHttp
            .connect({
              method: 'GET',
              url: '/api/favorite/' + $scope.type + '/list',
            })
            .success(function (response) {
              $scope.favorites = response.result;
              $scope.loading = false;
            })
            .error(function () {
              $scope.loading = false;
            });
        }

        function getCfg(property) {
          var cfg = configs[$scope.type],
            val;

          if (cfg) {
            val = property === undefined ? cfg : cfg[property];
          }

          return val;
        }

        $scope.isAddSupported = function () {
          return configs[$scope.type].isAddSupported;
        };

        $scope.getTitle = function () {
          return configs[$scope.type].title;
        };

        $scope.getEmpty = function () {
          return configs[$scope.type].empty;
        };

        $scope.hasMoreLink = function () {
          return hasMoreResults && getCfg('more');
        };

        $scope.getMoreLink = function () {
          var url,
            cfg = getCfg('more');

          if (cfg) {
            url = cfg.link;
          }

          return url;
        };

        $scope.getMoreLabel = function () {
          var label,
            cfg = getCfg('more');

          if (cfg) {
            label = cfg.label;
          }

          return label;
        };

        $scope.remove = function (item) {
          var index = _.indexOf($scope.favorites, item);

          if (index !== -1) {
            $scope.favorites.splice(index, 1);
          }

          smartHttp
            .connect({
              method: 'POST',
              url: '/api/favorite/' + $scope.type + '/remove',
              data: {
                favorite_id: item.id,
              },
            })
            .success(function (response) {
              var data = response.result[0];

              for (var key in data) {
                item[key] = data[key];
              }
            })
            .error(function (/*response*/) {
              $scope.favorites.splice(index, 0, item);

              $scope.$emit('systemMessage', {
                type: 'error',
                content: translationService.get(
                  'Dit item kon niet uit uw favorieten worden verwijderd. Probeer het later opnieuw.'
                ),
              });
            });
        };

        $scope.$on('zs.sort.update', function (event, data, before) {
          safeApply($scope, function () {
            var favorite,
              to = before
                ? _.findIndex($scope.favorites, { id: before.id })
                : $scope.favorites.length - 1;

            favorite = _.find($scope.favorites, { id: data.id });

            arrayMove($scope.favorites, favorite, to);
          });
        });

        $scope.$on('zs.sort.commit', function (event, data) {
          safeApply($scope, function () {
            var index = _.findIndex($scope.favorites, { id: data.id }),
              after = $scope.favorites[index - 1],
              afterId = after ? after.id : null;

            smartHttp
              .connect({
                url: '/api/favorite/' + $scope.type + '/move',
                method: 'POST',
                data: {
                  favorite_id: data.id,
                  after: afterId,
                },
              })
              .error(function () {
                smartHttp.emitDefaultError();
              });
          });
        });

        $scope.$watch('type', function () {
          if ($scope.type) {
            loadFavorites();
          }
        });

        $scope.$watch(
          'favorites',
          function () {
            // TODO: find a cleaner way to do this
            setTimeout(function () {
              initializeEverything();
            }, 0);
          },
          true
        );

        $scope.$watch('newFav', function () {
          var obj = $scope.newFav,
            fav;

          if (obj) {
            fav = {
              id: -1,
              type: $scope.type,
              object: obj,
            };

            $scope.favorites.push(fav);

            $scope.newFav = null;

            $scope.addfavorite.$setPristine();

            smartHttp
              .connect({
                method: 'POST',
                url: '/api/favorite/' + $scope.type + '/add',
                data: {
                  object_id: obj.id,
                },
              })
              .success(function (response) {
                var data = response.result[0];

                for (var key in data) {
                  fav[key] = data[key];
                }
              })
              .error(function (/*response*/) {
                var index = _.indexOf($scope.favorites, fav);
                if (index !== -1) {
                  $scope.favorites.splice(index, 1);
                }
                $scope.$emit('systemMessage', {
                  type: 'error',
                  content: translationService.get(
                    'Er ging iets fout bij het toevoegen van dit item aan uw favorieten. Probeer het later opnieuw.'
                  ),
                });
              });
          }
        });
      },
    ]);
})();
