// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular
    .module('Zaaksysteem.widget.search')
    .controller('nl.mintlab.widget.search.FilterPositionAddController', [
      '$scope',
      'organisationalUnitService',
      'objectService',
      'translationService',
      function (
        $scope,
        organisationalUnitService,
        objectService,
        translationService
      ) {
        var controlVisible = false,
          orgUnitId,
          roleId;

        $scope.showControl = function () {
          controlVisible = true;
        };

        $scope.hideControl = function () {
          controlVisible = false;
        };

        $scope.isControlVisible = function () {
          return controlVisible;
        };

        $scope.getOrgUnitId = function () {
          return orgUnitId;
        };

        $scope.getRoleId = function () {
          return roleId;
        };

        $scope.handlePositionChange = function ($orgUnitId, $roleId) {
          orgUnitId = $orgUnitId;
          roleId = $roleId;
        };

        $scope.confirm = function () {
          var entity = objectService.createPositionEntity(orgUnitId, roleId),
            rule = objectService.createSecurityRule(
              entity.entity_id,
              entity.entity_type,
              'read'
            );

          if (objectService.hasSecurityRule($scope.filter, rule)) {
            $scope.$emit('systemMessage', {
              type: 'error',
              content: translationService.get(
                'Deze positie heeft al raadpleegrechten voor deze zoekopdracht'
              ),
            });
          } else {
            objectService.addSecurityRule($scope.filter, rule);
            $scope.hideControl();
          }
        };

        organisationalUnitService.getUnits().then(function () {
          var defaults = organisationalUnitService.getDefaults();

          orgUnitId = defaults.org_unit_id;
          roleId = defaults.role_id;
        });
      },
    ]);
})();
