// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

(function () {
  var win = window;

  window.zsDefine('nl.mintlab.utils.events.addEventListener', function () {
    if (win.addEventListener) {
      return function (dispatcher, type, listener, useCapture) {
        dispatcher.addEventListener(type, listener, useCapture);
      };
    } else if (win.attachEvent) {
      return function (dispatcher, type, listener, useCapture) {
        if (useCapture) {
          //console.log('useCapture not supported in this browser');
        }
        dispatcher.attachEvent('on' + type, listener);
      };
    }

    throw new Error('events not supported in this browser');
  });
})();
