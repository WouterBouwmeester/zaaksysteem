// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular.module('Zaaksysteem').directive('zsAlwaysVisibleParent', [
    function () {
      return {
        controller: [
          '$scope',
          function ($scope) {
            var ctrl = this,
              element;

            function trigger() {
              $scope.$broadcast('zs.always.visible.reference.update', element);
            }

            return _.assign(ctrl, {
              getElement: function () {
                return element;
              },
              setElement: function (el) {
                element = el;
                trigger();
              },
              trigger: trigger,
            });
          },
        ],
      };
    },
  ]);
})();
