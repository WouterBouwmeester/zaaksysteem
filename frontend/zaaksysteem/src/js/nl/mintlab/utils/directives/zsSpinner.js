// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular.module('Zaaksysteem').directive('zsSpinner', [
    function () {
      // .zs-spinner = positioned to parent, hidden, medium
      // .zs-spinner.zs-spinner-fixed = positioned to document
      // .zs-spinner.zs-spinner-visible = visible

      // .zs-spinner-[small|medium|large] = set size of icon

      return {
        restrict: 'A',
        template:
          '<div class="zs-spinner" data-ng-class="{\'zs-spinner-visible\': spinner.isActive() }"><div class="zs-spinner-icon"></div></div>',
        replace: true,
        controller: [
          '$scope',
          '$element',
          '$attrs',
          function ($scope, $element, $attrs) {
            var ctrl = this;

            ctrl.isActive = function () {
              return $scope.$eval($attrs.zsSpinner);
            };

            return ctrl;
          },
        ],
        controllerAs: 'spinner',
      };
    },
  ]);
})();
