// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular
    .module('Zaaksysteem.sysin.records')
    .controller('nl.mintlab.sysin.records.RecordController', [
      '$scope',
      '$window',
      function ($scope, $window) {
        $scope.getRecordMutations = function (item) {
          var mutations = [];
          if (item.parsed_mutations) {
            return item.parsed_mutations;
          }
          // parsing the mutations on the fly every time results
          // in a strange digestion loop
          try {
            mutations = JSON.parse(item.mutations);
            item.parsed_mutations = mutations;
          } catch (error) {}
          return mutations;
        };

        $scope.$on('detail.item.change', function (event, item) {
          $scope.record = item;
        });
      },
    ]);
})();
