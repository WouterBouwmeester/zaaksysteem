// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular
    .module('Zaaksysteem.sysin.datastore')
    .controller('nl.mintlab.sysin.datastore.DatastoreController', [
      '$scope',
      '$http',
      'translationService',
      '$window',
      function ($scope, $http, translationService, $window) {
        $scope.options = [];

        $scope.downloadTable = function () {
          $http({
            method: 'GET',
            url: '/datastore/csv/' + $scope.cl,
          })
            .success(function (response) {
              $scope.$emit('systemMessage', {
                type: 'info',
                content:
                  'Exportbestand wordt gegenereerd. Er wordt een bericht na afronding verstuurd.',
              });
            })
            .error(function (/*response*/) {
              $scope.$emit('systemMessage', {
                type: 'error',
                content: translationService.get(
                  'Er is een onbekende fout opgetreden!'
                ),
              });
            });
        };

        $http({
          method: 'GET',
          url: '/datastore/classes',
        })
          .success(function (response) {
            $scope.options = response.result;
            $scope.cl = $scope.options[0];
          })
          .error(function (/*response*/) {
            $scope.$emit('systemMessage', {
              type: 'error',
              content: translationService.get(
                'Er ging iets fout bij het ophalen van de gegevens. Probeer het later opnieuw.'
              ),
            });
          });
      },
    ]);
})();
