// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular
    .module('Zaaksysteem.form')
    .controller('nl.mintlab.core.form.FormFieldsetController', [
      '$scope',
      function ($scope) {
        $scope.toggleCollapse = function () {
          $scope.fieldset.collapsed = !$scope.fieldset.collapsed;
        };

        $scope.isChanged = function () {
          var isChanged = _.some($scope.fieldset.fields, function (field) {
            return !field.ignore && !$scope.isDefaultValue(field);
          });

          return isChanged;
        };

        $scope.isEmpty = function () {
          var isEmpty = _.every($scope.fieldset.fields, function (field) {
            return field.ignore || $scope.isFieldEmpty(field);
          });

          return isEmpty;
        };

        $scope.revert = function (event) {
          _.each($scope.fieldset.fields, function (field) {
            $scope.revertField(field);
            if (event) {
              $scope.$emit('form.change.committed', field);
            }
          });
          if (event) {
            event.stopPropagation();
          }
        };
      },
    ]);
})();
