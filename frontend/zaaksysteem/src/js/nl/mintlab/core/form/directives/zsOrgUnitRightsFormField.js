// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular.module('Zaaksysteem.form').directive('zsOrgUnitRightsFormField', [
    'organisationalUnitService',
    function (organisationalUnitService) {
      var rightTypes = [
        {
          id: 'zaak_beheer',
          label: 'Mag zaken beheren',
        },
        {
          id: 'zaak_edit',
          label: 'Mag zaken behandelen',
        },
        {
          id: 'zaak_read',
          label: 'Mag zaken raadplegen',
        },
        {
          id: 'zaak_search',
          label: 'Mag zaken zoeken',
        },
      ];

      return {
        require: ['zsOrgUnitRightsFormField', 'zsArrayModel'],
        controller: [
          '$scope',
          '$attrs',
          function ($scope, $attrs) {
            var ctrl = this,
              arrayModel,
              types = angular.copy(rightTypes),
              availableRights = $scope.$eval($attrs.availableRights);

            if (availableRights) {
              types = _.filter(types, function (right) {
                return availableRights.indexOf(right.id) !== -1;
              });
            }

            ctrl.getRightTypes = function () {
              return types;
            };

            ctrl.addRight = function () {
              organisationalUnitService.getUnits().then(function () {
                var right = {
                    zaak_beheer: false,
                    zaak_read: false,
                    zaak_edit: false,
                    zaak_search: false,
                  },
                  orgUnitId = organisationalUnitService.getDefaultOrgUnitId(),
                  roleId = organisationalUnitService.getDefaultRoleIdForOrgUnitId(
                    orgUnitId
                  );

                right.org_unit_id = orgUnitId;
                right.role_id = roleId;

                arrayModel.addObject(right);
              });
            };

            ctrl.removeRight = function (right) {
              arrayModel.removeObject(right);
            };

            ctrl.setArrayModel = function (model) {
              arrayModel = model;
            };

            ctrl.handlePositionChange = function (right, $orgUnitId, $roleId) {
              right.org_unit_id = $orgUnitId;
              right.role_id = $roleId;
            };

            ctrl.getRights = function () {
              return arrayModel.getList();
            };

            return ctrl;
          },
        ],
        controllerAs: 'orgUnitRightsFormField',
        link: function (scope, element, attrs, controllers) {
          var zsOrgUnitRightsFormField = controllers[0],
            arrayModel = controllers[1];

          zsOrgUnitRightsFormField.setArrayModel(arrayModel);
        },
      };
    },
  ]);
})();
