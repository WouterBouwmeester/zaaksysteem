// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular.module('Zaaksysteem.form').directive('zsPriceFormField', [
    '$locale',
    'currencyFilter',
    function ($locale, currencyFilter) {
      return {
        require: 'ngModel',
        priority: 100,
        link: function (scope, element, attrs, ngModel) {
          var groupSep = $locale.NUMBER_FORMATS.GROUP_SEP,
            decSep = $locale.NUMBER_FORMATS.DECIMAL_SEP;

          ngModel.$formatters.push(function (val) {
            return currencyFilter(val, '');
          });

          ngModel.$parsers.push(function (val) {
            if (!val) {
              val = '';
            }
            val = val.replace(new RegExp('\\' + groupSep), '');
            val = val.replace(new RegExp('\\' + decSep, 'g'), '.');

            if (val !== '') {
              val = Number(val);
            }

            return val;
          });

          element.bind('blur', function () {
            element.val(currencyFilter(ngModel.$modelValue, ''));
          });
        },
      };
    },
  ]);
})();
