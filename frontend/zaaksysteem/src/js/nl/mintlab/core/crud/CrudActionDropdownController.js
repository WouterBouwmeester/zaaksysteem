// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular
    .module('Zaaksysteem')
    .controller('nl.mintlab.core.crud.CrudActionDropDownController', [
      '$scope',
      function ($scope) {
        $scope.$on('zs.ezra.dialog.open', function () {
          $scope.closePopupMenu();
        });

        $scope.$on('zs.crud.action.click', function () {
          $scope.closePopupMenu();
        });
      },
    ]);
})();
