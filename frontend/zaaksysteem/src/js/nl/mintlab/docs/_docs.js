// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

angular.module('Zaaksysteem.docs', [
  'Zaaksysteem.events',
  'Zaaksysteem.net',
  'Zaaksysteem.dom',
  'Zaaksysteem.message',
  'Zaaksysteem.directives',
  'Zaaksysteem.filters',
  'Zaaksysteem.core.user',
  'Zaaksysteem.core.data',
]);
