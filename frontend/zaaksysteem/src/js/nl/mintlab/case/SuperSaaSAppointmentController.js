// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  'use strict';

  angular
    .module('Zaaksysteem.case')
    .controller('nl.mintlab.case.SuperSaaSAppointmentController', [
      '$scope',
      'smartHttp',
      'translationService',
      'dateFilter',
      function ($scope, smartHttp, translationService, dateFilter) {
        var safeApply = window.zsFetch('nl.mintlab.utils.safeApply');

        $scope.dates = [];
        $scope.times = [];

        function getDates() {
          smartHttp
            .connect({
              method: 'GET',
              url: '/api/supersaas/index',
              params: {
                action: 'getAvailableAppointmentDays',
                productLinkID: $scope.productLinkID,
                zapi_no_pager: 1,
              },
            })
            .success(function (response) {
              $scope.dates = response.result;
            })
            .error(function (response) {
              broadcastError(response);
            });
        }

        function removeDuplicates(arr, prop1, prop2) {
          var new_arr = [],
            lookup = {},
            i = arr.length;

          while (i--) {
            lookup[arr[i][prop1] + arr[i][prop2]] = arr[i];
          }

          for (i in lookup) {
            new_arr.push(lookup[i]);
          }

          return new_arr.reverse();
        }

        function getTimes() {
          smartHttp
            .connect({
              method: 'GET',
              url: '/api/supersaas/index',
              params: {
                action: 'getAvailableAppointmentTimes',
                productLinkID: $scope.productLinkID,
                appDate: $scope.appointmentDate,
                zapi_no_pager: 1,
              },
            })
            .success(function (response) {
              $scope.times = removeDuplicates(
                response.result,
                'start',
                'finish'
              );
            })
            .error(function (response) {
              broadcastError(response);
            });
        }

        function broadcastError(response) {
          var message = {
              type: 'error',
              content: translationService.get(
                'Er ging iets fout bij het ophalen van de beschikbare data. Probeer het later opnieuw.'
              ),
            },
            type =
              response.result && response.result[0]
                ? response.result[0].type
                : null;

          if (type === 'supersaas/config_missing') {
            message.content = translationService.get(
              'Er is nog geen SuperSaaS koppeling geconfigureerd.'
            );
          }

          $scope.$emit('systemMessage', message);
        }

        function updateZSCalendarAttribute() {
          var attributeName = $scope.inputFieldName,
            changes = {
              update: attributeName,
            };

          changes[attributeName] =
            $scope.appointmentTimeStart +
            ';' +
            $scope.appointmentTimeFinish +
            ';' +
            $scope.productLinkID;

          smartHttp
            .connect({
              method: 'POST',
              url:
                '/pip/zaak/' +
                $scope.$parent.caseId +
                '/request_attribute_update/' +
                $scope.$parent.bibliotheekId,
              data: changes,
            })
            .success(function (response) {
              $scope.$emit('systemMessage', {
                type: 'info',
                content: translationService.get(
                  'Uw afspraak is geboekt op ' +
                    dateFilter($scope.appointmentTimeStart, 'longDate') +
                    ' om ' +
                    dateFilter($scope.appointmentTimeStart, 'shortTime')
                ),
              });
            })
            .error(function (/*response*/) {
              $scope.$emit('systemMessage', {
                type: 'error',
                content: translationService.get(
                  'Er ging iets fout bij het opslaan van uw wijziging. Probeer het later opnieuw'
                ),
              });
            });
        }

        $scope.setAppointmentTime = function (time) {
          $scope.appointmentTimeStart = time.start;
          $scope.appointmentTimeFinish = time.finish;
          $scope.appointmentResourceID = time.resource_id;
        };

        $scope.setAppointmentDate = function (date) {
          $scope.appointmentDate = date;
        };

        $scope.deleteAppointment = function () {
          smartHttp
            .connect({
              method: 'POST',
              url: '/api/supersaas/index',
              params: {
                action: 'deleteAppointment',
                productLinkID: $scope.productLinkID,
                appointmentId: $scope.appointmentId,
              },
            })
            .success(function (/*response*/) {
              $scope.confirmedAppointmentDate = null;
              $scope.confirmedAppointmentTime = null;

              $scope.$emit('systemMessage', {
                type: 'info',
                content: translationService.get('Uw afspraak is verwijderd'),
              });
              $scope.appointment = null;
              $scope.closePopup();
            })
            .error(function (/*response*/) {});
        };

        $scope.book = function () {
          smartHttp
            .connect({
              method: 'POST',
              url: '/api/supersaas/index',
              params: {
                action: 'bookAppointment',
                appTimeStart: $scope.appointmentTimeStart,
                appTimeFinish: $scope.appointmentTimeFinish,
                resourceID: $scope.appointmentResourceID,
                productLinkID: $scope.productLinkID,
                aanvrager: $scope.aanvrager,
                previous_appointment_id: $scope.appointmentId,
              },
            })
            .success(function (response) {
              $scope.confirmedAppointmentTimeStart =
                $scope.appointmentTimeStart;
              $scope.confirmedAppointmentTimeFinish =
                $scope.appointmentTimeFinish;

              $scope.appointment = [
                $scope.confirmedAppointmentTimeStart,
                $scope.confirmedAppointmentTimeFinish,
                response.result[0].appointmentId,
              ].join(';');

              // When we are on the PIP, we want to skip the extra confirmation step, and immediately update the attribute on ZS.

              if ($scope.pip) {
                updateZSCalendarAttribute();
              } else {
                $scope.$emit('systemMessage', {
                  type: 'info',
                  content: translationService.get(
                    'Uw afspraak is geboekt op ' +
                      dateFilter($scope.appointmentTimeStart, 'longDate') +
                      ' om ' +
                      dateFilter($scope.appointmentTimeStart, 'shortTime')
                  ),
                });
              }

              $scope.closePopup();
            })
            .error(function (/*response*/) {});
        };

        $scope.getTimeLabel = function (time) {
          var start = new Date(time.start).getTime(),
            finish = new Date(time.finish).getTime(),
            from,
            to;

          if (isNaN(start)) {
            // we're dealing with an old browser here,
            // unable to parse iso formats
            start = new Date(
              time.start
                .replace(/-/g, '/')
                .replace('00.000', '')
                .replace(/(.*):/, '$1')
            ).getTime();
            finish = new Date(
              time.finish
                .replace(/-/g, '/')
                .replace('00.000', '')
                .replace(/(.*):/, '$1')
            ).getTime();
          }

          from = dateFilter(start, 'HH:mm');
          to = dateFilter(finish, 'HH:mm');

          return from + ' - ' + to;
        };

        $scope.$on('popupopen', function (/*event*/) {
          $scope.appointmentDate = null;
          $scope.appointmentTimeStart = null;
          $scope.appointmentTimeFinish = null;

          safeApply($scope, function () {
            if ($scope.productLinkID) {
              getDates();
            } else {
              $scope.$emit('systemMessage', {
                type: 'error',
                content: translationService.get(
                  'Er is iets misgegaan. Neem contact op met de gemeente.'
                ),
              });
              $scope.closePopup();
            }
          });
        });

        $scope.$watch('appointmentDate', function () {
          if ($scope.appointmentDate) {
            getTimes();
          } else {
            $scope.times = [];
          }
        });

        $scope.$watch('appointment', function () {
          var parts = ($scope.appointment || '').split(';');

          $scope.confirmedAppointmentTimeStart = parts[0];
          $scope.confirmedAppointmentTimeFinish = parts[1];
          $scope.appointmentId = parts[2];
        });
      },
    ]);
})();
