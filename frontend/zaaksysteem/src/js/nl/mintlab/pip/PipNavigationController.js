// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular
    .module('Zaaksysteem.pip')
    .controller('nl.mintlab.pip.PipNavigationController', [
      '$scope',
      '$element',
      function ($scope, $element) {
        $scope.onMenuClick = function () {
          document.getElementById('toggle').checked = !document.getElementById(
            'toggle'
          ).checked;
        };

        $(document).on('keydown', '.pip-nav-menu', function (event) {
          if (
            event.originalEvent.code === 'Tab' &&
            event.target.getAttribute('position') === 'last'
          ) {
            document.getElementById('pip-nav-close').focus();
          }
        });
      },
    ]);
})();
