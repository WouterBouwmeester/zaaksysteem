// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
(function () {
  angular
    .module('Zaaksysteem.pip')
    .controller('nl.mintlab.pip.TiwikAnalyticsController', [
      '$scope',
      function ($scope) {
        $scope.tiwikAnalytics = function (id) {
          var _paq = _paq || [];

          _paq.push([
            'setDocumentTitle',
            document.domain + '/' + document.title,
          ]);

          _paq.push(['setCookieDomain', '*.epe.nl']);

          _paq.push(['setDomains', ['*.epe.nl']]);

          _paq.push(['trackPageView']);

          _paq.push(['enableLinkTracking']);

          function setAnalytics() {
            var u = '//www.simanalytics.nl/';

            _paq.push(['setTrackerUrl', u + 'piwik.php']);

            _paq.push(['setSiteId', id]);

            var d = document,
              g = d.createElement('script'),
              s = d.getElementsByTagName('script')[0];

            g.type = 'text/javascript';
            g.async = true;
            g.defer = true;
            g.src = u + 'piwik.js';
            s.parentNode.insertBefore(g, s);
          }

          setAnalytics();
        };
      },
    ]);
})();
