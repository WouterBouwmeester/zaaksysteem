#!/bin/bash

set -e

db=zaaksysteem
hostname=dev.zaaksysteem.nl

usage() {
    cat <<OEF

    $(basename $0) OPTIONS

    h       This help
    u       Hostname
    d       Database name
    p       Port number
    f       File to upload
    n       New file to upload
    l       Use localhost for DB queries
    c       Use the client certificate
    v       Debug this tool
OEF
    exit 0

}

while getopts "hd:f:u:n:lvc:p:" name
do
    case $name in
        h) usage;;
        d) db=$OPTARG;;
        f) file=$OPTARG;;
        n) new_file=$OPTARG;;
        u) hostname=$OPTARG;;
        l) PSQL_OPTS='-h localhost -U zaaksysteem';;
        p) PSQL_PORT=$OPTARG;;
        c) client_cert=$OPTARG;;
        v) set -x;;
    esac
done
shift $((OPTIND - 1))

[ -n "$PSQL_PORT" ] && PSQL_OPTS="$PSQL_OPTS -p $PSQL_PORT"


do_sql() {
    psql -X -q -t $PSQL_OPTS $db "$*"
}

sql_exec_echo() {
    echo $(do_sql -c "$@")
}

die() {
    echo "$@" 1>&2
    exit 1
}


INTERFACE=$(sql_exec_echo \
    "SELECT uuid,id FROM interface WHERE module='xential' and active = true and date_deleted is null ORDER BY id DESC LIMIT 1")

INTERFACE_UUID=$(echo $INTERFACE | awk -F \|\  '{print $1}');
INTERFACE_ID=$(echo $INTERFACE | awk -F \| '/|/{gsub(/ /, "", $2); print $2}');

get_data() {

    local processor=$1
    if [ -z "$processor" ]
    then
        sql="processor_params is not null"
    else
        sql="processor_params::jsonb @> jsonb_build_object('processor', '$processor')"
    fi

    TID=$(sql_exec_echo "SELECT uuid FROM transaction WHERE interface_id = $INTERFACE_ID and $sql order by id desc limit 1");

    if [[ -z $TID ]];
    then
        if [[ -z $processor ]]
        then
            die "No transaction found for interface with ID $INTERFACE_ID"
        else
            die "No transaction ($processor) found for interface with ID $INTERFACE_ID"
        fi

    fi

    PROC_PARAMS=$(sql_exec_echo "SELECT processor_params from transaction WHERE interface_id = $INTERFACE_ID and uuid = '$TID'")

    echo $PROC_PARAMS | jq

    CID=$(echo $PROC_PARAMS | jq -r '.case');

    # Empty CID means we processed the transaction already, for testing
    # purposes we override the $CID

    if [ -z "$CID" ]
    then
        CASEUUID=$(echo $PROC_PARAMS | jq -r '.case_uuid');
    else
        CASEUUID=$(sql_exec_echo "SELECT uuid from zaak WHERE id = $CID")
    fi

    FILE_UUID=$(echo $PROC_PARAMS | jq -r '.filestore_uuid');

    BASE_URL="https://$hostname/api/v1/sysin/interface/$INTERFACE_UUID/trigger/api_post_file?transaction_uuid=$TID&case_uuid=$CASEUUID"
}

if [ -z "$file" ] && [ -z "$new_file" ]
then
    echo "No file or new file given, found the following transaction";
    get_data _create_file_from_template
    get_data _process_request_edit_file
    exit 2;
fi

CURL_OPTS='-q -k'

if [ -n "$client_cert" ]
then

    CURL_OPTS="$CURL_OPTS --cert $client_cert --key $client_cert"

fi

if [ -n "$file" ]
then

    get_data _create_file_from_template

    curl $CURL_OPTS --form "upload=@$file"  $BASE_URL | jq

    FILE_UUID=$(sql_exec_echo "SELECT fs.uuid FROM filestore fs JOIN file f on f.filestore_id = fs.id and f.generator = 'xential' and f.active_version = true order by f.id desc limit 1");
    echo "Added file $FILE_UUID";
fi

if [ -n "$new_file" ]
then
    get_data _process_request_edit_file
    curl $CURL_OPTS --form "upload=@$new_file" \
        "$BASE_URL&file_uuid=$FILE_UUID" | jq
fi
