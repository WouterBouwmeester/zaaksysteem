#!/usr/bin/env bash

USER=$1
PASS=$2
URL=$3
ID=$4
JSON=$5

opts="-k --digest -u '$USER:$PASS' '$URL' --verbose"

if [ -n "$ID" ]
then
    opts="$opts -H 'API-Interface-ID: $ID'"
fi

if [ -n "$JSON" ]
then
    opts="$opts -H 'Content-Type: application/json' -d @$JSON"
fi

echo curl $opts
eval curl $opts
