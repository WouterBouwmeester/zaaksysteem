BEGIN;

  ALTER TABLE natuurlijk_persoon ALTER COLUMN preferred_contact_channel DROP NOT NULL;
  ALTER TABLE gm_natuurlijk_persoon ALTER COLUMN preferred_contact_channel DROP NOT NULL;
  ALTER TABLE bedrijf ALTER COLUMN preferred_contact_channel DROP NOT NULL;
  ALTER TABLE gm_bedrijf ALTER COLUMN preferred_contact_channel DROP NOT NULL;

COMMIT;
