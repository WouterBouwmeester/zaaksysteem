BEGIN;

  DROP INDEX IF EXISTS logging_zaak_view_idx;
  CREATE INDEX logging_zaak_view_idx ON logging(zaak_id)
    WHERE (component = 'zaak'
    AND event_type  = 'case/view')
  ;

COMMIT;
