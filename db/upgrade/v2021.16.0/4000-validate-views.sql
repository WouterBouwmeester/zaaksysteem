BEGIN;


  DROP VIEW IF EXISTS case_documents CASCADE;

  CREATE VIEW case_documents AS
  SELECT
      z.id AS case_id,
      (COALESCE(array_agg(fs.uuid) FILTER (WHERE fs.uuid IS NOT NULL), '{}'))::text[] as value,
      bk.magic_string as magic_string,
      bk.id as library_id
    FROM
      zaak z
    JOIN
      zaaktype_kenmerken ztk
    ON
      (z.zaaktype_node_id = ztk.zaaktype_node_id)
    JOIN
      bibliotheek_kenmerken bk
    ON (ztk.bibliotheek_kenmerken_id = bk.id and bk.value_type = 'file')
    LEFT JOIN file_case_document fcd
      JOIN file f
        JOIN filestore fs ON f.filestore_id = fs.id
          ON fcd.file_id = f.id
           ON (z.id = fcd.case_id and bk.id = fcd.bibliotheek_kenmerken_id)
    GROUP BY 1, 3, 4
  ;

  CREATE VIEW case_attributes_v1 AS
    SELECT case_id, magic_string, library_id, attribute_value_to_jsonb(value, value_type) as value FROM case_attributes
  UNION ALL
    SELECT case_id, magic_string, library_id, appointment_attribute_value_to_jsonb(value, reference) as value FROM case_attributes_appointments
  UNION ALL
    SELECT case_id, magic_string, library_id, attribute_value_to_jsonb(value, 'file') from case_documents
  ;

  CREATE OR REPLACE FUNCTION attribute_value_to_jsonb(
    IN value text[],
    IN value_type text,
    OUT value_json jsonb
  )
  LANGUAGE plpgsql
  AS $$
  DECLARE
    m text;
    length int;
  BEGIN

      value_json := '[]'::jsonb;
      SELECT INTO length cardinality(COALESCE(value, '{}'::text[]));

      IF length = 0 and value_type = 'file'
      THEN
        value_json := '[]'::jsonb;
        RETURN;
      ELSIF length = 0
      THEN
        value_json := '[null]'::jsonb;
        RETURN;
      END IF;

      IF value_type IN ('geojson', 'address_v2', 'appointment_v2', 'relationship')
      THEN

          FOREACH m IN ARRAY value
          LOOP
            SELECT INTO value_json value_json || jsonb_build_array((CONCAT('[', m::text, ']')::json ->> 0)::json);
          END LOOP;

      ELSIF value_type IN ('checkbox', 'select')
      THEN
        SELECT INTO value_json to_jsonb(ARRAY[value]);

      ELSE
        SELECT INTO value_json to_jsonb(value);
      END IF;
      RETURN;
  END;
  $$;

  CREATE OR REPLACE FUNCTION timestamp_to_perl_datetime(
    IN dt timestamp with time zone,
    OUT tt text
  )
  LANGUAGE plpgsql
  AS $$
  BEGIN
    SELECT INTO tt to_char(dt::timestamp with time zone at time zone 'UTC', CONCAT('YYYY-MM-DD"T"HH24:MI:SS"Z"'));
  END;
  $$;

  CREATE OR REPLACE FUNCTION position_matrix(
    IN groups int[],
    IN roles int[],
    OUT json JSONB
  )
  LANGUAGE plpgsql
  AS $$
  DECLARE
    role_id int;
    group_id int;

    gr jsonb;
    role json;

    pos jsonb;
  BEGIN

      pos := '[]'::jsonb; -- || does nothing when value is NULL

      FOREACH group_id IN ARRAY groups
      LOOP
        SELECT INTO gr v1_json FROM groups WHERE id = group_id;
        FOREACH role_id IN ARRAY roles
        LOOP
          SELECT INTO role v1_json FROM roles WHERE id = role_id;
          SELECT INTO pos pos || jsonb_build_object(
            'preview', 'position(unsynched)',
            'reference', NULL,
            'type', 'position',
            'instance', json_build_object(
              'date_modified', timestamp_to_perl_datetime(NOW()),
              'date_created', timestamp_to_perl_datetime(NOW()),
              'group', gr,
              'role', role
            )
          );
        END LOOP;
      END LOOP;

      SELECT INTO json json_build_object(
        'type', 'set',
        'reference', null,
        'instance', json_build_object(
          'pager', null,
          'rows', pos
        )
      );
  END;
  $$;

  CREATE OR REPLACE FUNCTION subject_employee_json(
    IN subject hstore,
    OUT json JSONB
  )
  LANGUAGE plpgsql
  AS $$
  DECLARE
    properties jsonb;
    preview text;
    positions jsonb;

  BEGIN
      properties := (subject->'properties')::jsonb;

      SELECT INTO json subject_json(subject, 'employee');

      SELECT INTO positions position_matrix(
        COALESCE((subject->'group_ids')::int[], '{}'::int[]),
        COALESCE((subject->'role_ids')::int[], '{}'::int[])
      );

      SELECT INTO json json || jsonb_build_object(
        'instance', json_build_object(
          'username', subject->'username',
          'initials', properties->'initials',
          'first_names', properties->'givenname',
          'surname', properties->'sn',
          'display_name', properties->'displayname',
          'email_address', properties->'mail',
          'phone_number', properties->'telephonenumber',
          'settings', (subject->'settings')::jsonb,
          'positions', positions,
          'date_modified', timestamp_to_perl_datetime((subject->'last_modified')::timestamp with time zone),
          'date_created', timestamp_to_perl_datetime((subject->'last_modified')::timestamp with time zone)
        )
      );
      RETURN;
  END;
  $$;

  CREATE OR REPLACE FUNCTION subject_person_json(
    IN subject hstore,
    OUT json JSONB
  )
  LANGUAGE plpgsql
  AS $$
  DECLARE

    address json;
    address_cor json;
    address_res json;

    partner json;
    surname text;

  BEGIN

    SELECT INTO json subject_json(subject, 'person');

    address := json_build_object(
      'preview', 'address(unsynched)',
      'reference', null,
      'type', 'address',
      'instance', json_build_object(
        'bag_id', subject->'bag_id',
        'city', subject->'city',
        'street', subject->'street',
        'street_number', (subject->'street_number')::int,
        'street_number_suffix', subject->'street_number_suffix',
        'street_number_letter', subject->'street_number_letter',
        'foreign_address_line1', subject->'foreign_address_line1',
        'foreign_address_line2', subject->'foreign_address_line2',
        'foreign_address_line3', subject->'foreign_address_line3',
        'zipcode', subject->'zipcode',
        'country', (subject->'country')::jsonb,
        'municipality', (subject->'municipality')::jsonb,
        'latitude', (subject->'latitude')::float,
        'longitude', (subject->'longitude')::float,
        'date_created', timestamp_to_perl_datetime(NOW()),
        'date_modified', timestamp_to_perl_datetime(NOW())
      )
    );

    IF subject->'address_type' = 'W' THEN
          address_res := address;
    ELSE
          address_cor := address;
    END IF;

    IF   subject->'partner_a_nummer' IS NOT NULL
      OR subject->'partner_burgerservicenummer' IS NOT NULL
      OR subject->'partner_geslachtsnaam' IS NOT NULL
      OR subject->'partner_voorvoegsel' IS NOT NULL
    THEN

      IF subject->'partner_voorvoegsel' IS NOT NULL
      THEN
        surname := CONCAT(subject->'partner_voorvoegsel', ' ', subject->'partner_geslachtsnaam');
      ELSE
        surname := subject->'partner_geslachtsnaam';
      END IF;

      SELECT INTO partner json_build_object(
        'instance', json_build_object(
          'surname', surname,
          'family_name' , subject->'partner_geslachtsnaam',
          'prefix', subject->'partner_voorvoegsel',
          'personal_number', lpad(subject->'partner_burgerservicenummer', 9, '0'),
          'personal_number_a', lpad(subject->'partner_a_nummer', 10, '0'),
          -- We have no information on these items
          'first_names' , null,
          'use_of_name', null,
          'is_local_resident', false,
          'gender', null,
          'noble_title', null,
          'place_of_birth', null,
          'initials', null,
          'date_of_birth', null,
          'is_secret', false,
          'email_address', null,
          'mobile_phone_number', null,
          'address_correspondence', null,
          'address_residence', null,
          'date_of_death', null,
          'phone_number', null,
          'partner', null,
          'date_created', timestamp_to_perl_datetime(NOW()),
          'date_modified', timestamp_to_perl_datetime(NOW())
        ),
        'type', 'person',
        'preview', 'person(unsynched)',
        'reference', null
      );
    END IF;

    SELECT INTO json json || jsonb_build_object(
      'instance', json_build_object(
        'address_residence', address_res,
        'address_correspondence', address_cor,
        'email_address', subject->'email_address',
        'phone_number', subject->'phone_number',
        'mobile_phone_number', subject->'mobile',
        'partner', partner,
        -- non-existent data
        'date_created', timestamp_to_perl_datetime(NOW()), -- np table?
        'date_modified', timestamp_to_perl_datetime(NOW()), -- np table?
        -- we have it
        'date_of_birth', (subject->'geboortedatum')::date,
        'date_of_death', (subject->'datum_overlijden')::date,
        'family_name', subject->'geslachtsnaam',
        'first_names', subject->'voornamen',
        'gender', subject->'geslachtsaanduiding',
        'initials', subject->'voorletters',
        'is_local_resident', (subject->'in_gemeente')::boolean,
        'is_secret', CASE WHEN subject->'indicatie_geheim' = '0' THEN false ELSE true END,
        'noble_title', subject->'adellijke_titel',
        'personal_number', lpad(subject->'burgerservicenummer', 9, '0'),
        'personal_number_a', lpad(subject->'a_nummer', 10, '0'),
        'place_of_birth', subject->'geboorteplaats',
        'prefix', subject->'aanhef_aanschrijving',
        'surname', COALESCE(subject->'naamgebruik', subject->'geslachtsnaam'),
        'use_of_name', subject->'aanduiding_naamgebruik'
      )
    );

    RETURN;
  END;
  $$;

  CREATE OR REPLACE FUNCTION subject_company_json(
    IN subject hstore,
    OUT json JSONB
  )
  LANGUAGE plpgsql
  AS $$
  DECLARE

    address_cor json;
    address_res json;

    latitude float;
    longitude float;

  BEGIN

    SELECT INTO json subject_json(subject, 'company');

    latitude := ((subject->'vestiging_latlong')::point)[0];
    longitude := ((subject->'vestiging_latlong')::point)[1];

    IF (
        subject->'vestiging_adres_buitenland1' IS NOT NULL
        AND
        subject->'vestiging_adres_buitenland2' IS NOT NULL
        AND
        subject->'vestiging_adres_buitenland3' IS NOT NULL
        )
      OR (subject->'vestiging_straatnaam' IS NOT NULL
          AND subject->'vestiging_postcode' IS NOT NULL
      )
    THEN
      address_res := json_build_object(
        'preview', 'address(unsynched)',
        'reference', null,
        'type', 'address',
        'instance', json_build_object(
          'bag_id', (subject->'vestiging_bag_id')::bigint,
          'city', subject->'vestiging_woonplaats',
          'street', subject->'vestiging_straatnaam',
          'street_number', (subject->'vestiging_huisnummer')::int,
          'street_number_suffix', subject->'vestiging_huisnummertoevoeging',
          'street_number_letter', subject->'vestiging_huisletter',
          'foreign_address_line1', subject->'vestiging_adres_buitenland1',
          'foreign_address_line2', subject->'vestiging_adres_buitenland2',
          'foreign_address_line3', subject->'vestiging_adres_buitenland3',
          'country', (subject->'vestiging_country')::jsonb,
          'zipcode', subject->'vestiging_postcode',
          'latitude', latitude,
          'longitude', longitude,
          -- non-existent data
          'municipality', null,
          'date_created', timestamp_to_perl_datetime(NOW()),
          'date_modified', timestamp_to_perl_datetime(NOW())
        )
      );
    END IF;

    IF (
        subject->'correspondentie_adres_buitenland1' IS NOT NULL
        AND
        subject->'correspondentie_adres_buitenland2' IS NOT NULL
        AND
        subject->'correspondentie_adres_buitenland3' IS NOT NULL
        )
      OR (subject->'correspondentie_straatnaam' IS NOT NULL
          AND subject->'correspondentie_postcode' IS NOT NULL
      )
    THEN
      address_cor := json_build_object(
        'preview', 'address(unsynched)',
        'reference', null,
        'type', 'address',
        'instance', json_build_object(
          'bag_id', (subject->'correspondentie_bag_id')::bigint,
          'city', subject->'correspondentie_woonplaats',
          'street', subject->'correspondentie_straatnaam',
          'street_number', (subject->'correspondentie_huisnummer')::int,
          'street_number_suffix', subject->'correspondentie_huisnummertoevoeging',
          'street_number_letter', subject->'correspondentie_huisletter',
          'foreign_address_line1', subject->'correspondentie_adres_buitenland1',
          'foreign_address_line2', subject->'correspondentie_adres_buitenland2',
          'foreign_address_line3', subject->'correspondentie_adres_buitenland3',
          'zipcode', subject->'correspondentie_postcode',
          'country', (subject->'correspondentie_country')::jsonb,
          'latitude', null,
          'longitude', null,
          -- non-existent data
          'municipality', null,
          'date_created', timestamp_to_perl_datetime(NOW()),
          'date_modified', timestamp_to_perl_datetime(NOW())
        )
      );
    END IF;

    IF subject->'main_activity' = '{}'
    THEN
      SELECT INTO subject subject || hstore('main_activity', null);
    END IF;

    IF subject->'secondairy_activities' = '[]'
    THEN
      SELECT INTO subject subject || hstore('secondairy_activities', null);
    END IF;

    SELECT INTO json json || jsonb_build_object(
      'instance', json_build_object(
        'address_residence', address_res,
        'address_correspondence', address_cor,
        'email_address', subject->'email',
        'phone_number', subject->'phone_number',
        'mobile_phone_number', subject->'mobile',
        'coc_number', lpad(subject->'dossiernummer', 8, '0'),
        'coc_location_number', lpad(subject->'vestigingsnummer', 12, '0'),
        'date_ceased', (subject->'date_ceased')::date,
        'date_founded', (subject->'date_founded')::date,
        'date_registration', (subject->'date_registration')::date,
        'main_activity', (subject->'main_activity')::jsonb,
        'secondairy_activities', (subject->'secondairy_activities')::jsonb,
        'company_type', (subject->'company_type')::jsonb,
        'oin', (subject->'oin')::bigint,
        'rsin', (subject->'rsin')::bigint,
        'company', (subject->'handelsnaam')::text,
        -- non-existent data
        'date_created', timestamp_to_perl_datetime(NOW()),
        'date_modified', timestamp_to_perl_datetime(NOW())
      )
    );

    RETURN;
  END;
  $$;
  CREATE OR REPLACE FUNCTION case_subject_json(
    IN zb hstore,
    OUT json JSONB
  )
  LANGUAGE plpgsql
  AS $$
  DECLARE
    btype text;
    gm_id int;
    old_id text;

    subject record;
    s_hstore hstore;
    subject_json json;

    subject_uuid uuid;
    subject_type text;
    display_name text;

  BEGIN

    btype := zb->'betrokkene_type';

    IF btype IS NULL THEN
      json := null;
      RETURN;
    END IF;

    IF btype NOT IN ('medewerker', 'natuurlijk_persoon', 'bedrijf') THEN
      RAISE EXCEPTION 'Unknown betrokkene type %', btype;
    END IF;


    gm_id := (zb->'gegevens_magazijn_id')::int;
    old_id := 'betrokkene-' || btype || '-' || gm_id;

    IF btype = 'medewerker' THEN
      subject_type := 'employee';

      SELECT INTO subject * FROM subject s WHERE s.subject_type = 'employee'
        AND id = gm_id;

      s_hstore := hstore(subject);
      SELECT INTO subject_json subject_employee_json(s_hstore);
      SELECT INTO display_name get_display_name_for_employee(s_hstore);

    ELSIF btype = 'natuurlijk_persoon' THEN

      subject_type := 'person';

      SELECT INTO
        subject
        np.*,
        a.straatnaam as street,
        a.huisnummer as street_number,
        a.huisletter as street_number_letter,
        a.huisnummertoevoeging as street_number_suffix,
        a.postcode as zipcode,
        a.woonplaats as city,
        a.functie_adres as address_type,
        mc.json as municipality,
        cc.json as country,
        a.adres_buitenland1 as foreign_address_line_1,
        a.adres_buitenland2 as foreign_address_line_2,
        a.adres_buitenland3 as foreign_address_line_3,
        a.bag_id as bag_id,
        a.geo_lat_long[0] as latitude,
        a.geo_lat_long[1] as longitude,
        cd.mobiel as mobile,
        cd.telefoonnummer as phone_number,
        cd.email as email_address
      FROM
        natuurlijk_persoon np
      LEFT JOIN
        adres a
      ON
        np.adres_id = a.id
      LEFT JOIN
        country_code_v1_view cc
      ON
        cc.dutch_code = a.landcode
      LEFT JOIN
        municipality_code_v1_view mc
      ON
        mc.dutch_code = a.gemeente_code
      LEFT JOIN
        contact_data cd
      ON
        -- 1 or 2 is much clearer than natuurlijk_persoon or bedrijf ey
        -- :/
        (cd.gegevens_magazijn_id = np.id and cd.betrokkene_type = 1)

      WHERE
        np.id = gm_id;

      s_hstore := hstore(subject);

      SELECT INTO subject_json subject_person_json(s_hstore);
      SELECT INTO display_name get_display_name_for_person(s_hstore);

    ELSIF btype = 'bedrijf' THEN

      subject_type := 'company';

      SELECT INTO subject
      b.*,
      cc_vestiging.json as vestiging_country,
      cc_correspondentie.json as correspondentie_country,
      let.json as company_type
      FROM
        bedrijf b
      LEFT JOIN
        country_code_v1_view cc_vestiging
      ON
        cc_vestiging.dutch_code = b.vestiging_landcode
      LEFT JOIN
        country_code_v1_view cc_correspondentie
      ON
        cc_correspondentie.dutch_code = b.correspondentie_landcode
      LEFT JOIN
        legal_entity_v1_view let
      ON
        let.code = b.rechtsvorm
      WHERE
        b.id = gm_id
      ;

      s_hstore := hstore(subject);

      SELECT INTO subject_json subject_company_json(s_hstore);
      SELECT INTO display_name get_display_name_for_company(s_hstore);

    END IF;

    SELECT INTO json json_build_object(
        'preview', display_name,
        'type', 'subject',
        'reference', subject.uuid,
        'instance', json_build_object(
          'date_created', timestamp_to_perl_datetime(NOW()),
          'date_modified', timestamp_to_perl_datetime(NOW()),
          'display_name', display_name,
          'external_subscription', null,
          'old_subject_identifier', old_id,
          'subject_type', subject_type,
          'subject', subject_json
        )
    );

  END;
  $$;

  CREATE OR REPLACE FUNCTION org_unit(
    IN org hstore,
    IN type TEXT,
    OUT org_json JSONB
  )
  LANGUAGE plpgsql
  AS $$
  DECLARE
    org_instance jsonb;
    org_id text;
  BEGIN

    IF type = 'role'
    THEN
      org_id := 'role_id';
    ELSE
      org_id := 'group_id';
    END IF;


      SELECT INTO org_instance jsonb_build_object(
        'name', org->'name',
        'description', org->'description',
        'date_modified', timestamp_to_perl_datetime((org->'date_modified')::timestamp with time zone),
        'date_created', timestamp_to_perl_datetime((org->'date_created')::timestamp with time zone)
      );

      IF type = 'role' THEN
        SELECT INTO org_instance org_instance || jsonb_build_object(
          'system_role', (org->'system_role')::boolean
        );
        SELECT INTO org_instance org_instance || jsonb_build_object(
          'role_id', (org->'id')::int
        );
      ELSE
        SELECT INTO org_instance org_instance || jsonb_build_object(
          'group_id', (org->'id')::int
        );
      END IF;

      SELECT INTO org_json json_build_object(
        'reference', org->'uuid',
        'preview', org->'name',
        'type', type,
        'instance', org_instance
      );

      RETURN;
  END;
  $$;

  update groups set id = id;
  update roles set id = id;

  CREATE OR REPLACE FUNCTION update_subject_json() RETURNS TRIGGER LANGUAGE 'plpgsql' AS $$

    BEGIN

      SELECT INTO
        NEW.requestor_v1_json
        case_subject_json(hstore(zb))
      FROM
        zaak_betrokkenen zb
      WHERE
        zb.id = NEW.aanvrager;

      IF NEW.behandelaar IS NOT NULL THEN
        SELECT INTO
          NEW.assignee_v1_json
          case_subject_json(hstore(zb))
        FROM
          zaak_betrokkenen zb
        WHERE
          zb.id = NEW.behandelaar;

      END IF;

      IF NEW.coordinator IS NOT NULL THEN
        SELECT INTO
          NEW.coordinator_v1_json
          case_subject_json(hstore(zb))
        FROM
          zaak_betrokkenen zb
        WHERE
          zb.id = NEW.coordinator AND zb.zaak_id = NEW.id;
      END IF;
      RETURN NEW;

    END;
  $$;

  update zaak set uuid = uuid;

  CREATE OR REPLACE FUNCTION update_subject_json() RETURNS TRIGGER LANGUAGE 'plpgsql' AS $$

    BEGIN

      IF TG_OP = 'INSERT'
      THEN
        SELECT INTO
          NEW.requestor_v1_json
          case_subject_json(hstore(zb))
        FROM
          zaak_betrokkenen zb
        WHERE
          -- zb does not have the zaak id yet on create..
          zb.id = NEW.aanvrager;

      ELSIF TG_OP = 'UPDATE' AND (NEW.requestor_v1_json IS NULL OR NEW.aanvrager != OLD.aanvrager)
      THEN
        SELECT INTO
          NEW.requestor_v1_json
          case_subject_json(hstore(zb))
        FROM
          zaak_betrokkenen zb
        WHERE
          -- zb does not have the zaak id yet on update, this is handled
          -- later on..
           zb.id = NEW.aanvrager;

      END IF;

      IF NEW.behandelaar IS NOT NULL
      THEN
        IF TG_OP = 'INSERT' OR (TG_OP = 'UPDATE' AND ( OLD.behandelaar IS NULL OR NEW.behandelaar != OLD.behandelaar))
        THEN
          SELECT INTO
            NEW.assignee_v1_json
            case_subject_json(hstore(zb))
          FROM
            zaak_betrokkenen zb
          WHERE
            zb.id = NEW.behandelaar AND zb.zaak_id = NEW.id;
        END IF;
      ELSE
        NEW.assignee_v1_json := NULL;
      END IF;


      IF NEW.coordinator IS NOT NULL
      THEN
        IF TG_OP = 'INSERT' OR (TG_OP = 'UPDATE' AND ( OLD.coordinator IS NULL OR NEW.coordinator != OLD.coordinator))
        THEN
          SELECT INTO
            NEW.coordinator_v1_json
            case_subject_json(hstore(zb))
          FROM
            zaak_betrokkenen zb
          WHERE
            zb.id = NEW.coordinator AND zb.zaak_id = NEW.id;
        END IF;
      ELSE
        NEW.coordinator_v1_json := NULL;
      END IF;

      RETURN NEW;

    END;

  $$;

  DROP VIEW IF EXISTS case_v1;
  CREATE VIEW case_v1 AS
  SELECT

    z.id AS number,
    z.uuid AS id,
    z.pid AS number_parent,
    z.number_master AS number_master,
    z.vervolg_van AS number_previous,

    z.onderwerp AS subject,
    z.onderwerp_extern AS subject_external,

    z.status AS status,

    z.created AS date_created,
    z.last_modified AS date_modified,
    z.vernietigingsdatum AS date_destruction,
    z.afhandeldatum AS date_of_completion,
    z.registratiedatum AS date_of_registration,
    z.streefafhandeldatum AS date_target,

    z.html_email_template AS html_email_template,

    z.payment_status AS payment_status,
    z.payment_amount AS price,

    z.contactkanaal AS channel_of_contact,
    z.archival_state AS archival_state,

    get_confidential_mapping(z.confidentiality) as confidentiality,

    CASE WHEN z.status = 'stalled' THEN
      zm.stalled_since
    ELSE
      NULL
    END AS stalled_since,

    CASE WHEN z.status = 'stalled' THEN
      z.stalled_until
    ELSE
      NULL
    END AS stalled_until,

    zm.current_deadline AS current_deadline,
    zm.deadline_timeline AS deadline_timeline,

    (
      SELECT
        COALESCE(
          jsonb_object_agg(ca.magic_string, ca.value::jsonb)
            FILTER (where ca.magic_string is not null),
          '{}'::jsonb
      )
    FROM case_attributes_v1 ca where ca.case_id = z.id) AS attributes,

    ztr.id AS result_id,
    ztr.resultaat as result,
    ztr.selectielijst as active_selection_list,

    CASE WHEN (ztr.id IS NOT NULL) THEN
      json_build_object(
        'reference', NULL,
        'type', 'case/result',
        'preview', CASE WHEN ztr.label IS NOT NULL THEN
          ztr.label
        ELSE
          ztr.resultaat
        END,
        'instance', json_build_object(
          'date_created', timestamp_to_perl_datetime(ztr.created),
          'date_modified', timestamp_to_perl_datetime(ztr.last_modified),
          'archival_type', ztr.archiefnominatie,
          'dossier_type', ztr.dossiertype,
          'name', CASE WHEN ztr.label IS NOT NULL THEN
            ztr.label
          ELSE
            ztr.resultaat
          END,
          'result', ztr.resultaat,
          'retention_period', ztr.bewaartermijn,
          'selection_list', CASE WHEN ztr.selectielijst = '' THEN
                              NULL
                            ELSE
                              ztr.selectielijst
                            END,
          'selection_list_start', ztr.selectielijst_brondatum,
          'selection_list_end', ztr.selectielijst_einddatum
        )
      )
    ELSE
      NULL
    END AS outcome,

    json_build_object(
      'preview', ct_ref.title,
      'reference', ct_ref.uuid,
      'instance', json_build_object(
        'version', ct_ref.version,
        'name', ct_ref.title
      ),
      'type', 'casetype'
    ) AS casetype,

    json_build_object(
      'preview', gr.name || ', ' || role.name,
      'reference', NULL,
      'type', 'case/route',
      'instance', json_build_object(
        'date_created', timestamp_to_perl_datetime(NOW()),
        'date_modified', timestamp_to_perl_datetime(NOW()),
        'group', gr.v1_json,
        'role', role.v1_json
      )
    ) AS route,

    CASE WHEN z.status = 'stalled' THEN
      zm.opschorten
    ELSE
      NULL
    END AS suspension_rationale,

    CASE WHEN z.status = 'resolved' THEN
      zm.afhandeling
    ELSE
      NULL
    END AS premature_completion_rationale,

    zts.fase::text AS phase,

    (
      SELECT
    json_build_object(
      'preview', zts.fase,
      'reference', null,
      'type', 'case/milestone',
      'instance', json_build_object(
        'date_created', timestamp_to_perl_datetime(NOW()),
        'date_modified', timestamp_to_perl_datetime(NOW()),
          'phase_label',
            CASE WHEN zts.id IS NOT NULL THEN
              zts.fase
            ELSE
              zts_end.fase
            END,
          'phase_sequence_number',
            CASE WHEN zts.id IS NOT NULL THEN
              zts.status
            ELSE
              zts_end.status
            END,

        'milestone_label', zts_previous.naam,
        'milestone_sequence_number', zts_previous.status,
        'last_sequence_number', zts_end.status
      )
    ) FROM casetype_end_status zts_end where z.zaaktype_node_id = zts_end.zaaktype_node_id )
    as milestone,

    json_build_object(
      'type', 'set',
      'instance', json_build_object(
        'rows', COALESCE(crp.relationship, '[]'::jsonb)
      )
    ) AS relations,

    json_build_object(
      'parent', z.pid,
      'continuation', json_build_object(
        'type', 'set',
        'instance', json_build_object(
          'rows', COALESCE(continuation.relationship, '[]'::jsonb)
        )
      ),
      'child', json_build_object(
        'type', 'set',
        'instance', json_build_object(
          'rows', COALESCE(children.relationship, '[]'::jsonb)
        )
      ),
      'plain', json_build_object(
        'type', 'set',
        'instance', json_build_object(
          'rows', COALESCE(crp.relationship, '[]'::jsonb)
        )
      )
    ) AS case_relationships,

    z.requestor_v1_json AS requestor,
    z.assignee_v1_json AS assignee,
    z.coordinator_v1_json AS coordinator,

    -- static values
    'Dossier' AS aggregation_scope,

    -- Not available via api/v1
    null AS case_location,
    null AS correspondence_location

  FROM zaak z

  LEFT JOIN zaak_meta zm
  ON zm.zaak_id = z.id

  LEFT JOIN casetype_v1_reference ct_ref
  ON z.zaaktype_node_id = ct_ref.casetype_node_id

  LEFT JOIN zaaktype_resultaten ztr
  ON z.resultaat_id = ztr.id

  LEFT JOIN groups gr
  ON (z.route_ou = gr.id)

  LEFT JOIN roles role
  ON (z.route_role = role.id)

  LEFT JOIN zaaktype_status zts
  ON ( z.zaaktype_node_id = zts.zaaktype_node_id AND zts.status = z.milestone + 1)

  LEFT JOIN zaaktype_status zts_previous
  ON ( z.zaaktype_node_id = zts_previous.zaaktype_node_id AND zts_previous.status = z.milestone)

  LEFT JOIN case_relationship_json_view crp
  ON ( z.id = crp.case_id and crp.type = 'plain')

  LEFT JOIN case_relationship_json_view continuation
  ON ( z.id = continuation.case_id and continuation.type = 'initiator')

  LEFT JOIN case_relationship_json_view children
  ON ( z.id = children.case_id and children.type = 'parent')

  WHERE z.deleted IS NULL

  ;

  DELETE FROM file_case_document WHERE file_id IN (SELECT id FROM file WHERE active_version = FALSE);

COMMIT;
