BEGIN;
  INSERT INTO queue (type, label, priority, metadata, data)
  VALUES (
    'fix_gm_natuurlijk_personen',
    'devops: update natuurlijk personen for gm',
    800,
    -- metadata
    json_build_object(
      'require_object_model', 1,
      'disable_acl', 1,
      'target', 'backend'
    ),
    -- data
    json_build_object(
      'null', null
    )
  );

COMMIT;
