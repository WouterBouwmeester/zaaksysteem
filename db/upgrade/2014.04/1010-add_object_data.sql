BEGIN;

-- object_data, containing all the object data
-- Features:
-- + Hstore: for fast search filtering
-- + object_id: for constraint bound creating serials for objects which
--   need an id (like case)

DROP TABLE IF EXISTS object_data;

CREATE TABLE object_data (
    uuid                            UUID UNIQUE PRIMARY KEY DEFAULT uuid_generate_v4(),
    object_id                       INTEGER,
    object_class                    TEXT NOT NULL CHECK(
        (object_class)::TEXT ~ '^case$'::TEXT
    ),
    properties                      TEXT NOT NULL DEFAULT '{}',
    index_hstore                    HSTORE,
    date_created                    timestamp without time zone,
    date_modified                   timestamp without time zone,
    UNIQUE(object_class, object_id)
);

-- select properties me,array(
--     select distinct properties from object_data, object_relationships where
--       (
--         object_relationships.object1_uuid = me.uuid OR object_relationships.object2_uuid = me.uuid
--       ) AND 
--       (
--         object_data.uuid = object_relationships.object1_uuid OR
--         object_data.uuid = object_relationships.object2_uuid 
--       ) AND object_data.uuid != me.uuid
--   ) as related_objects from object_data me;

DROP TABLE IF EXISTS object_relationships;

CREATE TABLE object_relationships (
    uuid                            UUID UNIQUE PRIMARY KEY DEFAULT uuid_generate_v4(),
    object1_uuid                    UUID NOT NULL,
    object2_uuid                    UUID NOT NULL
);

COMMIT;