BEGIN;

    ALTER TABLE service_geojson_relationship ADD uuid UUID NOT NULL UNIQUE;

COMMIT;

