BEGIN;

UPDATE user_entity SET password = NULL where password IS NOT NULL AND source_interface_id IN (
    SELECT id FROM interface WHERE module = 'samlidp'
);

COMMIT;
