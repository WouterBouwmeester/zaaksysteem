
BEGIN;

  INSERT INTO bibliotheek_notificaties (object_type, label, subject, sender_address, message, search_term, search_order)
  VALUES
  (
    'bibliotheek_notificaties', 'Exportnotificatie-mail', 'Exportbestand is klaar om gedownload te worden', 'noreply@zaaksysteem.nl','Uw exportbestand is te downloaden via [[token_uri]].', 'Uw exportbestand is te downloaden via [[token_uri]].', 'Uw exportbestand is te downloaden via [[token_uri]].');

  DROP TRIGGER IF EXISTS file_update_timestamp_trigger ON bibliotheek_notificaties;

COMMIT;
