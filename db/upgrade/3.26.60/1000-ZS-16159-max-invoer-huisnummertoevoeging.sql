BEGIN;

    ALTER TABLE adres ALTER COLUMN huisnummertoevoeging TYPE TEXT;
    ALTER TABLE gm_adres ALTER COLUMN huisnummertoevoeging TYPE TEXT;

    ALTER TABLE adres ALTER COLUMN huisnummer TYPE BIGINT;
    ALTER TABLE gm_adres ALTER COLUMN huisnummer TYPE BIGINT;

    -- Make all the huisnummertoevoegingen the same for consistency
    -- reasons

    ALTER TABLE bag_nummeraanduiding ALTER COLUMN huisnummer TYPE BIGINT;
    ALTER TABLE bag_nummeraanduiding ALTER COLUMN huisnummertoevoeging TYPE TEXT;

    ALTER TABLE bedrijf ALTER COLUMN correspondentie_huisnummer TYPE BIGINT using correspondentie_huisnummer::bigint;
    ALTER TABLE gm_bedrijf ALTER COLUMN correspondentie_huisnummer TYPE BIGINT using correspondentie_huisnummer::bigint;

    ALTER TABLE bedrijf ALTER COLUMN correspondentie_huisnummertoevoeging TYPE TEXT;
    ALTER TABLE gm_bedrijf ALTER COLUMN correspondentie_huisnummertoevoeging TYPE TEXT;

    ALTER TABLE bedrijf ALTER COLUMN vestiging_huisnummer TYPE BIGINT using vestiging_huisnummer::bigint;
    ALTER TABLE gm_bedrijf ALTER COLUMN vestiging_huisnummer TYPE BIGINT using vestiging_huisnummer::bigint;

    ALTER TABLE bedrijf ALTER COLUMN vestiging_huisnummertoevoeging TYPE TEXT;
    ALTER TABLE gm_bedrijf ALTER COLUMN vestiging_huisnummertoevoeging TYPE TEXT;

COMMIT;
