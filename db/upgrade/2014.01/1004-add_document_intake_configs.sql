BEGIN;

INSERT INTO config (parameter, value, advanced) SELECT 'file_username_seperator', '-', 't'
    WHERE NOT EXISTS (
        SELECT id FROM config WHERE parameter = 'file_username_seperator'
    )
;
INSERT INTO config (parameter, value, advanced) SELECT 'feedback_email_template_id', NULL, 't'
    WHERE NOT EXISTS (
        SELECT id FROM config WHERE parameter = 'feedback_email_template_id'
    )
;

COMMIT;
