BEGIN;

  DROP VIEW IF EXISTS case_documents CASCADE;

  CREATE VIEW case_documents AS
  SELECT
      z.id AS case_id,
      array_agg(fs.uuid)::text[] as value,
      bk.magic_string as magic_string,
      bk.id as library_id
    FROM
      zaak z
    JOIN
      zaaktype_kenmerken ztk
    ON
      (z.zaaktype_node_id = ztk.zaaktype_node_id)
    JOIN
      bibliotheek_kenmerken bk
    ON (ztk.bibliotheek_kenmerken_id = bk.id and bk.value_type = 'file')
    LEFT JOIN file_case_document fcd
      JOIN file f
        JOIN filestore fs ON f.filestore_id = fs.id
          ON fcd.file_id = f.id
           ON (z.id = fcd.case_id and bk.id = fcd.bibliotheek_kenmerken_id)
    GROUP BY 1, 3, 4
  ;

  DROP VIEW IF EXISTS case_attributes CASCADE;

  CREATE VIEW case_attributes AS
  SELECT
    z.id AS case_id,
    zk.value as value,
    zk.magic_string as magic_string,
    zk.bibliotheek_kenmerken_id as library_id
  FROM zaak z
  JOIN
    zaak_kenmerk zk
  ON
    z.id = zk.zaak_id
  JOIN
    zaaktype_kenmerken ztk
  ON
    (ztk.bibliotheek_kenmerken_id = zk.bibliotheek_kenmerken_id and z.zaaktype_node_id = ztk.zaaktype_node_id)
  UNION ALL
  SELECT * FROM case_documents;

  DROP VIEW IF EXISTS case_v1;

  CREATE VIEW case_v1 AS
  SELECT
    z.id AS number,
    z.uuid AS id,
    z.pid AS number_parent,
    z.number_master AS number_master,
    z.vervolg_van AS number_previous,

    z.onderwerp AS subject,
    z.onderwerp_extern AS subject_external,

    z.status AS status,

    z.created AS date_created,
    z.last_modified AS date_modified,
    z.vernietigingsdatum AS date_destruction,
    z.afhandeldatum AS date_of_completion,
    z.registratiedatum AS date_of_registration,
    z.streefafhandeldatum AS date_target,

    z.payment_status AS payment_status,
    z.payment_amount AS price,

    z.contactkanaal AS channel_of_contact,
    z.stalled_until AS stalled_until,
    z.archival_state AS archival_state,
    zm.stalled_since AS stalled_since,
    zm.current_deadline AS current_deadline,
    zm.deadline_timeline AS deadline_timeline,

    -- complex queries here
    null AS relations,
    null AS case_relationships,

    jsonb_object_agg(ca.magic_string, ca.value) AS attributes,

    -- Look into logging, but it is also in zaak meta
    -- Python, what are you using?
    null AS suspension_rationale,
    -- investigate..
    null AS premature_completion_rationale,

    -- if result unset null
    -- else zaaktype result join
    null AS result,
    null AS result_id,
    null AS active_selection_list,
    -- if result
    -- json blob
    null AS case_outcome,

    null AS casetype,

    -- Subject API/v1 shit, mind boggling
    null AS requestor,
    null AS assignee,
    null AS coordinator,

    -- routing info
    null AS group,
    null AS role,

    -- phase can volgende fase, show the next phase number
    null AS phase,

    -- milestones, similar to phase
    -- json blob
    null AS milestone,

    -- static values
    'Dossier' AS aggregation_scope,

    -- Not available via api/v1
    null AS case_location,
    null AS correspondence_location

  FROM zaak z
  JOIN zaak_meta zm
  ON zm.zaak_id = z.id
  JOIN case_attributes ca
  ON  ca.case_id = z.id
  GROUP BY z.id, zm.stalled_since, zm.current_deadline, zm.deadline_timeline
  ;

COMMIT;

/*

BEGIN;

  DROP VIEW IF EXISTS case_martijn;
  CREATE VIEW case_martijn AS
  SELECT
    z.id,
    z.uuid,
    z.streefafhandeldatum,
    z.registratiedatum,
    z.afhandeldatum,
    z.vernietigingsdatum,
    z.created,
    z.last_modified,
    z.onderwerp,
    z.onderwerp_extern,
    CASE WHEN (route_role IS NOT NULL
        AND route_ou IS NOT NULL) THEN
        json_build_object('group_uuid', g.uuid, 'group_name', g.name, 'role_uuid', r.uuid, 'role_name', r.name)
    ELSE
        NULL
    END AS "route",
    json_build_object('status', COALESCE(z.payment_status, 'unknown'), 'amount', z.payment_amount) AS payment,
    CASE WHEN (zr.id IS NOT NULL) THEN
        json_build_object('label', zr.label, 'result', zr.resultaat)
    ELSE
        NULL
    END AS resultaat,
    (
        SELECT
            json_agg(json_build_object('uuid', f.uuid, 'store_uuid', fs.uuid, 'name', f.name, 'extension', f.extension))
        FROM
            "file" f
            JOIN filestore fs ON (f.filestore_id = fs.id)
        WHERE
            f.case_id = z.id) AS documents,
    (
        SELECT
            coalesce(json_object_agg(bk.magic_string, json_build_object('value', CASE WHEN bk.value_type IN ('appointment', 'geojson', 'address_v2', 'relationship') THEN
                            array_to_json(zk.value::json[])
                        ELSE
                            array_to_json(zk.value)
                        END, 'type', bk.value_type, 'type_uuid', bk.uuid)), '{}'::json)
        FROM
            zaak_kenmerk zk
            JOIN bibliotheek_kenmerken bk ON (zk.bibliotheek_kenmerken_id = bk.id)
        WHERE
            zk.zaak_id = z.id) AS custom_fields,
    (
        SELECT
            json_agg(json_build_object('automatic', coalesce(ca.automatic, FALSE), 'phase', zs.status, 'type', ca.type, 'label', ca.label))
        FROM
            case_action ca
            JOIN zaaktype_status zs ON zs.id = ca.casetype_status_id
        WHERE
            ca.case_id = z.id) AS case_actions
FROM
    zaak z
    LEFT JOIN zaaktype_resultaten zr ON (z.resultaat_id = zr.id)
    LEFT JOIN "roles" r ON (z.route_role = r.id)
    LEFT JOIN "groups" g ON (z.route_ou = g.id)
    ;


COMMIT;
*/
