BEGIN;

ALTER TABLE queue ALTER COLUMN date_created SET DEFAULT statement_timestamp();
ALTER TABLE queue ADD COLUMN parent_id UUID REFERENCES queue(id);

COMMIT;
