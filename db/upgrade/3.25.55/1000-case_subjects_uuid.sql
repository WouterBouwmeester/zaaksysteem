BEGIN;

ALTER TABLE zaak_betrokkenen ADD COLUMN subject_id UUID;

UPDATE zaak_betrokkenen SET subject_id = (
    SELECT uuid FROM natuurlijk_persoon WHERE id = zaak_betrokkenen.gegevens_magazijn_id
) WHERE betrokkene_type = 'natuurlijk_persoon';

UPDATE zaak_betrokkenen SET subject_id = (
    SELECT uuid FROM subject WHERE id = zaak_betrokkenen.gegevens_magazijn_id
) WHERE betrokkene_type = 'medewerker';

UPDATE zaak_betrokkenen SET subject_id = (
    SELECT uuid FROM bedrijf WHERE id = zaak_betrokkenen.gegevens_magazijn_id
) WHERE betrokkene_type = 'bedrijf';

ALTER TABLE zaak_betrokkenen ALTER COLUMN subject_id SET NOT NULL;

COMMIT;
