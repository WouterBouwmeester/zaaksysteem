
BEGIN;

CREATE TABLE legal_entity_type (
  id SERIAL,
  code int NOT NULL UNIQUE,
  label TEXT NOT NULL,
  uuid UUID NOT NULL DEFAULT uuid_generate_v4(),
  active BOOLEAN DEFAULT TRUE NOT NULL
);


INSERT INTO legal_entity_type (
  code,
  label,
  uuid,
  active
)
VALUES
(1, 'Eenmanszaak', '645cf805-6ef9-42e4-bf32-ede66aecc885', true),
(2, 'Eenmanszaak met meer dan één eigenaar', '1873346f-927c-4d23-9322-186e8aea758a', false),
(3, 'N.V./B.V. in oprichting op A-formulier', '9f0d0a7d-6ece-4a54-823e-8b5b93e048b6', false),
(5, 'Rederij', 'ce7f249e-d996-4987-b660-b66067af0c7d', false),
(7, 'Maatschap', '8171d3a2-0576-459f-b940-88e5a0d875d1', true),
(11, 'Vennootschap onder firma', '284c6cef-b3d5-47e9-986f-23476d732825', true),
(12, 'N.V/B.V. in oprichting op B-formulier', '0053b5a3-f2da-43c8-a736-125a5cad5a62', false),
(21, 'Commanditaire vennootschap met een beherend vennoot', 'c0e2868d-ba56-4d4f-bbbe-4bf53120dd43', true),
(22, 'Commanditaire vennootschap met meer dan één beherende vennoot', '5d5bbe2c-c74b-4427-9f55-62470737e7f4', false),
(23, 'N.V./B.V. in oprichting op D-formulier', '62fc1e5f-f04b-4bcd-a4db-0dd8a298a5d0', false),
(40, 'Rechtspersoon in oprichting', '4344ce11-6cb7-433e-9eb1-8526ff9a4a55', false),
(41, 'Besloten vennootschap met gewone structuur', '6ed6a0de-bbb9-432a-8e03-4f3294f6e10a', true),
(42, 'Besloten vennootschap blijkens statuten structuurvennootschap', '4ffaf90c-db27-42c2-9e4a-400424bad6e5', false),
(51, 'Naamloze vennootschap met gewone structuur', '5db79ef3-651a-4fd0-9211-ae7f4bb39ce7', true),
(52, 'Naamloze vennootschap blijkens statuten structuurvennootschap', '0973fbd2-ed83-4dff-9d34-51f745b391c5', false),
(53, 'Naamloze vennootschap beleggingsmaatschappij met veranderlijk kapitaal', '1ede5f01-6d76-49aa-ae26-75f2f4b20ce0', false),
(54, 'Naamloze vennootschap beleggingsmaatschappij met veranderlijk kapitaal blijkens statuten structuurvennootschap', 'e7fe016f-37cc-4e34-947e-ebfd49877952', false),
(55, 'Europese naamloze vennootschap (SE) met gewone structuur', '912835bf-9f98-40d2-aba1-b8866e2b98ee', true),
(56, 'Europese naamloze vennootschap (SE) blijkens statuten structuurvennootschap', '230077a3-b0a9-445f-885f-5890c3bfe133', false),
(61, 'Coöperatie U.A. met gewone structuur', '686e0b53-91e7-4a5b-af08-5c512de94ba9', false),
(62, 'Coöperatie U.A. blijkens statuten structuurcoöperatie', 'f24d328e-1122-434f-b40b-dc4f5fe38c82', false),
(63, 'Coöperatie W.A. met gewone structuur', 'e04e5ee9-d712-43c7-8e3a-1bd8c84fa39d', false),
(64, 'Coöperatie W.A. blijkens statuten structuurcoöperatie', '5bbc2951-5f0e-4203-8be6-259912272ead', false),
(65, 'Coöperatie B.A. met gewone structuur', '27662752-6f20-4cf1-b081-662def3503c6', false),
(66, 'Coöperatie B.A. blijkens statuten structuurcoöperatie', 'bf276571-9380-4ad2-ab52-14b84ff133c6', false),
(70, 'Vereniging van eigenaars', 'edc1477f-2fc5-4e1f-bb61-233b9823509a', true),
(71, 'Vereniging met volledige rechtsbevoegdheid', '3918d3ab-e11d-43ff-ae1c-51f5f523338f', false),
(72, 'Vereniging met beperkte rechtsbevoegdheid', 'fc7f7654-b23c-4ecd-b719-39d80f6c98c9', false),
(73, 'Kerkgenootschap', '759b76bf-bceb-49d0-948b-4bbe17992326', true),
(74, 'Stichting', 'c91215d9-14b5-43e0-ba2b-1b8487e2b889', true),
(81, 'Onderlinge waarborgmaatschappij U.A. met gewone structuur', 'fc01a26d-1ec9-419a-b05d-c3c0a53dcab6', false),
(82, 'Onderlinge waarborgmaatschappij U.A. blijkens statuten structuuronderlinge', '3c718f04-77e3-4ec7-87fc-6a896c16c8a4', false),
(83, 'Onderlinge waarborgmaatschappij W.A. met gewone structuur', '649dba84-8c72-4a94-aa91-617db9dab51c', false),
(84, 'Onderlinge waarborgmaatschappij W.A. blijkens statuten structuuronderlinge', '755f456d-a94f-44bb-bfe8-47277ee360ab', false),
(85, 'Onderlinge waarborgmaatschappij B.A. met gewone structuur', '71580ddf-7ba8-4ee1-a383-fc20d562ed20', false),
(86, 'Onderlinge waarborgmaatschappij B.A. blijkens statuten structuuronderlinge', '38fc7147-724a-4c30-9d3b-204aa99b859f', false),
(88, 'Publiekrechtelijke rechtspersoon', '423a8ddf-81d3-4499-a0f1-33fad764839c', true),
(89, 'Privaatrechtelijke rechtspersoon', '292840a2-4e03-47f8-b00b-d5d1ffe0392a', false),
(91, 'Buitenlandse rechtsvorm met hoofdvestiging in Nederland', '7861a128-8419-4d09-9647-e31f3498faf0', false),
(92, 'Nevenvest. met hoofdvest. in buitenl.', 'c164718f-fd53-43bb-8b99-b403cf5cff43', false),
(93, 'Europees economisch samenwerkingsverband', '0d566451-9f52-40f7-95e6-79033101d13f', false),
(94, 'Buitenl. EG-venn. met onderneming in Nederland', '86641b3f-3e1f-45c7-87dc-9ba24b92c10d', false),
(95, 'Buitenl. EG-venn. met hoofdnederzetting in Nederland', '454fc546-0d05-404a-80f4-8f2c3e2e9355', false),
(96, 'Buitenl. op EG-venn. lijkende venn. met onderneming in Nederland', '47c9ab5c-1900-4e9c-8a2a-26ded7105a82', false),
(97, 'Buitenl. op EG-venn. lijkende venn. met hoofdnederzetting in Nederland', '4c8a1012-8f38-4b41-8583-79f48f98c581', false),
(201, 'Coöperatie', '8a5398b1-4b27-4395-9a52-568dff425aae', true),
(202, 'Vereniging', '4e038ac3-8f8e-4391-b8e6-1f8ffb1a23fe', true);

COMMIT;
