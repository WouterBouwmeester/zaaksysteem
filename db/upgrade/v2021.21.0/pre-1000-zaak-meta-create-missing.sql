
BEGIN;

  INSERT INTO zaak_meta (zaak_id)
    SELECT id FROM zaak WHERE id NOT IN
      (SELECT zaak_id FROM zaak_meta WHERE zaak_id IS NOT NULL);

COMMIT;
