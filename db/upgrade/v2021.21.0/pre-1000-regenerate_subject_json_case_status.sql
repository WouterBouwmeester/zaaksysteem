BEGIN;

  CREATE OR REPLACE FUNCTION update_subject_json() RETURNS TRIGGER LANGUAGE 'plpgsql' AS $$
    DECLARE

      update_requestor boolean;
      update_assignee boolean;
      update_coordinator boolean;

    BEGIN

      update_requestor   := false;
      update_assignee    := false;
      update_coordinator := false;

      IF TG_OP = 'INSERT'
      THEN
        update_requestor := true;

      -- Update subject information on reopening of the case
      ELSIF TG_OP = 'UPDATE' AND (NEW.status IN ('open', 'stalled', 'new') AND OLD.status = 'resolved')
      THEN
        update_requestor   := true;
        update_assignee    := true;
        update_coordinator := true;

      ELSIF TG_OP = 'UPDATE' AND (NEW.requestor_v1_json IS NULL OR NEW.aanvrager != OLD.aanvrager)
      THEN
        update_requestor := true;
      END IF;

      IF NEW.behandelaar IS NOT NULL
      THEN
        IF TG_OP = 'INSERT' OR (TG_OP = 'UPDATE' AND ( NEW.assignee_v1_json IS NULL OR OLD.behandelaar IS NULL OR NEW.behandelaar != OLD.behandelaar))
        THEN
          update_assignee := true;
        END IF;
      ELSE
        NEW.assignee_v1_json := NULL;
      END IF;


      IF NEW.coordinator IS NOT NULL
      THEN
        IF TG_OP = 'INSERT' OR (TG_OP = 'UPDATE' AND ( NEW.coordinator_v1_json IS NULL OR OLD.coordinator IS NULL OR NEW.coordinator != OLD.coordinator))
        THEN
          update_coordinator := true;
        END IF;
      ELSE
        NEW.coordinator_v1_json := NULL;
      END IF;

      IF update_requestor = true
      THEN
        SELECT INTO
          NEW.requestor_v1_json
          case_subject_json(hstore(zb))
        FROM
          zaak_betrokkenen zb
        WHERE
           zb.id = NEW.aanvrager;
      END IF;

      IF update_assignee = true
      THEN
        SELECT INTO
          NEW.assignee_v1_json
          case_subject_json(hstore(zb))
        FROM
          zaak_betrokkenen zb
        WHERE
           zb.id = NEW.behandelaar;
      END IF;

      IF update_coordinator = true
      THEN
        SELECT INTO
          NEW.coordinator_v1_json
          case_subject_json(hstore(zb))
        FROM
          zaak_betrokkenen zb
        WHERE
           zb.id = NEW.coordinator;
      END IF;

      RETURN NEW;

    END;

  $$;


COMMIT;
