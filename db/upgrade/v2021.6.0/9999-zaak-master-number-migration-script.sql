
BEGIN;

    INSERT INTO queue (type, label, priority, metadata, data)
    SELECT
    'update_number_master_for_migration',
    'Release: update master number for case ' || id,
    950,
    -- metadata
    json_build_object(
      'require_object_model', 0,
      'target', 'backend'
    ),
    -- data
    json_build_object(
      'case_number', id
    )
  FROM zaak
  ;
COMMIT;

