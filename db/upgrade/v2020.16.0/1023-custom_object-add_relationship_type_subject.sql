BEGIN;
ALTER TABLE custom_object_relationship
ADD COLUMN related_person_id integer REFERENCES natuurlijk_persoon(id);
ALTER TABLE custom_object_relationship
ADD COLUMN related_organization_id integer REFERENCES bedrijf(id);
ALTER TABLE custom_object_relationship
ADD COLUMN related_employee_id integer REFERENCES subject(id);
ALTER TABLE custom_object_relationship DROP CONSTRAINT IF EXISTS custom_object_relationship_at_least_one_relationship;
ALTER TABLE custom_object_relationship
ADD CONSTRAINT custom_object_relationship_at_least_one_relationship CHECK (
        num_nonnulls(
            related_document_id,
            related_case_id,
            related_custom_object_id,
            related_person_id,
            related_employee_id,
            related_organization_id
        ) = 1
    );
COMMIT;