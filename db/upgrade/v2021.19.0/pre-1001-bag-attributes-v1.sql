BEGIN;

  CREATE OR REPLACE FUNCTION attribute_value_to_jsonb(
    IN value text[],
    IN value_type text,
    OUT value_json jsonb
  )
  LANGUAGE plpgsql
  AS $$
  DECLARE
    length int;
    m text;

  BEGIN

      value_json := '[]'::jsonb;
      SELECT INTO length cardinality(COALESCE(value, '{}'::text[]));

      IF length = 0 and value_type = 'file'
      THEN
        value_json := '[]'::jsonb;
        RETURN;
      ELSIF length = 0
      THEN
        value_json := '[null]'::jsonb;
        RETURN;
      END IF;

      IF value_type = 'date'
      THEN
        SELECT INTO value array_agg(attribute_date_value_to_text(value[1]));
      END IF;

      IF value_type LIKE 'bag_%'
      THEN
        SELECT INTO value_json bag_attribute_value_to_jsonb(value, value_type);
        RETURN;
      END IF;

      IF value_type IN ('geojson', 'address_v2', 'appointment_v2', 'relationship')
      THEN

          FOREACH m IN ARRAY value
          LOOP
            SELECT INTO value_json value_json || jsonb_build_array((CONCAT('[', m::text, ']')::json ->> 0)::json);
          END LOOP;

      ELSIF value_type IN ('checkbox', 'select')
      THEN
        SELECT INTO value_json to_jsonb(ARRAY[value]);
      ELSE
        SELECT INTO value_json to_jsonb(value);
      END IF;
      RETURN;
  END;
  $$;

COMMIT;

