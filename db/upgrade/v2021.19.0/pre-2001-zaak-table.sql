BEGIN;


  CREATE OR REPLACE FUNCTION update_zaak_percentage ()
      RETURNS TRIGGER
      LANGUAGE plpgsql
  AS $$
  BEGIN

    SELECT INTO NEW.status_percentage get_case_status_perc(
      NEW.milestone,
      NEW.zaaktype_node_id
    );
    RETURN NEW;

  END
  $$;
  DROP TRIGGER IF EXISTS update_zaak_percentage ON zaak;

  CREATE TRIGGER update_zaak_percentage
     BEFORE INSERT OR UPDATE
      OF milestone, zaaktype_node_id
     ON zaak
     FOR EACH ROW
     EXECUTE PROCEDURE update_zaak_percentage();

COMMIT;
