BEGIN;
    ALTER TABLE adres
        ADD CONSTRAINT adres_postcode_value CHECK ( postcode ~ '^\d{4}[a-zA-Z]{2}$' );
COMMIT;
