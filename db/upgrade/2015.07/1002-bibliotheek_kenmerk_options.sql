BEGIN;

    ALTER TABLE
        bibliotheek_kenmerken
    ADD COLUMN
        properties TEXT
    DEFAULT '{}';

    ALTER TABLE
        zaaktype_kenmerken
    ADD COLUMN
        properties TEXT
    DEFAULT '{}';

COMMIT;
