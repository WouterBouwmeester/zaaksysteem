BEGIN;

    ALTER TABLE adres ALTER COLUMN woonplaats   TYPE TEXT;
    ALTER TABLE adres ALTER COLUMN straatnaam   TYPE TEXT;
    ALTER TABLE adres ALTER COLUMN gemeentedeel TYPE TEXT;

    ALTER TABLE natuurlijk_persoon ALTER COLUMN geslachtsnaam TYPE TEXT;
    ALTER TABLE natuurlijk_persoon ALTER COLUMN voornamen TYPE TEXT;
    ALTER TABLE natuurlijk_persoon ALTER COLUMN geboorteland TYPE TEXT;
    ALTER TABLE natuurlijk_persoon ALTER COLUMN geboorteplaats TYPE TEXT;
    ALTER TABLE natuurlijk_persoon ALTER COLUMN naam_aanschrijving TYPE TEXT;

COMMIT;
