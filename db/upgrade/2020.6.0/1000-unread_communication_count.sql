BEGIN;

-- Add a new case property, 'num_unread_communication'/unread_communication_count
INSERT INTO case_property (name, namespace, TYPE, value_v0, case_id, object_id) (
  SELECT
    'unread_communication_count',
    'case',
    'null',
    ('[{"name": "case.num_unread_communication", "value": ' || coalesce(sum(t.unread_employee_count), 0) || ', "human_label": "Ongelezen berichten", "human_value": "' || coalesce(sum(t.unread_employee_count), 0) || '", "attribute_type": "integer"}]')::JSONB,
    z.id,
    z.uuid
  FROM
    zaak z
  LEFT JOIN thread t ON t.case_id = z.id
  WHERE z.uuid IS NOT NULL
GROUP BY
  z.id,
  z.uuid)
ON CONFLICT
  DO NOTHING;

COMMIT;

