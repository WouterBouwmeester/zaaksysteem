BEGIN;
CREATE TABLE saved_search (
    "id" integer GENERATED ALWAYS AS IDENTITY,
    "uuid" uuid NOT NULL DEFAULT uuid_generate_v4 (),
    "name" text NOT NULL,
    "kind" text NOT NULL,
    "owner_id" integer NOT NULL REFERENCES subject ("id"),
    "filters" jsonb NOT NULL DEFAULT '{}',
    "permissions" text[] NOT NULL DEFAULT '{}',
    "columns" text[] NOT NULL DEFAULT '{}',
    "sort_column" text NOT NULL,
    "sort_order" text NOT NULL DEFAULT 'asc',
    PRIMARY KEY ("id"),
    UNIQUE ("uuid"),
    UNIQUE ("owner_id", "name"),
    CHECK ("sort_order" IN ('asc', 'desc')),
    CHECK ("kind" IN ('case', 'custom_object'))
);
-- Index on foreign key; makes updates of referenced tables faster
CREATE INDEX saved_search_owner_id_idx ON saved_search ("owner_id");
CREATE INDEX saved_search_permissions_idx ON saved_search USING GIN ("permissions");
CREATE TABLE saved_search_permission (
    "id" integer GENERATED ALWAYS AS IDENTITY,
    "saved_search_id" integer NOT NULL REFERENCES saved_search ("id") ON DELETE CASCADE,
    "group_id" integer NOT NULL REFERENCES GROUPS ("id") ON DELETE CASCADE,
    "role_id" integer NOT NULL REFERENCES roles ("id") ON DELETE CASCADE,
    "permission" text NOT NULL,
    CHECK ("permission" IN ('read', 'write')),
    UNIQUE ("saved_search_id", "group_id", "role_id", "permission")
);
-- Index on foreign key; makes updates of referenced tables faster
CREATE INDEX saved_search_permission_saved_search_idx ON saved_search_permission ("saved_search_id");
CREATE INDEX saved_search_permission_group_idx ON saved_search_permission ("group_id");
CREATE INDEX saved_search_permission_role_idx ON saved_search_permission ("role_id");
CREATE OR REPLACE PROCEDURE saved_search_permission_sync ("search_id" int)
    AS $$
BEGIN
    UPDATE
        saved_search
    SET
        "permissions" = ARRAY (
            SELECT
                CONCAT_WS('|', "group_id"::varchar, "role_id"::varchar, "permission")
            FROM
                saved_search_permission
            WHERE
                "saved_search_id" = "search_id"
            GROUP BY
                "group_id",
                "role_id",
                "permission")
    WHERE
        "id" = "search_id";
END;
$$
LANGUAGE plpgsql;
COMMIT;
