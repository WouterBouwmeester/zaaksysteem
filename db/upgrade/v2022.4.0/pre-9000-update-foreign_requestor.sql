BEGIN;

  UPDATE zaak SET requestor_v1_json = NULL WHERE deleted IS NULL
    AND aanvrager_gm_id IN (
      SELECT natuurlijk_persoon_id from adres where landcode != 6030
    )
  ;

COMMIT;
