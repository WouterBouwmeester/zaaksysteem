
BEGIN;

    UPDATE interface SET interface_config = interface_config::jsonb ||
        '{"binding":"urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST"}'::jsonb
            WHERE module = 'samlidp'
            AND interface_config::jsonb @> '{"saml_type": "eidas"}'
            ;

COMMIT;
