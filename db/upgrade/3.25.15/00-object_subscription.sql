BEGIN;
    
    ALTER TABLE object_subscription ADD config_interface_id INTEGER REFERENCES interface;

    UPDATE object_subscription SET config_interface_id = (
            SELECT id FROM interface
                WHERE module = 'stufconfig' AND active IS TRUE AND date_deleted IS NULL
        )
        WHERE interface_id IN (
            SELECT id FROM interface
                WHERE module ~ '^stuf[a-z]{3}$' AND active IS TRUE AND date_deleted IS NULL
        );

COMMIT;
