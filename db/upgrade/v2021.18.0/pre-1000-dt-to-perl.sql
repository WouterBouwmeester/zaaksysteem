BEGIN;

  CREATE OR REPLACE FUNCTION timestamp_to_perl_datetime(
    IN dt timestamp with time zone,
    OUT tt text
  )
  LANGUAGE plpgsql
  AS $$
  BEGIN
    SELECT INTO tt to_char(dt::timestamp with time zone at time zone 'UTC', 'YYYY-MM-DD"T"HH24:MI:SS"Z"');
  END;
  $$;

COMMIT;
