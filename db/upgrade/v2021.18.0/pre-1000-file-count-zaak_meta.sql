BEGIN;

  ALTER TABLE zaak_meta ADD COLUMN unaccepted_files_count INT NOT NULL DEFAULT 0;

COMMIT;
