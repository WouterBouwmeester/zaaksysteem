BEGIN;

  CREATE OR REPLACE FUNCTION attribute_file_value_to_v0_jsonb(
    IN value text[],
    OUT value_json jsonb
  )
  LANGUAGE plpgsql
  AS $$
  DECLARE
    length int;
    m text;

    r record;

  BEGIN

    value_json := '[]'::jsonb;

    FOR r IN
      SELECT
        f.accepted as accepted,
        f.confidential as confidential,
        CONCAT(f.name, f.extension) as filename,
        f.id as id,
        fs.uuid as uuid,
        fs.size as filesize,
        fs.original_name as original_name,
        fs.mimetype as mimetype,
        fs.md5 as md5,
        fs.is_archivable as is_archivable
      FROM
        filestore fs
      JOIN
        file f
      ON
        (f.filestore_id = fs.id)
      WHERE
        fs.uuid = ANY(value::uuid[])
      ORDER by f.id
    LOOP

      select into value_json value_json || jsonb_build_object(
        'accepted', CASE WHEN r.accepted = true THEN 1 ELSE 0 END,
        'confidential', r.confidential,
        'file_id', r.id,
        'filename', r.filename,
        'is_archivable', CASE WHEN r.is_archivable = true THEN 1 ELSE 0 END,
        'md5', r.md5,
        'mimetype', r.mimetype,
        'original_name', r.original_name,
        'size', r.filesize,
        'thumbnail_uuid', null,
        'uuid', r.uuid
      );
    END LOOP;

    RETURN;


  END;
  $$;

  CREATE OR REPLACE FUNCTION attribute_date_value_to_text(
    IN value text,
    OUT datestamp text
  )
  LANGUAGE plpgsql
  AS $$
  DECLARE
    dd text;
    mm text;
    yy text;
    dt timestamp;
  BEGIN
     dd := split_part(value, '-', 1);
     mm := split_part(value, '-', 2);
     yy := split_part(value, '-', 3);
     dt := make_date(yy::int, mm::int, dd::int);
     SELECT INTO datestamp timestamp_to_perl_datetime(dt::timestamp with time zone at time zone 'Europe/Amsterdam');
  END;
  $$;

  CREATE OR REPLACE FUNCTION attribute_value_to_v0(
    IN value text[],
    IN value_type text,
    IN value_mvp boolean,
    OUT value_json jsonb
  )
  LANGUAGE plpgsql
  AS $$
  DECLARE
    length int;
    m text;

  BEGIN

    value_json := null;
    SELECT INTO length cardinality(COALESCE(value, '{}'::text[]));

    IF length = 0 AND value_type = 'file'
    THEN
      value_json := '[]'::jsonb;
      RETURN;
    ELSIF length = 0
    THEN
      RETURN;
    END IF;

    IF value_type = 'date'
    THEN
      SELECT INTO value_json to_jsonb(attribute_date_value_to_text(value[1]));
      RETURN;
    END IF;


    IF value_type LIKE 'bag_%'
    THEN

      SELECT INTO value_json bag_attribute_value_to_jsonb(value, value_type);
      RETURN;

    END IF;

    IF value_type = 'file'
    THEN

      SELECT INTO value_json attribute_file_value_to_v0_jsonb(value);
      RETURN;

    END IF;


    IF value_type IN ('geojson', 'address_v2', 'appointment_v2', 'relationship')
    THEN

        IF value_mvp = true
        THEN
          value_json := '[]'::jsonb;
          FOREACH m IN ARRAY value
          LOOP
            SELECT INTO value_json value_json || to_jsonb(m::jsonb);
          END LOOP;
        ELSE
          SELECT INTO value_json to_jsonb(value[1]::jsonb);
        END IF;
        RETURN;
    END IF;

    IF value_type IN ('checkbox', 'select')
    THEN
      SELECT INTO value_json to_jsonb(value);
    END IF;


    IF value_type = 'valuta'
    THEN
      value_json := value[1]::numeric;
      RETURN;
    END IF;

    IF value_mvp = true
    THEN
      SELECT INTO value_json to_jsonb(value);
    ELSE
      SELECT INTO value_json to_jsonb(value[1]);
    END IF;


    RETURN;
  END;
  $$;

COMMIT;

