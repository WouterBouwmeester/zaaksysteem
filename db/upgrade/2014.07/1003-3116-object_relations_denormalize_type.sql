BEGIN;

ALTER TABLE object_relationships ADD object1_type TEXT;
ALTER TABLE object_relationships ADD object2_type TEXT;

UPDATE object_relationships SET 
      object1_type = (SELECT object_class FROM object_data WHERE uuid = object_relationships.object1_uuid)
    , object2_type = (SELECT object_class FROM object_data WHERE uuid = object_relationships.object2_uuid)
;

ALTER TABLE object_relationships ALTER object1_type SET NOT NULL;
ALTER TABLE object_relationships ALTER object2_type SET NOT NULL;

COMMIT;
