BEGIN;

  CREATE OR REPLACE FUNCTION update_case_relations_complete_for_case() RETURNS TRIGGER LANGUAGE 'plpgsql' AS $$
    DECLARE
      open_case int;
      complete boolean;

    BEGIN

      -- Only for cases which have a parent
      IF NEW.pid IS NULL
      THEN
        RETURN NEW;
      END IF;

      complete := true;

      IF OLD.pid IS NULL AND NEW.status != 'resolved'
      THEN
        complete := false;
      ELSIF OLD.status = 'resolved' and NEW.status != 'resolved'
      THEN
        complete := false;
      ELSIF OLD.status != 'resolved' and NEW.status = 'resolved'
      THEN
        SELECT INTO open_case id FROM zaak WHERE status != 'resolved' AND pid = NEW.pid LIMIT 1;
        IF open_case IS NOT NULL
        THEN
          complete := false;
        END IF;
      END IF;

      UPDATE zaak_meta SET relations_complete = complete WHERE zaak_id = NEW.pid;

      RETURN NEW;

    END;

  $$;

  DROP TRIGGER IF EXISTS update_case_relations_complete ON "zaak";

  CREATE TRIGGER update_case_relations_complete
  AFTER INSERT OR UPDATE
  OF pid, status
  ON zaak
  FOR EACH ROW
  EXECUTE PROCEDURE update_case_relations_complete_for_case ();

COMMIT;
