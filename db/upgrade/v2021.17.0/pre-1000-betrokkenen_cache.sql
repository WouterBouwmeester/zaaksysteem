BEGIN;

-- Add new field to `zaak` containing a cache of people involved with the case
ALTER TABLE zaak
    ADD betrokkenen_cache JSONB NOT NULL DEFAULT '{}'::jsonb;

-- Trigger function that keeps the cache up to date
CREATE OR REPLACE FUNCTION sync_zaak_betrokkenen_cache ()
    RETURNS TRIGGER
    LANGUAGE 'plpgsql'
    AS $$
DECLARE
    affected RECORD;
BEGIN
    IF TG_OP = 'INSERT' OR TG_OP = 'UPDATE' THEN
        affected := NEW;
    ELSIF TG_OP = 'DELETE' THEN
        affected := OLD;
    END IF;

    IF affected.zaak_id IS NULL THEN
        -- Betrokkene without a case. Weird situation.
        RETURN affected;
    END IF;

    UPDATE
        zaak
    SET
        betrokkenen_cache = q.betrokkenen
    FROM (
        SELECT
            z.id AS zaak_id,
            json_object_agg(r.uuid, jsonb_build_object('roles', r.roles, 'pip_authorized', r.authorized)) AS betrokkenen
        FROM
            zaak z,
            (
                SELECT
                    zb.zaak_id,
                    zb.subject_id AS uuid,
                    json_object_agg(zb.id, jsonb_build_object('pip_authorized', zb.pip_authorized, 'authorisation', zb.authorisation, 'role', coalesce(zb.rol, CASE WHEN zb.id = z.aanvrager THEN
                                    'Aanvrager'
                                WHEN zb.id = z.behandelaar THEN
                                    'Behandelaar'
                                WHEN zb.id = z.coordinator THEN
                                    'Coordinator'
                                ELSE
                                    'Onbekende rol'
                                END))) AS roles,
                    sum(
                        CASE WHEN zb.pip_authorized THEN
                            1
                        ELSE
                            0
                        END)::int::boolean AS authorized
                FROM
                    zaak_betrokkenen zb
                    JOIN zaak z ON zb.zaak_id = z.id
                WHERE
                    zb.deleted IS NULL
                GROUP BY
                    zb.subject_id,
                    zb.zaak_id) AS r
            WHERE
                r.zaak_id = z.id
                AND r.uuid IS NOT NULL
            GROUP BY
                z.id) AS q
        WHERE (q.zaak_id = zaak.id AND zaak.id = affected.zaak_id);
    RETURN affected;
END;
$$;

DROP TRIGGER IF EXISTS update_zaak_betrokkenen_cache ON "zaak_betrokkenen";

CREATE TRIGGER update_zaak_betrokkenen_cache
    AFTER INSERT OR UPDATE OR DELETE ON "zaak_betrokkenen"
    FOR EACH ROW
    EXECUTE PROCEDURE sync_zaak_betrokkenen_cache ();

-- Update existing records
UPDATE
    zaak
SET
    betrokkenen_cache = q.betrokkenen
FROM (
    SELECT
        z.id AS zaak_id,
        json_object_agg(r.uuid, jsonb_build_object('roles', r.roles, 'pip_authorized', r.authorized)) AS betrokkenen
    FROM
        zaak z,
        (
            SELECT
                zb.zaak_id,
                zb.subject_id AS uuid,
                json_object_agg(zb.id, jsonb_build_object('pip_authorized', zb.pip_authorized, 'authorisation', zb.authorisation, 'role', coalesce(zb.rol, CASE WHEN zb.id = z.aanvrager THEN
                                'Aanvrager'
                            WHEN zb.id = z.behandelaar THEN
                                'Behandelaar'
                            WHEN zb.id = z.coordinator THEN
                                'Coordinator'
                            ELSE
                                'Onbekende rol'
                            END))) AS roles,
                sum(
                    CASE WHEN zb.pip_authorized THEN
                        1
                    ELSE
                        0
                    END)::int::boolean AS authorized
            FROM
                zaak_betrokkenen zb
                JOIN zaak z ON zb.zaak_id = z.id
            WHERE
                zb.deleted IS NULL
            GROUP BY
                zb.subject_id,
                zb.zaak_id) AS r
        WHERE
            r.zaak_id = z.id
            AND r.uuid IS NOT NULL
        GROUP BY
            z.id) AS q
WHERE
    q.zaak_id = zaak.id;

-- Make it fast:
DROP INDEX IF EXISTS zaak_betrokkenen_cache_uuid;

CREATE INDEX zaak_betrokkenen_cache_uuid ON zaak USING GIN (betrokkenen_cache);

COMMIT;

