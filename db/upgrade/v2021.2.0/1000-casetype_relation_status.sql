BEGIN;

  ALTER TABLE zaaktype_relatie ADD COLUMN status_new BOOLEAN NOT NULL DEFAULT false;
  UPDATE zaaktype_relatie set status_new = true where status = 1;
  ALTER TABLE zaaktype_relatie DROP COLUMN status;
  ALTER TABLE zaaktype_relatie RENAME COLUMN status_new TO status;

COMMIT;
