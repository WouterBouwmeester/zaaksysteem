
BEGIN;

DROP INDEX IF EXISTS object_relation_name_idx;
DROP INDEX IF EXISTS object_relation_name_and_id_idx;

COMMIT;

CREATE INDEX CONCURRENTLY object_relation_name_idx ON object_relation(name);
CREATE INDEX CONCURRENTLY object_relation_name_and_id_idx ON object_relation(name, object_id);
