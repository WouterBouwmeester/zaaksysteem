BEGIN;
    ALTER TABLE natuurlijk_persoon ADD COLUMN landcode INTEGER default 6030 NOT NULL;
    ALTER TABLE gm_natuurlijk_persoon ADD COLUMN landcode INTEGER default 6030 NOT NULL;
COMMIT;
