BEGIN;

    UPDATE zaaktype_node SET prevent_pip = false WHERE prevent_pip IS NULL;

    ALTER TABLE zaaktype_node ALTER COLUMN prevent_pip SET NOT NULL;

COMMIT;
