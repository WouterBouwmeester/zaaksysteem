
BEGIN;

  CREATE OR REPLACE FUNCTION case_subject_json(
    IN zb hstore,
    OUT json JSONB
  )
  LANGUAGE plpgsql
  AS $$
  DECLARE
    btype text;
    gm_id int;
    old_id text;
    bid int;

    subject record;
    s_hstore hstore;
    subject_json json;

    subject_uuid uuid;
    subject_type text;
    display_name text;
 BEGIN

    btype := zb->'betrokkene_type';

    IF btype IS NULL THEN
      json := null;
      RETURN;
    END IF;

    IF btype NOT IN ('medewerker', 'natuurlijk_persoon', 'bedrijf') THEN
      RAISE EXCEPTION 'Unknown betrokkene type %', btype;
    END IF;


    gm_id  := (zb->'gegevens_magazijn_id')::int;
    bid    := (zb->'betrokkene_id')::int;
    old_id := 'betrokkene-' || btype || '-' || gm_id;

    IF btype = 'medewerker' THEN
      subject_type := 'employee';

      SELECT INTO subject * FROM subject s WHERE s.subject_type = 'employee'
        AND id = gm_id;

      s_hstore := hstore(subject);
      SELECT INTO subject_json subject_employee_json(s_hstore);
      SELECT INTO display_name get_display_name_for_employee(s_hstore);

    ELSIF btype = 'natuurlijk_persoon' THEN

      subject_type := 'person';

      SELECT INTO
        subject
        np.*,
        a.straatnaam as street,
        a.huisnummer as street_number,
        a.huisletter as street_letter,
        a.huisnummertoevoeging as street_number_suffix,
        a.postcode as zipcode,
        a.woonplaats as city,
        a.functie_adres as address_type,
        mc.json as municipality,
        cc.json as country,
        a.adres_buitenland1 as foreign_address_line_1,
        a.adres_buitenland2 as foreign_address_line_2,
        a.adres_buitenland3 as foreign_address_line_3,
        a.bag_id as bag_id,
        a.geo_lat_long[0] as latitude,
        a.geo_lat_long[1] as longitude,
        cd.mobiel as mobile,
        cd.telefoonnummer as phone,
        cd.email as email,
        npa.uuid as uuid
      FROM
        gm_natuurlijk_persoon np
      JOIN
        natuurlijk_persoon npa
      ON
        np.gegevens_magazijn_id = npa.id
      LEFT JOIN
        gm_adres a
      ON
        np.adres_id = a.id
      LEFT JOIN
        country_code_v1_view cc
      ON
        cc.dutch_code = a.landcode
      LEFT JOIN
        municipality_code_v1_view mc
      ON
        mc.dutch_code = a.gemeente_code
      LEFT JOIN
        contact_data cd
      ON
        -- 1 or 2 is much clearer than natuurlijk_persoon or bedrijf ey
        -- :/
        (cd.gegevens_magazijn_id = np.gegevens_magazijn_id and cd.betrokkene_type = 1)

      WHERE
        np.gegevens_magazijn_id = gm_id
      AND
        np.id = bid
      ;

      s_hstore := hstore(subject);

      SELECT INTO subject_json subject_person_json(s_hstore);
      SELECT INTO display_name get_display_name_for_person(s_hstore);

    ELSIF btype = 'bedrijf' THEN

      subject_type := 'company';

      SELECT INTO subject
      b.*,
      ba.uuid,
      cc_vestiging.json as vestiging_country,
      cc_correspondentie.json as correspondentie_country,
      let.json as company_type
      FROM
       gm_bedrijf b
      LEFT JOIN
        bedrijf ba
      ON
        b.gegevens_magazijn_id = ba.id
      LEFT JOIN
        country_code_v1_view cc_vestiging
      ON
        cc_vestiging.dutch_code = b.vestiging_landcode
      LEFT JOIN
        country_code_v1_view cc_correspondentie
      ON
        cc_correspondentie.dutch_code = b.correspondentie_landcode
      LEFT JOIN
        legal_entity_v1_view let
      ON
        let.code = b.rechtsvorm
      WHERE
        b.gegevens_magazijn_id = gm_id
      AND
        b.id = bid
      ;

      s_hstore := hstore(subject);

      SELECT INTO subject_json subject_company_json(s_hstore);
      SELECT INTO display_name get_display_name_for_company(s_hstore);

    END IF;

    SELECT INTO json json_build_object(
        'preview', display_name,
        'type', 'subject',
        'reference', subject.uuid,
        'instance', json_build_object(
          'date_created', NOW(),
          'date_modified', NOW(),
          'display_name', display_name,
          'external_subscription', null,
          'old_subject_identifier', old_id,
          'subject_type', subject_type,
          'subject', subject_json
        )
    );

  END;
  $$;

COMMIT;

