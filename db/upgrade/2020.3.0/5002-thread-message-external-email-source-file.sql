BEGIN;
    ALTER TABLE thread_message_external ADD source_file_id integer DEFAULT NULL REFERENCES public.filestore(id);
COMMIT;
