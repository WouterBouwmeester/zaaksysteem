# Database files

## File layout

### db/template.sql

is used to create an empty database. This is the latest
correct database schema for Zaaksysteem.

### db/test-template.sql db/initial

Are used to load the initial docker instance.

### db/migration\_scripts

These are migration scripts for the database and these are deprecated. Please
use dev-bin or bin scripts for this or the queue-ing mechanism in Zaaksysteem.

### db/upgrade

All the database upgrade scripts.

### db/testbase

Deprecated, do not use.

## DB changes

You need to run `dev-bin/update_database.sh` to upgrade a database with one or
more files. This script will perform all the actions for you including updating
both test-template.sql and template.sql

### Files

In order to provide the people running the database upgrade, please adhere to
the following agreements.

* Upgrade file go into db/upgrade/NEXT for master release
* Upgrades that can be done prior to a release (think adding a column that
  doesn't for not null), use `pre-` as a file prefix.
* Upgrades that need to be done during a release use the prefix `rel-`.
* Upgrades that can be done post-release get the prefix `post-`.
* Upgrades can follow an order, use numbers 0000 - 9999 to indicate order.

### Views

Complex queries should become views. This is easier to debug and it also
becomes easier to optimize a query plan. Database views are prefixed with
`view_`.
