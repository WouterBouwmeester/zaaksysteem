// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

const path = require('path');
const fs = require('fs');
const crypto = require('crypto');
const express = require('express');
const helmet = require('helmet');
const generateCspHeader = require('./generateCspHeader');

const app = express();
const generateNonce = () => crypto.randomBytes(16).toString('base64');
const ROOT = path.join(__dirname, 'assets');

app.use(helmet({ contentSecurityPolicy: false }));
app.use(function (req, res, next) {
  res.removeHeader('X-Powered-By');
  next();
});

const frontends = ['intern', 'mor', 'pdc', 'vergadering'];

frontends.forEach((fe) => {
  const serveIndexFile = (req, res) => {
    const nonce = generateNonce();
    const unsafe = Boolean(req.query.unsafeinlinestyles);
    const indexPath = path.join(ROOT, fe, 'index.html');

    const indexPromise = fs.promises.readFile(indexPath).then((data) => {
      const index = data.toString();
      return index.replace('%%NONCE%%', nonce);
    });

    const cspHeaderPromise = generateCspHeader(nonce, unsafe, req.headers.host);

    Promise.all([indexPromise, cspHeaderPromise])
      .then(([renderedIndex, cspHeader]) => {
        res.setHeader('Content-Security-Policy', cspHeader);
        res.setHeader('Cross-Origin-Embedder-Policy', 'unsafe-none');
        res.send(renderedIndex);
      })
      .catch(() => {
        res.status(500);
      });
  };

  const serveAssetFile = (req, res) => {
    const parts = req.path.split('/');
    const file = parts[parts.length - 1];

    if (file && file.includes('worker')) {
      res.sendFile(path.join(ROOT, fe, file));
    } else {
      res.sendFile(path.join(ROOT, file));
    }
  };

  app.get('/' + fe, serveIndexFile);

  app.get('/' + fe + '/*', (req, res) => {
    if (req.path.includes('.') && !req.path.includes('.html')) {
      serveAssetFile(req, res);
    } else {
      serveIndexFile(req, res);
    }
  });
});

app.listen(process.env.PORT || 1050);
