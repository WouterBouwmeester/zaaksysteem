// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import shortid from 'shortid';
import assign from 'lodash/assign';
import capitalize from 'lodash/capitalize';
import first from 'lodash/head';
import get from 'lodash/get';

const statuses = {
  resolved: 'Afgehandeld',
  stalled: 'Opgeschort',
  new: 'Nieuw',
  open: 'In behandeling',
};

export const translateStatus = (status) => statuses[status];

const paymentStatusIcons = {
  offline: 'timer-sand',
  pending: 'close-circle',
  failed: 'close-circle',
  success: 'check-circle',
};

export const getRequestorTitle = (requestor) => {
  const hasPresentClient = get(requestor, 'preset_client') === 'Ja';

  return hasPresentClient ? 'Vooringevulde aanvrager' : 'Aanvrager';
};

const subjectTypeV2 = {
  natuurlijk_persoon: 'person',
  bedrijf: 'organization',
  medewerker: 'employee',
};

export const getSubjectLabel = (subject) => {
  const type = subjectTypeV2[subject.subject_type] || 'employee';

  switch (type) {
    case 'person': {
      const firstNames = get(subject, 'first_names');
      const firstInitial = capitalize(first(firstNames));
      const surName = get(subject, 'surname');

      return `${firstInitial}. ${surName}`;
    }
    case 'organization': {
      return get(subject, 'name');
    }
    case 'employee': {
      return subject.assignee || subject.full_name;
    }
  }
};

export const getSubjectUrl = (subject) => {
  const type = subjectTypeV2[subject.subject_type] || 'employee';
  const uuid = subject.uuid;

  return `/main/contact-view/${type}/${uuid}`;
};

export const getAssigneeLabel = (assignee, canAssign) => {
  if (!assignee) {
    return canAssign ? 'In behandeling nemen' : 'Geen behandelaar';
  }

  return getSubjectLabel(assignee);
};

export const isPassedDueDate = (targetDate) =>
  new Date(targetDate || 0).getTime() < new Date().getTime();

export const getLocationLabel = (location) =>
  get(location, 'instance.nummeraanduiding.human_identifier') ||
  get(location, 'instance.openbareruimte.human_identifier');

export const getPaymentStatusTitle = (paymentStatus) =>
  `Betaalstatus: ${paymentStatus.mapped || 'Onbekend'}`;

export const getPaymentStatusIcon = (paymentStatus) =>
  paymentStatusIcons[paymentStatus];

export const mapLink = (isCollapsed) => (link) =>
  assign(link, {
    id: shortid(),
    tooltip: isCollapsed ? `${link.title}: ${link.label}` : link.title,
  });
