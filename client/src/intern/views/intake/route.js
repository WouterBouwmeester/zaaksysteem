// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import angularUiRouterModule from 'angular-ui-router';
import filter from 'lodash/filter';
import get from 'lodash/get';
import resourceModule from '../../../shared/api/resource';
import snackbarServiceModule from './../../../shared/ui/zsSnackbar/snackbarService';

export default angular
  .module('Zaaksysteem.intern.intake.route', [
    angularUiRouterModule,
    resourceModule,
    snackbarServiceModule,
  ])
  .config([
    '$stateProvider',
    '$urlMatcherFactoryProvider',
    ($stateProvider, $urlMatcherFactoryProvider) => {
      $urlMatcherFactoryProvider.strictMode(false);

      $stateProvider.state('intake', {
        url: '/intake',
        title: 'Intake',
        template: '<zs-intake-view />',
        params: {
          action: {
            squash: true,
            value: null,
          },
          id: {
            squash: true,
            value: null,
          },
        },
        resolve: {
          __SIDE_EFFECT__: [
            '$rootScope',
            '$ocLazyLoad',
            '$q',
            'user',
            'snackbarService',
            ($rootScope, $ocLazyLoad, $q, user, snackbarService) => {
              const loggedInUser = get(user.data(), 'instance.logged_in_user');
              const capabilities = get(loggedInUser, 'capabilities');
              const authorized = Boolean(
                filter(
                  capabilities,
                  (capability) => capability.indexOf('documenten_intake') > -1
                ).length > 0
              );

              if (!authorized) {
                snackbarService.error(
                  'Helaas, u heeft geen toegang tot dit gedeelte van het zaaksysteem.',
                  {
                    actions: [],
                  }
                );

                return $q.reject('Not authorized');
              }

              return $q((resolve) => {
                require(['./index.js'], (...names) => {
                  $rootScope.$evalAsync(() => {
                    $ocLazyLoad.load(names.map((name) => ({ name })));
                    resolve();
                  });
                });
              });
            },
          ],
        },
      });
    },
  ]).name;
