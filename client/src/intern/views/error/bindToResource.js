// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import getParamsFromResourceError from './getParamsFromResourceError';
import propCheck from './../../../shared/util/propCheck';

export default (resource, $state, options = {}) => {
  propCheck.throw(
    propCheck.shape({
      resource: propCheck.object,
      $state: propCheck.object,
      options: propCheck.shape({
        description: propCheck.string.optional,
      }).optional,
    }),
    {
      resource,
      $state,
      options,
    }
  );

  resource.onError((err) => {
    $state.go(
      'error',
      getParamsFromResourceError(err, { description: options.description }),
      { location: false }
    );
  });

  return resource;
};
