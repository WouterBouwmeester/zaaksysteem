// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import get from 'lodash/get';

export const getAction = (data, casetype) => {
  // aggregate all actions from different phases
  const action = data.reduce((acc, phase) => {
    const actions = get(phase, 'items');

    if (!actions) {
      return acc;
    }

    return [...acc, ...actions];
  }, []);

  // find the relevant action by matching the uuids from
  // - the casetype of the form that we're in
  // - the casetype as defined in the action
  return action.find((action) => {
    const casetypeUuid = casetype.reference;

    return casetypeUuid === get(action, 'data.related_casetype_uuid');
  });
};

// values is of format = { attribute[parent_magicstring]: value }
//   and also contains non-attribute data
// we filter out the attributes to format = { parent_magicstring: value }
const formatValues = (values) =>
  Object.keys(values).reduce((acc, key) => {
    const [prefix, name] = key.split('.');

    if (prefix !== 'attribute') {
      return acc;
    }

    acc[name] = values[key];

    return acc;
  }, {});

// mapping is of format = { parent_magicstring: child_magicstring }
// values is of format  = { attribute[parent_magicstring]: value }
// note: 2 attributes can be mapped to 1 attribute. Last one wins.
const mapValues = (values, mapping) =>
  Object.keys(mapping).reduce((acc, nameParent) => {
    const nameChild = mapping[nameParent];

    acc[nameChild] = values[`attribute.${nameParent}`];

    return acc;
  }, {});

// the values of type checkbox are of format ['one', 'three']
// the form expects the format { 'one': true, 'two': false, 'three': true }
const convertCheckboxValues = (values, casetype) => {
  const fields = casetype.instance.phases.reduce(
    (acc, phase) => [...acc, ...phase.fields],
    []
  );

  const convertedValues = Object.keys(values).reduce((acc, key) => {
    const attributeConfig = fields.find((field) => field.magic_string === key);
    const isCheckbox = get(attributeConfig, 'type') === 'checkbox';
    const value = values[key];
    const hasValue = Boolean(value);

    if (!isCheckbox || !hasValue) {
      acc[key] = value;

      return acc;
    }

    acc[key] = value.reduce((acc, val) => {
      acc[val] = true;

      return acc;
    }, {});

    return acc;
  }, {});

  return convertedValues;
};

export const getValuesFromParent = (parentCase, action, casetype) => {
  const values = parentCase.values;
  const shouldCopyAllAttributes = get(action, 'data.kopieren_kenmerken');
  const attributeMapping = get(action, 'data.copy_selected_attributes');

  // copy all attributes 'off' && no attribute mapping ==> no copy
  // copy all attribures 'on'  && no attribute mapping ==> copy all
  // copy all attributes 'on'  &&    attribute mapping ==> copy all, ignore mapping
  // copy all attributes 'off' &&    attribute mapping ==> copy according to mapping
  if (!shouldCopyAllAttributes && !attributeMapping) {
    return null;
  } else if (shouldCopyAllAttributes) {
    const formattedValues = formatValues(values);

    return convertCheckboxValues(formattedValues, casetype);
  } else {
    const formattedValues = mapValues(values, attributeMapping);

    return convertCheckboxValues(formattedValues, casetype);
  }
};
