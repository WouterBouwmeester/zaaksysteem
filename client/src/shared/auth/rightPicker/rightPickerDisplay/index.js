// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import template from './template.html';
import roleServiceModule from './../../../user/roleService';
import composedReducerModule from './../../../api/resource/composedReducer';
import getPosition from './../../../user/roleService/getPosition';
import get from 'lodash/get';
import identity from 'lodash/identity';
import sortBy from 'lodash/sortBy';
import map from 'lodash/map';
import defaultCapabilities from './../defaultCapabilities';
import './styles.scss';

export default angular
  .module('rightPickerDisplay', [roleServiceModule, composedReducerModule])
  .component('rightPickerDisplay', {
    template,
    bindings: {
      right: '<',
    },
    controller: [
      '$scope',
      'roleService',
      'composedReducer',
      function (scope, roleService, composedReducer) {
        let ctrl = this,
          roleResource = roleService.createResource(scope),
          positionReducer,
          capabilitiesReducer;

        positionReducer = composedReducer(
          { scope },
          roleResource,
          () => ctrl.right
        ).reduce((departments, right) => {
          let position = get(right, 'position'),
            department = position ? getPosition(departments, position) : null;

          return {
            label: 'unit role'
              .split(' ')
              .map((pt) => get(department, `${pt}.name`))
              .filter(identity)
              .join('/'),
          };
        });

        capabilitiesReducer = composedReducer(
          { scope },
          () => ctrl.right
        ).reduce((right) => {
          let order = map(defaultCapabilities(), 'name');

          return {
            label: sortBy(
              get(right, 'capabilities', []).filter((cap) => cap.selected),
              (capability) => order.indexOf(capability.name)
            )
              .map((capability) => capability.label)
              .join(', '),
          };
        });

        ctrl.getPosition = positionReducer.data;

        ctrl.getCapabilities = capabilitiesReducer.data;
      },
    ],
  }).name;
