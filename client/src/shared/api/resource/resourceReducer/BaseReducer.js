// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import listenerFn from './../../../util/listenerFn';
import defaultsDeep from 'lodash/defaultsDeep';
import isEqual from 'lodash/isEqual';

let id = 0;

class BaseReducer {
  constructor(options) {
    this.$id = id++;

    this.$options = defaultsDeep({}, options);

    this.sources = [];

    this.$invalidated = false;

    this.$value = undefined;

    this.$state = 'pending';

    this.$destroyed = false;

    this.$reducers = [];

    this.$subscription = listenerFn();

    this.$onInvalidate = listenerFn();

    this.$onError = listenerFn();

    this.$onStateChange = listenerFn();

    this.$onDestroy = listenerFn();
  }

  setSrc(...rest) {
    this.sources = rest;

    this.invalidate();
  }

  sources() {
    return this.sources;
  }

  $setState(state) {
    if (this.$state !== state) {
      this.$state = state;
      this.invalidate();
      this.$onStateChange.invoke(state);
    }
  }

  reduce(reducer) {
    this.$reducers.push(reducer);
    return this;
  }

  subscribe(fn, opts) {
    this.$subscription.register(fn, opts);
  }

  invalidate() {
    if (!this.$invalidated) {
      this.$invalidated = true;
      this.$onInvalidate.invoke();
    }
  }

  recalculate() {
    let val = this.$value,
      compare = this.$options.compare,
      newVal,
      changed;

    this.$invalidated = false;

    try {
      newVal = !this.$reducers.length
        ? this.sources[0]
        : this.$reducers.reduce((prev, current, index) => {
            let args;

            if (index > 0) {
              args = [prev];
            } else {
              args = prev;
            }

            return current(...args);
          }, this.sources);

      if (compare) {
        if (typeof compare === 'function') {
          changed = !compare(val, newVal);
        } else {
          changed = !isEqual(val, newVal);
        }
      } else {
        changed = true;
      }

      if (changed) {
        this.$value = newVal;
        this.$subscription.invoke(this.$value, val);
      }
    } catch (error) {
      console.error(error);

      this.$value = undefined;
      this.$onError.invoke(error);

      this.$setState('rejected');
    }
  }

  value() {
    if (this.$invalidated) {
      this.recalculate();
    }

    return this.$value;
  }

  state() {
    return this.$state;
  }

  destroy() {
    this.$destroyed = true;

    this.$onDestroy.invoke();

    this.$subscription.destroy();
    this.$onInvalidate.destroy();
    this.$onStateChange.destroy();
    this.$onDestroy.destroy();
  }

  onInvalidate(fn) {
    return this.$onInvalidate.register(fn);
  }

  onError(fn) {
    return this.$onError.register(fn);
  }

  onStateChange(fn) {
    return this.$onStateChange.register(fn);
  }

  onDestroy(fn) {
    return this.$onDestroy.register(fn);
  }
}

export default BaseReducer;
