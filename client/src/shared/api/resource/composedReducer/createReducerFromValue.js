// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import BaseReducer from './../resourceReducer/BaseReducer';

export default (value) => {
  let reducer = new BaseReducer();

  reducer.$setState('resolved');
  reducer.setSrc(value);

  return reducer;
};
