// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import vormTemplateServiceModule from './../../../vorm/vormTemplateService';
import zsRolePickerModule from './../../../ui/zsRolePicker';
import get from 'lodash/get';

export default angular
  .module('vormRolePicker', [vormTemplateServiceModule, zsRolePickerModule])
  .directive('vormRolePicker', [
    () => {
      return {
        restrict: 'E',
        require: ['vormRolePicker', 'ngModel'],
        template: `<zs-role-picker
						data-unit="vm.getUnit()"
						data-role="vm.getRole()"
						data-depth="vm.depth()"
						data-on-change="vm.handleChange($unit, $role)"
					>
					</zs-role-picker>`,
        scope: {
          delegate: '&',
          templateData: '&',
          depth: '&',
        },
        bindToController: true,
        controller: [
          function () {
            let ctrl = this,
              ngModel;

            ctrl.link = (...controllers) => {
              [ngModel] = controllers;
            };

            ctrl.getUnit = () => get(ngModel, '$modelValue.unit');

            ctrl.getRole = () => get(ngModel, '$modelValue.role');

            ctrl.handleChange = (unit, role) => {
              ngModel.$setViewValue(
                {
                  unit,
                  role,
                },
                'click'
              );
            };
          },
        ],
        controllerAs: 'vm',
        link: (scope, element, attrs, controllers) => {
          controllers.shift().link(...controllers);
        },
      };
    },
  ])
  .run([
    'vormTemplateService',
    (vormTemplateService) => {
      vormTemplateService.registerType('org-unit', {
        control: angular.element(
          `<vorm-role-picker
						data-depth="vm.invokeData('depth')"
						ng-model
					></vorm-role-picker>`
        ),
      });
    },
  ]).name;
