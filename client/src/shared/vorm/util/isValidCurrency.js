// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
const currencyExpression = /^-?\d+(,\d{1,2})?$/;

/**
 * @param {string} value
 * @return {boolean}
 */
export const isValidCurrency = (value) =>
  typeof value === 'string' && currencyExpression.test(value);
