// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
export default {
  in: 1 / 1.19,
  in21: 1 / 1.21,
  in6: 1 / 1.06,
  ex: 1.19,
  ex6: 1.06,
  ex21: 1.21,
};
