// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import { weekdaysShort } from './i18nConstants';

/**
 * @test {weekdaysShort}
 */
describe('The `weekdaysShort` constant', () => {
  test('is an array that is mapped from the first two letters of each `weekdays` entry', () => {
    expect(weekdaysShort[0]).toBe('Zo');
    expect(weekdaysShort[1]).toBe('Ma');
    expect(weekdaysShort[2]).toBe('Di');
    expect(weekdaysShort[3]).toBe('Wo');
    expect(weekdaysShort[4]).toBe('Do');
    expect(weekdaysShort[5]).toBe('Vr');
    expect(weekdaysShort[6]).toBe('Za');
  });
});
