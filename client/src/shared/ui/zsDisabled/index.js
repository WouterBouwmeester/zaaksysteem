// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';

export default angular.module('zsDisabled', []).directive('zsDisabled', () => {
  return {
    restrict: 'A',
    link(scope, element, attr) {
      let value = false;
      const preventDefaultHandler = (event) => value && event.preventDefault();

      element.on('click', preventDefaultHandler);

      scope.$watch(attr.zsDisabled, (updatedValue) => {
        element.attr('aria-disabled', updatedValue);
        element.toggleClass('disabled', updatedValue);
        value = updatedValue;
      });

      scope.$on('$destroy', () => {
        element.off('click', preventDefaultHandler);
      });
    },
  };
}).name;
