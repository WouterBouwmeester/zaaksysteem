// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import merge from 'lodash/merge';

/**
 * @param {Function} getResourceData
 * @returns {Function}
 */
const getRequestOptionsFactory = (getResourceData) =>
  /**
   * @param {string} path
   * @param {Object} [options = {}]
   * @returns {*}
   */
  (path, options = {}) => {
    const {
      endpoint,
      token: { access_token },
    } = getResourceData();

    return merge(
      {
        url: `${endpoint}${path}`,
        headers: {
          Authorization: `Bearer ${access_token}`,
          'X-Client-Type': undefined,
        },
        withCredentials: false,
        timeout: 5000,
      },
      options
    );
  };

export default getRequestOptionsFactory;
