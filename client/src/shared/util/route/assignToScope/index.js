// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import isArray from 'lodash/isArray';
import assign from 'lodash/assign';
import mapKeys from 'lodash/mapKeys';

export default (resourcesToDestroy, ...rest) => {
  // This is not pretty. But we have to attach the resources to $rootScope,
  // and we want to destroy them along with the view scope

  let indexes,
    toDestroy = resourcesToDestroy,
    injectables = rest;

  if (!isArray(toDestroy)) {
    injectables = [resourcesToDestroy, ...injectables];
    toDestroy = [];
  }

  indexes = toDestroy.map((name) => injectables.indexOf(name));

  return [
    '$scope',
    '$stateParams',
    'observableStateParams',
    ...injectables,
    ($scope, $stateParams, observableStateParams, ...injected) => {
      let resources = indexes.map((index) => injected[index]);

      assign(
        $scope,
        $stateParams,
        observableStateParams,
        mapKeys(injected, (obj, index) => injectables[index])
      );

      $scope.$on('$destroy', () => {
        resources.forEach((resource) => {
          resource.destroy();
        });
      });
    },
  ];
};
