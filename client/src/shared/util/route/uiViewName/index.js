// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import angular from 'angular';
import getViewName from './../getViewName';

export default angular.module('uiViewName', []).directive('uiViewName', [
  '$interpolate',
  ($interpolate) => {
    return {
      restrict: 'A',
      link: (scope, element, attrs) => {
        element.addClass(
          getViewName($interpolate, scope, element, attrs).replace('@', '')
        );
      },
    };
  },
]).name;
