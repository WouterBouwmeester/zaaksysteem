// SPDX-License-Identifier: EUPL-1.2
// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import toIsoCalendarDate from './toIsoCalendarDate';

// NB: make sure to set the TZ environment variable to 'Europe/Amsterdam before you run the tests
// (especially on a build server)
// ZS-TODO: look into dynamic timezone faking

/**
 * @test {toIsoCalendarDate}
 */
describe('The `toIsoCalendarDate` module', () => {
  test('exports a function', () => {
    expect(typeof toIsoCalendarDate).toBe('function');
  });

  test('gets a local ISO calendar date from a date object', () => {
    const date = new Date('2000-07-01 12:00 UTC');

    expect(toIsoCalendarDate(date)).toBe('2000-07-01');
  });

  test('uses the environment time offset', () => {
    const date1 = new Date('2016-12-31 23:59 UTC+0100');
    const date2 = new Date('2016-12-31 23:59 UTC');

    expect(toIsoCalendarDate(date1)).toBe('2016-12-31');
    expect(toIsoCalendarDate(date2)).toBe('2017-01-01');
  });
});
