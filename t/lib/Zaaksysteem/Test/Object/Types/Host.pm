package Zaaksysteem::Test::Object::Types::Host;
use Moose;
extends 'Zaaksysteem::Test::Moose';
use Zaaksysteem::Test;

use Zaaksysteem::Object::Types::Host;

sub test_host_object {

    my %args = (
        owner    => 'betrokkene-bedrijf-42',
        fqdn     => 'test.zaaksysteem.nl',
        label    => 'testsuite host object',
        ssl_key  => 'foo',
        ssl_cert => 'bar',
        template => 'mintlab',
    );

    my $host = Zaaksysteem::Object::Types::Host->new(%args);
    isa_ok($host, 'Zaaksysteem::Object::Types::Host');

    foreach (keys %args) {
        is($host->$_, $args{$_}, "Host has correct $_: $args{$_}");
    }

    my @clearable_attrs = qw(
        ssl_key
        ssl_cert
        template
    );

    foreach (@clearable_attrs) {
        my $clearer = "clear_". $_;

        if (ok($host->can($clearer), "We can clear '$_'")) {
            $host->$clearer();
            is($host->$_, undef, ".. and '$_' is cleared");
        }
    }

}

sub test_host_new_empty {
    my $host = Zaaksysteem::Object::Types::Host->new_empty();
    isa_ok($host, 'Zaaksysteem::Object::Types::Host');
}




__PACKAGE__->meta->make_immutable;

__END__

=head1 NAME

Zaaksysteem::Test::Object::Types::Host - Test a control panel host object

=head1 DESCRIPTION

=head1 SYNOPSIS

    prove -lv :: Zaaksysteem::Test::Object::Types::Host

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
