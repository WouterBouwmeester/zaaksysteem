package Zaaksysteem::Test::DB::Component::ZaakOnafgerond;

use Moose;

extends 'Zaaksysteem::Test::Moose';

=head1 NAME

Zaaksysteem::Test::DB::Component::ZaakOnafgerond - Test
L<Zaaksysteem::DB::Component::ContactData>

=head1 SYNOPSIS

    prove -l -v :: Zaaksysteem::Test::DB::Component::ContactData

=cut

use Zaaksysteem::Test;
use Zaaksysteem::DB::Component::ZaakOnafgerond;

=head1 TEST METHODS

=cut

=head2 test_created

Test that $case->created returns a L<DateTime> object in 'UTC' timezone

=cut

sub test_created {
    my $mock = mock_Zaaksysteem_DB_Component_ZaakOnafgerond( epoch => 0 );
    my $dt = Zaaksysteem::DB::Component::ZaakOnafgerond::created($mock);

    isa_ok $dt, 'DateTime';
    isa_ok $dt->time_zone, 'DateTime::TimeZone::UTC';

}

sub test_zaak_gegevens {
    my $mock = mock_Zaaksysteem_DB_Component_ZaakOnafgerond( json_string => '[ "foo", { "bar": "baz", "qux": null } ]' );

    my $data = Zaaksysteem::DB::Component::ZaakOnafgerond::zaak_gegevens($mock);
    cmp_deeply(
        $data => [
            "foo",
            {
                bar => "baz",
                qux => undef,
            },
        ],
    "gets correct data from the JSON string"
    );

}

sub mock_Zaaksysteem_DB_Component_ZaakOnafgerond {
    my %args = @_;

    my $mock_obj = Test::MockObject->new();
    $mock_obj->mock(
        'json_string' => sub {
            return $args{json_string}
        }
    );
    $mock_obj->mock(
        'create_unixtime' => sub {
            return $args{epoch}
        }
    );
    $mock_obj->mock(
        '_created' => \&Zaaksysteem::DB::Component::ZaakOnafgerond::_created
    );

    return $mock_obj
}


1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
