package Zaaksysteem::Test::SAML2::Protocol::AuthnRequest;
use Moose;

extends 'Zaaksysteem::Test::Moose';

use Zaaksysteem::Test;
use Zaaksysteem::SAML2::Protocol::AuthnRequest;
use Zaaksysteem::Test::XML qw(get_xpath);

sub test_as_xml {
    my $auth_protocol;
    my $mock = mock_one(
        auth_protocol => sub { return $auth_protocol; },
        mangled_id    => sub { return 42; },
        issue_instant => sub { return 'now'; },
        destination   => sub { return 'final'; },
        issuer        => sub { return 'issuer'; },
        nameid_policy_format => sub { return 0; },
        service_identifiers => sub {
            return {
                AssertionConsumerServiceIndex => 1
            },
        },
        interface => {
            get_interface_config => \{
                saml_type => 'test',
            }
        }
    );

    {
        $auth_protocol = '';

        my $rv = Zaaksysteem::SAML2::Protocol::AuthnRequest::as_xml($mock);
        # In an ideal world, we'd check the XML here, but the generator uses a hash (which results in randomly ordered attributes).
        # If this element does not exist, it's good.
        unlike("$rv", qr/<samlp:RequestedAuthnContext[^>]+>/, "AuthnRequest without RequestedAuthnContext generated correctly");
    }

    {
        $auth_protocol = 'urn:oasis:names:tc:SAML:2.0:ac:classes:PasswordProtectedTransport';

        my $rv = Zaaksysteem::SAML2::Protocol::AuthnRequest::as_xml($mock);
        like("$rv", qr/<samlp:RequestedAuthnContext[^>]+>/, "AuthnRequest with RequestedAuthnContext generated correctly");
    }
}

sub test_eidas {
    my $auth_protocol;
    my $mock = mock_one(
        auth_protocol => sub { return $auth_protocol; },
        mangled_id    => sub { return 42; },
        issue_instant => sub { return 'now'; },
        destination   => sub { return 'final'; },
        issuer        => sub { return 'issuer'; },
        nameid_policy_format => sub { return 0; },
        service_identifiers => sub {
            return {
                AssertionConsumerServiceIndex => 1
            },
        },
        interface => {
            get_interface_config => \{
                saml_type => 'eidas',
                idp_scoping => 'foo',
            }
        }
    );

    my $xml = Zaaksysteem::SAML2::Protocol::AuthnRequest::as_xml($mock);
    my $xp = get_xpath($xml . "");

    $xp->registerNs('saml', 'urn:oasis:names:tc:SAML:2.0:assertion');
    $xp->registerNs('samlp', 'urn:oasis:names:tc:SAML:2.0:protocol');

    my @nodes = $xp->findnodes('//samlp:Scoping/samlp:IDPList/samlp:IDPEntry[@ProviderID]');
    is(@nodes, 1, "Found scoping node");
    is($nodes[0]->getAttribute('ProviderID'), "foo", "Got the correct provider ID");

    {

        my $mock = mock_one(
            auth_protocol => sub { return $auth_protocol; },
            mangled_id    => sub { return 42; },
            issue_instant => sub { return 'now'; },
            destination   => sub { return 'final'; },
            issuer        => sub { return 'issuer'; },
            nameid_policy_format => sub { return 0; },
            service_identifiers => sub {
                return {
                    AssertionConsumerServiceIndex => 1
                },
            },
            interface => {
                get_interface_config => \{
                    saml_type => 'eidas',
                }
            }
        );
        my $xml = Zaaksysteem::SAML2::Protocol::AuthnRequest::as_xml($mock);
        my $xp = get_xpath($xml . "");

        $xp->registerNs('saml', 'urn:oasis:names:tc:SAML:2.0:assertion');
        $xp->registerNs('samlp', 'urn:oasis:names:tc:SAML:2.0:protocol');

        my @nodes = $xp->findnodes('//samlp:Scoping/samlp:IDPList/samlp:IDPEntry[@ProviderID]');
        is(@nodes, 0, "No scoping node found");

    }

}

__PACKAGE__->meta->make_immutable();

=head1 COPYRIGHT and LICENSE

Copyright (c) 2018, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
